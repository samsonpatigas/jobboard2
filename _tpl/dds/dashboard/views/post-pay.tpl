<div id="app" class="container">
<h1 style="
    text-align: center;
    font-size: larger;
    font-weight: 400;
    padding-top: 30px;
    padding-bottom: 50px;
">Payment</h1>
  <form class="form-horizontal" role="form" @submit="formSubmit">
    <fieldset>
      <legend></legend>
      <div class="form-group">
        <label class="col-sm-3 control-label" for="card-holder-name">Name on Card</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" placeholder="Card Holder's Name" v-model="cardHolderName">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label" for="cardNumber">Card Number</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" placeholder="Debit/Credit Card Number" v-model="cardNumber">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
        <div class="col-sm-9">
          <div class="row">
            <div class="col-xs-3">
              <select class="form-control col-sm-2" v-model="expiryMonth">
                <option value="01">Jan (01)</option>
                <option value="02">Feb (02)</option>
                <option value="03">Mar (03)</option>
                <option value="04">Apr (04)</option>
                <option value="05">May (05)</option>
                <option value="06">June (06)</option>
                <option value="07">July (07)</option>
                <option value="08">Aug (08)</option>
                <option value="09">Sep (09)</option>
                <option value="10">Oct (10)</option>
                <option value="11">Nov (11)</option>
                <option value="12">Dec (12)</option>
              </select>
            </div>
            <div class="col-xs-3">
              <select class="form-control" v-model="expiryYear">
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label" for="cvv">Card CVV</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" placeholder="Security Code" v-model="cvv">
        </div>
      </div>   
      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
			<input
				class="btn btn-success"
				type="submit"
				value="Submit"
			>
        </div>
      </div>
    </fieldset>
  </form>
</div>

<script>
	var app = new Vue({
	  el: '#app',
	  delimiters: ['%%', '%%'],

	  data: {
	    cvv : "",
      expiryMonth : "",
      cardHolderName : "",
      cardNumber : "",
      expiryYear : "",
      employer_userid : "{$userid}",
      employer_email : "{$email}",
      employer_name : "{$name}",
      job_id : localStorage.getItem('job_id')

	  },
	  methods: {
            formSubmit(e) {
                e.preventDefault();
                let currentObj = this;
                axios.post('/charge-credit-card.php', {
                  cvv : app.cvv,
                  expiryMonth : app.expiryMonth,
                  cardHolderName : app.cardHolderName,
                  cardNumber : app.cardNumber,
                  expiryYear : app.expiryYear,
                  employer_userid : app.employer_userid,
                  employer_name : app.employer_name,
                  employer_email : app.employer_email,
                  job_id : app.job_id
                })
                .then(function (response) {
                    currentObj.output = response.data;
                    window.location = 'https://jobboard.ferret9.com/jobpaymentdone';
                })
                .catch(function (error) {
                    currentObj.output = error;
                });
            }
        }
	})
</script>