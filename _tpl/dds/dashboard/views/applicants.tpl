<!-- /_tpl/dds/dashboard/views/applicants.tpl -->
<link rel="stylesheet" href="/_tpl/dds/1.5/css/reset.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" integrity="sha256-vK3UTo/8wHbaUn+dTQD0X6dzidqc5l7gczvH+Bnowwk=" crossorigin="anonymous" />
<div class="row board ml0 pl0">
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <h2>Applicants</h2>
  </div>
  {* {$data|var_dump} *}
  <div class="col-lg-12" >
	  <table class="table is-striped is-striped">
		<thead>
			<tr>
				<td class="has-text-weight-bold">Applicant ID</td>
				<td class="has-text-weight-bold">Comment</td>
				<td class="has-text-weight-bold">Approve</td>
			</tr>
		</thead>
		  {foreach $data['list'] as $job}
			<tr>
				<td>
					{* {$job|var_dump} *}
					{$job['fullname']}
				</td>
				<td>
					{$job['comment']}
				</td>
				<td>
					{if $job['status'] == 2}
						Approved
					{else}
					    <a href="/jobapprove?id={$job['id']}&job={$job['job_id']}&emp={$job['employer_id']}">Approve</a>
					{/if}					
				</td>
			</tr>
		  {/foreach}
	  </table>
	</div>
</div>