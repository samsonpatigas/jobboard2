<!-- post-second-step.tpl -->
<div class="row " id="step-2">

	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 mlpl0">
    
		<div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Posted by</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.company}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0; margin-top: -34px;float:left;font-size:17px;">Category</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.category_name}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.job_type}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0; margin-top: -34px;float:left;font-size:17px;">Location</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.location_asci}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Date posted</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.created_on}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Post period</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      {if $job_data.f9_post_peroid == '3650'}
                        <p>No Expiry</p>
                      {else}
                        <p>{$job_data.f9_post_peroid}</p>
                      {/if}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Confidential Ad:</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      {if $job_data.f9_confidential_ad eq 0}
                      <p>No</p>
                      {else}
                        <p>Yes</p>
                      {/if}
                    </div>
                  </div>
                </div>
              </div>              
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Bilingual</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_bilingual}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>            
          </div>

        <div class="row" style="display:none">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Gender</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_gender}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6" style="display:none">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Language</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_language}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Position</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_position}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Zip Code</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_zip}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">City / State:</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_city}  {$job_data.f9_state}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Advertisement</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>
                        {$job_data.f9_position_notes}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6" style="display:none">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;    margin-top: -34px;float:left;font-size:17px;">Short Description</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.description}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Skills</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_skills}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Office Software</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>
                        {assign var="awords" value=','|explode:$job_data.f9_office_software}
                        {section name=i loop=$awords}
                        {$awords[i]|escape}<br />
                        {/section}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Practice Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_practice_type}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1" style="display:none">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Position Type</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_position_type}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1" style="display: none;">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Advertisement</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_position_notes}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1" style="display:none">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Pay Range (Minimum)</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_pay_min}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card1 card-nav-tabs1" style="display:none">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Pay Range (Maximum)</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_pay_max}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6" style="display: none">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Bilingual</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_bilingual}</p>
                    </div>
                  </div>
                </div>                
              </div>
            </div>
            <div class="col-md-6" style="display: none">
              <div class="card1 card-nav-tabs1">
                <div class="card-header1 card-header-primary1">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 style="color: #7527a0;margin-top: -34px;float:left;font-size:17px;">Job Description</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body1 ">
                  <div class="tab-content1 text-left">
                    <div class="tab-pane active" id="profile">
                      <p>{$job_data.f9_position_notes}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
		{if $PAY == 'true'}

				<div class="row mb20">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3 class="process-heading">{$translations.post_step2.adtype}</h3>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">		
					{if $SPOTLIGHT == 'true' && $PREMIUM_LISTING_PRICE > 0}                						
            {$translations.post_step2.premium} &nbsp;-&nbsp;&nbsp; {$translations.post_step2.premium_desc}
					{else}
						{$translations.post_step2.standard}
					{/if}
					</div>
				</div>

				{if $SPOTLIGHT == 'true' && $PREMIUM_LISTING_PRICE > 0}
					<div class="row mb20">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading">{$translations.post_step2.premium_ad_fee}</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
						                                                                       	{$FORMATED_CURRENCY}
						</div>
					</div>
				{/if}
				
					<div class="row mb20">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading">{$translations.post_step2.job_post_fee}</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">	
						{if $JOB_POSTED_PRICE > 0}                                             							
						  {$FORMATED_CURRENCY_JOBPOSTED}
						{elseif $JOB_POSTED_PRICE == 0}
							{$translations.post_step2.free}
						{/if}
						</div>
					</div>

				<br />

					<div class="row mb12">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading">{$translations.post_step2.fees}</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
						  {$PRICE}
						</div>
					</div>

        {if $VAT}
					<div class="row mb12">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading">{$translations.post_step2.vat}</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
						  {$PRICE_VAT}
						</div>
					</div>

					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<h3 class="process-heading">{$translations.post_step2.final_price}</h3>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
						  {$PRICE_VAT_TOTAL}
						</div>
					</div>
      {/if}

		{else}
			<div class="row" style="display: none">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h3 class="process-heading">{$translations.post_step2.adtype}</h3>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
				  {$translations.post_step2.standard}
				</div>
			</div>
			<br />

			{if $PAYMENT_MODE == '3'}
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3 class="process-heading">{$translations.post_step1.account_plan}</h3>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
					 {$jobs_left_msg}
					</div>
				</div>
			{else}
				<div class="row" style="display: none;">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3 class="process-heading">{$translations.post_step2.fees}</h3>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 pt10 tar light-black">								
					 {$translations.post_step2.free}
					</div>
				</div>
			{/if}
		{/if}

	</div>

<!--	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mlpl0">
    		<button type="button" data-toggle="modal" data-target="#previewModal" id="preview-btn-mobile" class="btn mbtn fl" style="margin-left: 40px;">{$translations.post_step2.preview_btn_label}</button>
    	</div> -->

</div>
<br />

<form method="post" action="{$BASE_URL}{$URL_JOB_POSTED}/" role="form">
  <button type="submit" style="position: absolute; left: 45px; top: 361px;" id="goback" name="goback" >{$translations.dashboard_recruiter.go_back_edit_text}</button>
	<input type="hidden" id="step" name="step" value="2" />
	<input type="hidden" id="employer_id" name="employer_id" value="{$job_data.employer_id}" />
	<input type="hidden" id="job_id" name="job_id" value="{$job_data.id}" />

	<div class="button-holder">
		{if $PAY == 'true'}
			<input type="hidden" id="price" name="price" />
			{if $SPOTLIGHT == 'true' && $PREMIUM_LISTING_PRICE > 0}
				<input type="hidden" id="price_premium" name="price_premium" />
			{/if}
			{if $JOB_POSTED_PRICE > 0}
				<input type="hidden" id="price_ad" name="price_ad" />
			{/if}

			<input type="hidden" name="item_name" value="{$translations.paypal.checkout_item_name}" /> 
			<input type="hidden" name="item_number" value="{$translations.paypal.checkout_item_number}" /> 
		    <input type="hidden" name="item_desc" value="{$translations.paypal.checkout_item_desc}" /> 
			<input type="hidden" name="item_price" />
			<input type="hidden" name="item_qty" value="1" />

		<button type="submit" class="btn btn-green" id="paypal_submit" name="paypal_submit" >{$translations.post_step2.paypal_btn_label}</button>

		{else}

			<button type="submit" class="btn btn-green" id="pblish" name="pblish" >Proceed to Payment</button>

		{/if}

			<button type="submit" class="btn btn-gray sec-btn" id="goback" name="goback" >{$translations.dashboard_recruiter.go_back_edit_text}</button>

	</div>

</form>

<script type="text/javascript">
    localStorage.setItem('job_id', '{$job_data.id}');
</script>