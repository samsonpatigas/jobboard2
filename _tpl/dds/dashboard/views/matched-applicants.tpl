
<style type="text/css">
.modal-body {
padding: 5% 3% !important;
}
.modal-dialog {
	margin-top: 10% !important;
}
</style>

<div class="row board mb25">
  <h2>Candidate List</h2>

</div>
<br /><br />

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mlpl0 mb25">
	<form role="form">
		<label class="mb15" for="job_select">Select jobs that candidate applied</label>
			<i id="apps_spinner" class="ml15 displayNone fa fa-refresh fa-spin fa-lg fa-fw refresh-spinner"></i>
			<select id="job_select" name="job_select" class="form-control minput">
				{foreach from=$jobs_select key=id item=value}
					{if $select_job_id}
						{if $select_job_id == $id}
						<option selected value="{$id}">{$value}</option>
						{else}
						<option value="{$id}">{$value}</option>
						{/if}
					{else}
						<option value="{$id}">{$value}</option>
					{/if}
					}
				{/foreach}
			</select>
	</form>
</div>
<br /><br />

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
	<div class="table-responsive applications-table">
		<table class="table table-striped">
		    <thead>
		      <tr>
		        <th><strong>Candidates</strong></th>
		        <th><strong>Position</strong></th>
		        <th><strong>{$translations.apply.skills}</strong></th>
		        <th><strong>{$translations.apply.located}</strong></th>
		        <th><strong>{$translations.applications.cv}</strong></th>
		        <th><strong>{$translations.apply.table_message}</strong></th>
		        <th><strong>{$translations.apply.table_review}</strong></th>
		        <th><strong>{$translations.apply.table_delete}</strong></th>
		      </tr>
		    </thead>
		    <tbody id="ajax-content">
		    </tbody>
		</table>
	</div>
</div>

<!-- {literal}
	<script type="text/javascript">
		$( "#modal" ).click(function() {
		  alert( "Modal is click successfully" );
		});
	</script>
{/literal} -->
<!-- </form> -->

<!-- {foreach from=$active_jobs item=$job}
	{$job}
{/foreach}