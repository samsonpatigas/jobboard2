{include file="1.5/layout/sjs-header.tpl"}
<!-- dashboard.tpl -->
<div class="candidate-v2 mobileMT10">
  <div class="row">
      <div class="col-md-2 col-sm-12">
        <ul>
            <a href="/{$URL_DASHBOARD}/{$URL_DASHBOARD_SEARCHABLE}">
              <li id="search-li">
                <i class="fa fa-search" aria-hidden="true"></i>
                  SEARCH CANDIDATES
              </li>
            </a>
            <a href="/{$URL_DASHBOARD}/{$URL_DASHBOARD_POST}">
              <li id="search-li">
                <i class="fa fa-search" aria-hidden="true"></i>
                  POST A JOB +
              </li>
            </a>
            <a href="/{$URL_DASHBOARD}/{$URL_DASHBOARD_JOBS}">
              <li id="search-li">
                <i class="fa fa-search" aria-hidden="true"></i>
                  MY JOBS
              </li>
            </a>            
            <a href="/{$URL_DASHBOARD}/{$URL_DASHBOARD_EDIT_COMPANY}">
              <li id="company-li">
                <i class="fa fa-vcard-o" aria-hidden="true"></i>
                  MY PRACTICE
              </li>
            </a>

            <a href="/{$URL_DASHBOARD}/{$URL_DASHBOARD_SETTINGS}">
              <li id="settings-li">
                <i class="fa fa-cogs" aria-hidden="true"></i>
                {$translations.dashboard_recruiter.left_menu_settings}
              </li>
            </a>

            <a href="/{$URL_DASHBOARD}/URL_DASHBOARD_EMAIL_LOGS">
              <li>
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                CONTACTS
              </li>
            </a>
            <a href="/{$URL_DASHBOARD}/URL_DASHBOARD_BANNED_JOB_SEEKER">
              <li>
                <i class="fa fa-cogs" aria-hidden="true"></i>
                BANNED JOB SEEKER
              </li>
            </a>
            <a href="/{$URL_DASHBOARD}/URL_DASHBOARD_PREVIOUS_JOB_SEEKER">
              <li>
                <i class="fa fa-cogs" aria-hidden="true"></i>
                PREVIOUS JOB SEEKER
              </li>
            </a>
            <a href="/{$URL_LOGOUT}"><li><i class="fa fa-sign-out" aria-hidden="true"></i>
                LOGOUT</li></a>

            {if $PAYMENT_MODE == '3'}
              <a href="/{$URL_DASHBOARD}/{$URL_DASHBOARD_ACCOUNT}">
                <li id="account-li"><i class="fa fa-cubes" aria-hidden="true"></i>
                  {$translations.dashboard_recruiter.left_menu_myaccount}
                </li>
              </a>
            {/if}
         </ul>
        </div>

        <div class="col-md-10 col-sm-12">
         <div class="employer-dashboard">
          <div class="container">
            {include file="dashboard/views/$view"}
           </div>
          </div>
        </div>
  </div>

</div>

{if $init_modal_popups == '1'}
  <div class="dash-modal">
    {include file="dashboard/modals/applicant-modal.tpl"}
  </div>
{/if}

{if $init_modal_popup_preview == '1'}
  <div class="dash-modal">
    {include file="dashboard/modals/preview-modal.tpl"}
  </div>
{/if}

{if $init_modal_popup_jobs == '1'}
  <div class="dash-modal">
    {include file="dashboard/modals/jobinfo-modal.tpl"}
  </div>
{/if}

{if $applications_modal_init == '1'}
  <div class="dash-modal">
    {include file="dashboard/modals/applications-modal.tpl"}
  </div>
{/if}

{if $applications_modal_init == '2'}
  <div class="dash-modal">
    {include file="dashboard/modals/match-applicant-modal.tpl"}
  </div>
{/if}


<div class="dash-modal">
  {include file="dashboard/modals/deactivate-modal.tpl"}
</div>

<div class="dash-modal">
  {include file="dashboard/modals/renew-modal.tpl"}
</div>

{include file="1.5/layout/sjs-footer.tpl"}
<script>
  localStorage.setItem("is_client", true);
  localStorage.setItem("is_logged_in", true);
</script>