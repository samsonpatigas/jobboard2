<!-- _tpl/dds/1.5/layout/sjs-footer-v2.tpl -->
            <footer class="footer is-fixed-bottom" style="padding-top:40px; padding-bottom:50px;background-color:#ff9900">
                <div class="container">
                    <div class="columns">
                        <div class="column">
                            <p>{$SITE_NAME} &copy; {$YEAR}</p>
                        </div>
                    </div>
                </div>
            </footer>

            {if $dashboard_footer_flag}
                {include file="1.5/layout/dashboard-snippets.tpl"}
            {else}
                {include file="1.5/layout/js-snippets.tpl"}
            {/if}
        </section>
    </body>
</html>