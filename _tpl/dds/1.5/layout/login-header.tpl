<!-- /_tpl/dds/1.5/layout/login-header.tpl -->
<!doctype html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
	<title>{if $seo_title}{$seo_title}{else}{$html_title}{/if}</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <meta name="description" content="{if $seo_desc}{$seo_desc}{else}{$meta_description}{/if}" />
  <meta name="keywords" content="{if $seo_keys}{$seo_keys}{else}{$meta_keywords}{/if}" />

    <!-- 
   Chrome and Opera Icons - to add support create your icon with different resolutions. Default is 192x192
     <link rel="icon" sizes="192x192" href="{$BASE_URL}fav.png" >
    <link rel="icon" sizes="144x144" href="{$BASE_URL}fav-144.png" >
    <link rel="icon" sizes="96x96" href="{$BASE_URL}fav-96.png" >
    <link rel="icon" sizes="48x48" href="{$BASE_URL}fav-48.png" >
  -->
  <link rel="icon" href="{$BASE_URL}fav.png" >
  <!-- 
   Apple iOS icons
    <link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
  -->
  <link rel="apple-touch-icon" href="{$BASE_URL}fav-ios.png">
  <!-- iOS startup image -->
  <link rel="apple-touch-startup-image"  href="{$BASE_URL}fav-startup.png">
  <!-- Apple additional meta tags -->
  <meta name="apple-mobile-web-app-title" content="{if $seo_title}{$seo_title}{else}{$html_title}{/if}">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <!-- Android web app capable -->
  <meta name="mobile-web-app-capable" content="yes">
  <!-- IE & Windows phone pin icons. Almost same size like ios so it is reused -->
  <meta name="msapplication-square150x150logo" content="fav-ios.png">

  <!-- facebook meta tags for sharing -->
  <meta property="og:locale" content="en_US" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="{if $seo_title}{$seo_title}{else}{$html_title}{/if}" />
  <meta property="og:description" content="{if $seo_desc}{$seo_desc}{else}{$meta_description}{/if}" />
  <meta property="og:url" content="{$MAIN_URL}" />
  <meta property="og:site_name" content="{$SITE_NAME}" />
  <meta property="og:image" content="{$MAIN_URL}share-image.png" />

  <!-- twitter metatags -->
  <meta name="twitter:card" content="summary_large_image"/>
  <meta name="twitter:description" content="{if $seo_desc}{$seo_desc}{else}{$meta_description}{/if}"/>
  <meta name="twitter:title" content="{if $seo_title}{$seo_title}{else}{$html_title}{/if}"/>

  <meta name="twitter:domain" content="{$SITE_NAME}"/>
  <meta name="twitter:image" content="{$MAIN_URL}share-image.png" />

  <!-- RSS -->
  <link rel="alternate" type="application/rss+xml" title="{$SITE_NAME} RSS Feed" href="{$BASE_URL}rss">
  

  <!-- CSS -->

  <!-- Attached CSS -->
  <link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/1.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/1.5/css/style.css">
  <link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/1.5/css/reset.css">
  <link rel="stylesheet" href="{$BASE_URL}_tpl/{$THEME}/1.5/css/font-awesome.min.css">

  <link href="{$BASE_URL}js/tags/tagl/assets/css/taggle.css" rel="stylesheet">
  <link href="{$BASE_URL}_tpl/{$THEME}/1.5/css/dev.css" rel="stylesheet">

  <noscript>{$translations.website_general.noscript_msg}</noscript>
  
  <style type="text/css">
  {$custom_css}
  </style>

  <script src="{$BASE_URL}_tpl/{$THEME}/1.5/js/jquery.min.js"></script>
  <script src="{$BASE_URL}_tpl/{$THEME}/1.5/js/bootstrap.min.js"></script>


  {if $INDEED == 'activated'}
  <script type="text/javascript" src="https://gdc.indeed.com/ads/apiresults.js"></script>         
  {/if}

  <script type="text/javascript">
    current_url = "{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}";
    if(current_url == "https://jobboard.ferret9.com/login-recruiters"){

    }
  </script>
</head>

<body>
<div class="page-loader"></div>

<!-- enclosed in the footer -->
<div id="wrapper">
  <div id="headerLogo" style="width: 100%; background-color:#ffb000e0;">
    <div class="row">
      <div class="col-md-4 col-sm-3 col-xs-12" style="margin-top: 20px; padding-left: 32px;">
        <a href="{$BASE_URL}{$URL_SIGN_UP}"><i class="fa fa-chevron-circle-left"></i>&nbsp; {$translations.dashboard_recruiter.back}</a>
      </div>
      <div class="col-md-8 col-sm-7 col-xs-12">
        <img src="{$BASE_URL}{$SITE_LOGO_PATH}" style="width: 300px;">
      </div>
    </div>
  </div>

  

{if $PLAIN_SITE_REGISTER == 'true'}
{*   <div style="width: 100%; background-color:#ffb000e0">
    <div class="row">
      <div class="col-md-4 col-sm-3 col-xs-12" style="margin-top: 20px; padding-left: 32px;">
      <a href="{$BASE_URL}{$URL_SIGN_UP}"><i class="fa fa-chevron-circle-left"></i>&nbsp; {$translations.dashboard_recruiter.back}</a>
      </div>
      <div class="col-md-8 col-sm-7 col-xs-12">
        <img src="{$BASE_URL}{$SITE_LOGO_PATH}" style="width: 300px;">
      </div>
    </div>
  </div> *}
{/if}
