<?php
/* Smarty version 3.1.30, created on 2020-01-04 20:47:23
  from "/home1/fninport/public_html/jobboard/_tpl/dds/profile/applications.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e114e3bd16476_88402606',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e4d53b28f972d9034f3cce7df9642e9c0f37be0c' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/profile/applications.tpl',
      1 => 1569867106,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e114e3bd16476_88402606 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- profile/applications.tpl -->
<div class="row">
    <div class="col-md-12 col-sm-12 mb20">
        <p class="profile-subheadline">
            <?php echo $_smarty_tpl->tpl_vars['translations']->value['profile']['apps_desc'];?>

        </p>
    </div>
</div>

<?php $_smarty_tpl->_assignInScope('i', 0);
$__section_application_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_application']) ? $_smarty_tpl->tpl_vars['__smarty_section_application'] : false;
$__section_application_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['apps']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_application_0_total = $__section_application_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_application'] = new Smarty_Variable(array());
if ($__section_application_0_total != 0) {
for ($__section_application_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] = 0; $__section_application_0_iteration <= $__section_application_0_total; $__section_application_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']++){
?> 
<?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);
?>

 <div class="panel-group">
    <div class="panel panel-default" >
      <div class="">
        <h4 class="panel-title arrow-down1" data-toggle="collapse" href="#collapse<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" onclick="this.classList.toggle('active1')"><?php echo $_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['job_title'];?>
 @ <?php echo $_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['job_company'];?>

          <a ></a>
        </h4>
      </div>
      <div id="collapse<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="col-md-8 col-sm-8 tal">
            <i class="" aria-hidden="true"></i>

            <table class="table" style="width: auto;">
              <tr>
                <th style="border:none;color: #7527a0;">Date Applied</th>
                <th style="border:none;color: #7527a0;">Date Posted</th>
              </tr>
              <tr>
                <td style="border:none;"><?php echo $_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['created_on'];?>
</td>
                <td style="border:none;"><?php echo $_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['date_posted'];?>
</td>
              </tr>
              <tr>
                <th style="border:none;color: #7527a0;">Title</th>
                <th style="border:none;color: #7527a0;">City</th>
                <th style="border:none;color: #7527a0;">Job ID</th>
              </tr>
              <tr>
                <td style="border:none;"><?php echo $_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['job_title'];?>
</td>
                <td style="border:none;"><?php echo $_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['city_name'];?>
</td>
                <td style="border:none;"><?php echo 10000+$_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['job_id'];?>
</td>
              </tr>
              <tr>
                

              </tr>
            </table>
          </div>
          
          <div class="col-md-4 col-sm-4">
            <div class="listing-type">
                <p>Status: </p>
                <?php if ($_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['status'] == '0') {?>
                    <span class="profile-pending hideMobile">OPEN</span>
                <?php } elseif ($_smarty_tpl->tpl_vars['apps']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_application']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_application']->value['index'] : null)]['status'] == '1') {?>
                    <span class="profile-rejected hideMobile">CLOSED</span>
                <?php } else { ?>
                    <span class="profile-reviewed hideMobile"><?php echo $_smarty_tpl->tpl_vars['translations']->value['profile']['review_label'];?>
</span>
                <?php }?>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </div>
 <?php
}
}
if ($__section_application_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_application'] = $__section_application_0_saved;
}
?>
<br>

<div class="row">
  <div class="col-md-12 col-sm-12 mb20">
    <h2>Available jobs</h2>   
    <p>Below are the jobs matching your position entered in your profile</p>
 </div>
</div>
<br>
<br>
<?php $_smarty_tpl->_assignInScope('j', 0);
$__section_notapplied_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied'] : false;
$__section_notapplied_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['jobs']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_notapplied_1_total = $__section_notapplied_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_notapplied'] = new Smarty_Variable(array());
if ($__section_notapplied_1_total != 0) {
for ($__section_notapplied_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] = 0; $__section_notapplied_1_iteration <= $__section_notapplied_1_total; $__section_notapplied_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']++){
?> 
<?php $_smarty_tpl->_assignInScope('j', $_smarty_tpl->tpl_vars['j']->value+1);
?>


<?php if ($_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['status'] == '1') {?>
 <div class="panel-group">
    <div class="panel panel-default" >
      <div class="">
        <h4 class="panel-title arrow-down1" data-toggle="collapse" href="#collapses<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
" onclick="this.classList.toggle('active1')"><?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['job_title'];?>
 @ <?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['job_company'];?>

          <a ></a>
        </h4>
      </div>
      <div id="collapses<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="col-md-8 col-sm-8 tal">
            <i class="" aria-hidden="true"></i>
            
            <table class="table" style="width: auto;">
              <tr>
                <td style="width:300px"><p>Position</p></td>
                <td><?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['job_title'];?>
</td>
              </tr>
              <tr>
                <td><p>Job ID</p></td>
                <td><?php echo 10000+$_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['id'];?>
</td>
              </tr>
              <tr>
                <td><p>Job Location</p></td>
                <td><?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['f9_city'];?>
, <?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['f9_state'];?>
</td>
              </tr>
              <tr>
                <td><p style="100%">Fulltime / Parttime</p></td>
                <td><?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['f9_position_type'];?>
</td>
              </tr>
              <tr>
                <td><p>Date Posted</p></td>
                <td><?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['created_on'];?>
</td>
              </tr>
            </table>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="listing-type">
              <p>Status: </p>
              <?php if ($_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['status'] == '0') {?>
                  <span class="profile-pending hideMobile">INACTIVE</span>
              <?php } else { ?>
                  <span class="profile-rejected hideMobile">ACTIVE</span>
              <?php }?>
            </div> 
            <div>
              <form action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['url_title'];?>
/<?php echo $_smarty_tpl->tpl_vars['jobs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_notapplied']->value['index'] : null)]['job_id'];?>
">
                <input type="submit" value="View Details" class="btn mbtn zeromlplLeft" style="margin-top: 10px;" />
              </form>
            </div> 
            </div>
        </div>
      </div>
    </div>
  </div>
 <?php }?> 
 <?php
}
}
if ($__section_notapplied_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_notapplied'] = $__section_notapplied_1_saved;
}
?>


<?php }
}
