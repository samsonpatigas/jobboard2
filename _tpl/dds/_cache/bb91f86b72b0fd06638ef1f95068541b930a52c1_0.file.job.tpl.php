<?php
/* Smarty version 3.1.30, created on 2020-01-06 08:55:54
  from "/home1/fninport/public_html/jobboard/_tpl/dds/jobs/job.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e134a7a4ab279_56096299',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bb91f86b72b0fd06638ef1f95068541b930a52c1' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/jobs/job.tpl',
      1 => 1575393751,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:snippets/detail-modals.tpl' => 1,
    'file:jobs/apply-existing-modal.tpl' => 1,
    'file:jobs/apply-modal.tpl' => 1,
    'file:snippets/listing-sitemap.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5e134a7a4ab279_56096299 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- /_tpl/dds/jobs/job.tpl -->
<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


 <!-- FACEBOOK og protocol -->
<meta property="og:url"  content="http://<?php echo $_smarty_tpl->tpl_vars['job_url']->value;?>
" />
<meta property="og:type"  content="website" />
<meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['seo_title']->value;?>
" />
<meta property="og:site_name" content="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
"/>
<meta property="og:description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>" />
<meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['job']->value['company_logo_path'];?>
" />

<?php ob_start();
echo $_smarty_tpl->tpl_vars['job']->value['id'];
$_prefixVariable1=ob_get_clean();
$_smarty_tpl->_assignInScope('job_id', $_prefixVariable1);
?>

<?php $_smarty_tpl->_subTemplateRender("file:snippets/detail-modals.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="job-details">
            <!--job.tpl -->
            <div class="container">
                <div class="row top-heading">
                    <div class="col-md-12 col-xs-12">
                        
                        <h1><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</h1>
                    </div>
                </div>
                <div class="row top-heading">
                    <div class="col-md-9 col-xs-12 details">
                        <h2 class="jobd-title" style="width:100%"><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
 <?php if ($_smarty_tpl->tpl_vars['REMOTE_PORTAL']->value == 'deactivated') {
echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['in'];?>
 <?php echo $_smarty_tpl->tpl_vars['job']->value['location'];
}?> <?php if ($_smarty_tpl->tpl_vars['job']->value['new_flag']) {?><span class="new"><?php echo $_smarty_tpl->tpl_vars['translations']->value['job_detail_section']['new'];?>
</span><?php }?></h2>
                        <?php if (false) {?>
                            <?php if ($_smarty_tpl->tpl_vars['FAVORITES_PLUGIN']->value && $_smarty_tpl->tpl_vars['FAVORITES_PLUGIN']->value == 'true') {?>
                                <?php if ($_smarty_tpl->tpl_vars['favourites_job_ids']->value && in_array($_smarty_tpl->tpl_vars['job']->value['id'],$_smarty_tpl->tpl_vars['favourites_job_ids']->value)) {?>
                                    <span id="desk-favourites-block-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" ><a title="<?php echo $_smarty_tpl->tpl_vars['translations']->value['alljobs']['favourites_tooltip_remove'];?>
" href="#" onclick="return SimpleJobScript.removeFromFavourites(<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
, '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/img/', 'desk-');"><i class="fa fa-heart fa-lg ml10" aria-hidden="true"></i></a></span>
                                <?php } else { ?>
                                    <span id="desk-favourites-block-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" ><a title="<?php echo $_smarty_tpl->tpl_vars['translations']->value['alljobs']['favourites_tooltip_add'];?>
" href="#" onclick="return SimpleJobScript.addToFavourites(<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
, '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/img/', 'desk-');"><i class="fa fa-heart-o fa-lg ml10" aria-hidden="true"></i></a></span>
                                <?php }?>
                            <?php }?>
                        <?php }?>

                        <ul class="top-ul control-panel">
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-building" aria-hidden="true"></i>
                                </span> <?php echo $_smarty_tpl->tpl_vars['job']->value['company'];?>

                            </li>
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-braille" aria-hidden="true"></i>
                                </span> <?php echo $_smarty_tpl->tpl_vars['job']->value['job_type'];?>

                            </li>
                        </ul>
                        <ul class="control-panel">
                            <li data-toggle="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['translations']->value['detail_sidebar']['tooltip_email'];?>
" style="width: 50%">
                                <span>
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                </span>
                                <a class="undsc" data-toggle="modal" data-target="#emailModal" href="#" onclick="return false;">
                                    <?php echo $_smarty_tpl->tpl_vars['translations']->value['detail_sidebar']['control_email'];?>

                                </a>
                            </li>
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-id-card" aria-hidden="true"></i>
                                </span>Job ID: <?php echo 10000+$_smarty_tpl->tpl_vars['job']->value['id'];?>

                            </li>
                        </ul>
                        <ul class="control-panel">
                            <?php if ($_smarty_tpl->tpl_vars['REMOTE_PORTAL']->value == 'deactivated') {?>
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </span> <?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>

                            </li>
                            <?php }?>
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span> <?php echo $_smarty_tpl->tpl_vars['job']->value['post_date'];?>

                            </li>

                        </ul>
                        <ul class="control-panel">
                            <li style="width:100%">
                                <span>
                                    <i class="fa fa-certificate" aria-hidden="true"></i>
                                </span>Category: <?php echo $_smarty_tpl->tpl_vars['job']->value['category_name'];?>

                            </li>
                        </ul>

                        <ul class="control-panel">
                            <li style="width:100%">
                                <span>
                                    <i class="fa fa-circle" aria-hidden="true"></i>
                                </span>Position: <?php echo $_smarty_tpl->tpl_vars['job']->value['f9_position'];?>

                            </li>
                        </ul>                        
                        <div class="border-light"></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="detail-font" >
                                <p><?php echo $_smarty_tpl->tpl_vars['job']->value['description'];?>
</p>
                            </div>
                        </div>
                            <div class="border-light"></div>
                            <?php if ($_smarty_tpl->tpl_vars['job']->value['salary']) {?><p class="price-apply">Pay Rate: <?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
</p><?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['job']->value['apply_online'] == 1) {?>
                                    <?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value == 'true') {?>
                                        <?php $_smarty_tpl->_subTemplateRender("file:jobs/apply-existing-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                    <?php } else { ?>
                                        <?php $_smarty_tpl->_subTemplateRender("file:jobs/apply-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                    <?php }?>
                                <?php } else { ?>
                                    <div class="hta-p">username: <?php echo $_smarty_tpl->tpl_vars['SESSION_USERNAME']->value;?>
 applicant: <?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value;?>

                                        <a href="http://<?php echo $_smarty_tpl->tpl_vars['job']->value['apply_desc'];?>
" target="_blank">
                                            <button 
                                                type="button" 
                                                class="btn btn-apply" 
                                                id="apply_now"
                                            >
                                                <?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['apply_btn'];?>

                                            </button>
                                        </a>
                                    </div>
                                <?php }?>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 jd-ads-m">
                                <?php if (@constant('ADSENSE') == 'true') {?>
                                    <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['adsense_detail_rectangle']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                <?php }?>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12 co-name">
                        <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_confidential_ad'] == 0) {?>
                            <?php if ($_smarty_tpl->tpl_vars['job']->value['public_profile_flag'] == '1') {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['job']->value['company_detail_url'];?>
">
                                    <img class="co-logo" src="/<?php echo $_smarty_tpl->tpl_vars['job']->value['company_logo_path'];?>
" alt="company logo" />
                                </a>
                            <?php } else { ?>
                                <img class="co-logo" src="/<?php echo $_smarty_tpl->tpl_vars['job']->value['company_logo_path'];?>
" alt="company logo" />
                            <?php }?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_confidential_ad'] == 1) {?>
                            <img src="/_tpl/dds/img/f9_confidential_ad.png" class="similar-co-logo">
                        <?php }?>
                            <h2 class="co-title"><?php echo $_smarty_tpl->tpl_vars['job']->value['company'];?>
</h2>
                            <p class="co-summary">
                                <?php echo $_smarty_tpl->tpl_vars['job']->value['company_desc_excerpt'];?>

                                <?php if ($_smarty_tpl->tpl_vars['job']->value['public_profile_flag'] == '1') {?>
                                    <a 
                                        href="<?php echo $_smarty_tpl->tpl_vars['job']->value['company_detail_url'];?>
" 
                                        target="_blank">
                                            <button 
                                                type="button" 
                                                class="btn btn-more" >
                                                    <?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['more'];?>

                                            </button>
                                    </a>
                                <?php }?>

                            </p>
                            
                        </div>

                        <div class="col-md-3 col-xs-12">
                        <?php if (@constant('BANNER_MANAGER') == 'true') {?>
                            <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['banners_detail_rectangle']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                        <?php }?>	
                        </div>

                    </div>

                    <?php if (!$_smarty_tpl->tpl_vars['related_jobs']->value) {?>
                    <div class="row top-heading">
                        <h2 class="heading"><?php echo $_smarty_tpl->tpl_vars['translations']->value['detail_sidebar']['related_jobs_title'];?>
</h2>

                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['related_jobs']->value, 'job');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
?>

                            <div class="col-md-6 col-xs-12 similar-detail <?php if ($_smarty_tpl->tpl_vars['job']->value['spotlight'] == '1') {?>spotlight-related<?php }?>">
                            <div class="col-md-2 col-xs-12">
                                <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_confidential_ad'] == 1) {?>
                                    <img src="/<?php echo $_smarty_tpl->tpl_vars['job']->value['company_logo_path'];?>
" class="similar-co-logo">
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['job']->value['f9_confidential_ad'] == 0) {?>
                                    <img src="/_tpl/dds/img/f9_confidential_ad.png" class="similar-co-logo">
                                <?php }?>
                            </div>

                            <div class="col-md-10 col-xs-12">
                                <h3 class="similer-position"><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</h3>
                                
                                <ul class="similer-top-ul control-panel">
                                    <li class="similer-list-item"><span><i class="fa fa-building" aria-hidden="true"></i></span> <?php echo $_smarty_tpl->tpl_vars['job']->value['company'];?>
</li>
                                    <li class="similer-list-item"><span><i class="fa fa-braille" aria-hidden="true"></i></span> <?php echo $_smarty_tpl->tpl_vars['job']->value['job_type'];?>
</li>
                                </ul>
                                <ul class="similer-top-ul control-panel">
                            
                                    <?php if ($_smarty_tpl->tpl_vars['REMOTE_PORTAL']->value == 'deactivated') {?>
                                        <li class="similer-list-item"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span> <?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</li>
                                    <?php }?>
                                    <li class="similer-list-item"><span><i class="fa fa-calendar" aria-hidden="true"></i></span> <?php echo $_smarty_tpl->tpl_vars['job']->value['post_date'];?>
</li>
                                </ul>

                                <?php if ($_smarty_tpl->tpl_vars['job']->value['salary']) {?><p class="similer-price-apply"><?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
</p><?php }?>
                                <a rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['url_title'];?>
-<?php echo $_smarty_tpl->tpl_vars['job']->value['location_asci'];?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
">
                                    <button type="button" class="btn similer-btn-more"><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['more'];?>
</button>
                                </a>

                                <?php if ($_smarty_tpl->tpl_vars['FAVORITES_PLUGIN']->value && $_smarty_tpl->tpl_vars['FAVORITES_PLUGIN']->value == 'true') {?>
                                    <?php if (in_array($_smarty_tpl->tpl_vars['job']->value['id'],$_smarty_tpl->tpl_vars['favourites_job_ids']->value)) {?>
                                        <span id="desk-favourites-block-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" ><a title="<?php echo $_smarty_tpl->tpl_vars['translations']->value['alljobs']['favourites_tooltip_remove'];?>
" href="#" onclick="return SimpleJobScript.removeFromFavourites(<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
, '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/img/', 'desk-');"><i class="fa fa-heart fa-lg ml10 mt7" aria-hidden="true"></i></a></span>
                                     <?php } else { ?>
                                         <span id="desk-favourites-block-<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" ><a title="<?php echo $_smarty_tpl->tpl_vars['translations']->value['alljobs']['favourites_tooltip_add'];?>
" href="#" onclick="return SimpleJobScript.addToFavourites(<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
, '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/img/', 'desk-');"><i class="fa fa-heart-o fa-lg ml10 mt7" aria-hidden="true"></i></a></span>
                                     <?php }?>
                                 <?php }?>

                            </div>
                            </div>
                          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


                </div>
                <!-- related jobs -->
                <?php }?>

            </div>
        </div>

        <?php $_smarty_tpl->_subTemplateRender("file:snippets/listing-sitemap.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function() {
        SimpleJobScript.I18n = <?php echo $_smarty_tpl->tpl_vars['translationsJson']->value;?>
;
        SimpleJobScript.initApplyValidation();

        $('#cv').change(function() {
            var fname = $('input[type=file]').val().split('\\').pop();
            if( fname )
                $('#cvLabel').html(fname);
            else
                $('#cvLabel').html($('#cvLabel').html());
        });

        $('.popup').click(function(event) {
            var width  = 575,
                height = 400,
                left   = ($(window).width()  - width)  / 2,
                top    = ($(window).height() - height) / 2,
                url    = this.href,
                opts   = 'status=1' +
                         ',width='  + width  +
                         ',height=' + height +
                         ',top='    + top    +
                         ',left='   + left;
            
            window.open(url, 'twitter', opts);
            return false;
          });

    });

<?php echo '</script'; ?>
>

<?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value) {?>
    <?php echo '<script'; ?>
>
        localStorage.setItem('callbackJobURL', '');
    <?php echo '</script'; ?>
>
<?php } else { ?>
    <?php echo '<script'; ?>
>
        jobURL = window.location.href;
        localStorage.setItem('callbackJobURL', jobURL);
    <?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
