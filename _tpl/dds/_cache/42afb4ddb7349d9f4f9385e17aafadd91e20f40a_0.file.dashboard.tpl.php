<?php
/* Smarty version 3.1.30, created on 2020-01-06 08:33:12
  from "/home1/fninport/public_html/jobboard/_tpl/dds/dashboard/dashboard.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e13452849db71_04259512',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '42afb4ddb7349d9f4f9385e17aafadd91e20f40a' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/dashboard/dashboard.tpl',
      1 => 1577974245,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:dashboard/modals/applicant-modal.tpl' => 1,
    'file:dashboard/modals/preview-modal.tpl' => 1,
    'file:dashboard/modals/jobinfo-modal.tpl' => 1,
    'file:dashboard/modals/applications-modal.tpl' => 1,
    'file:dashboard/modals/match-applicant-modal.tpl' => 1,
    'file:dashboard/modals/deactivate-modal.tpl' => 1,
    'file:dashboard/modals/renew-modal.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5e13452849db71_04259512 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- dashboard.tpl -->
<div class="candidate-v2 mobileMT10">
  <div class="row">
      <div class="col-md-2 col-sm-12">
        <ul>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_SEARCHABLE']->value;?>
">
              <li id="search-li">
                <i class="fa fa-search" aria-hidden="true"></i>
                  SEARCH CANDIDATES
              </li>
            </a>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_POST']->value;?>
">
              <li id="search-li">
                <i class="fa fa-search" aria-hidden="true"></i>
                  POST A JOB +
              </li>
            </a>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_JOBS']->value;?>
">
              <li id="search-li">
                <i class="fa fa-search" aria-hidden="true"></i>
                  MY JOBS
              </li>
            </a>            
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_EDIT_COMPANY']->value;?>
">
              <li id="company-li">
                <i class="fa fa-vcard-o" aria-hidden="true"></i>
                  MY PRACTICE
              </li>
            </a>

            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_SETTINGS']->value;?>
">
              <li id="settings-li">
                <i class="fa fa-cogs" aria-hidden="true"></i>
                <?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['left_menu_settings'];?>

              </li>
            </a>

            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/URL_DASHBOARD_EMAIL_LOGS">
              <li>
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                CONTACTS
              </li>
            </a>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/URL_DASHBOARD_BANNED_JOB_SEEKER">
              <li>
                <i class="fa fa-cogs" aria-hidden="true"></i>
                BANNED JOB SEEKER
              </li>
            </a>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/URL_DASHBOARD_PREVIOUS_JOB_SEEKER">
              <li>
                <i class="fa fa-cogs" aria-hidden="true"></i>
                PREVIOUS JOB SEEKER
              </li>
            </a>
            <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_LOGOUT']->value;?>
"><li><i class="fa fa-sign-out" aria-hidden="true"></i>
                LOGOUT</li></a>

            <?php if ($_smarty_tpl->tpl_vars['PAYMENT_MODE']->value == '3') {?>
              <a href="/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_DASHBOARD_ACCOUNT']->value;?>
">
                <li id="account-li"><i class="fa fa-cubes" aria-hidden="true"></i>
                  <?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['left_menu_myaccount'];?>

                </li>
              </a>
            <?php }?>
         </ul>
        </div>

        <div class="col-md-10 col-sm-12">
         <div class="employer-dashboard">
          <div class="container">
            <?php $_smarty_tpl->_subTemplateRender("dashboard/views/".((string)$_smarty_tpl->tpl_vars['view']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

           </div>
          </div>
        </div>
  </div>

</div>

<?php if ($_smarty_tpl->tpl_vars['init_modal_popups']->value == '1') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/applicant-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['init_modal_popup_preview']->value == '1') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/preview-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['init_modal_popup_jobs']->value == '1') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/jobinfo-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['applications_modal_init']->value == '1') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/applications-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['applications_modal_init']->value == '2') {?>
  <div class="dash-modal">
    <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/match-applicant-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </div>
<?php }?>


<div class="dash-modal">
  <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/deactivate-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</div>

<div class="dash-modal">
  <?php $_smarty_tpl->_subTemplateRender("file:dashboard/modals/renew-modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
>
  localStorage.setItem("is_client", true);
  localStorage.setItem("is_logged_in", true);
<?php echo '</script'; ?>
><?php }
}
