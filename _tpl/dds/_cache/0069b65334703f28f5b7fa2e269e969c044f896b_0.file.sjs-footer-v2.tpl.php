<?php
/* Smarty version 3.1.30, created on 2020-01-06 09:32:31
  from "/home1/fninport/public_html/jobboard/_tpl/dds/1.5/layout/sjs-footer-v2.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e13530f69e961_84017891',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0069b65334703f28f5b7fa2e269e969c044f896b' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/1.5/layout/sjs-footer-v2.tpl',
      1 => 1573220551,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/dashboard-snippets.tpl' => 1,
    'file:1.5/layout/js-snippets.tpl' => 1,
  ),
),false)) {
function content_5e13530f69e961_84017891 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- _tpl/dds/1.5/layout/sjs-footer-v2.tpl -->
            <footer class="footer is-fixed-bottom" style="padding-top:40px; padding-bottom:50px;background-color:#ff9900">
                <div class="container">
                    <div class="columns">
                        <div class="column">
                            <p><?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
 &copy; <?php echo $_smarty_tpl->tpl_vars['YEAR']->value;?>
</p>
                        </div>
                    </div>
                </div>
            </footer>

            <?php if ($_smarty_tpl->tpl_vars['dashboard_footer_flag']->value) {?>
                <?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/dashboard-snippets.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/js-snippets.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php }?>
        </section>
    </body>
</html><?php }
}
