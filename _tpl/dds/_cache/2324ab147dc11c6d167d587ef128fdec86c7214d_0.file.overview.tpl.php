<?php
/* Smarty version 3.1.30, created on 2020-01-06 08:33:12
  from "/home1/fninport/public_html/jobboard/_tpl/dds/dashboard/views/overview.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e134528509c52_50357142',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2324ab147dc11c6d167d587ef128fdec86c7214d' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/dashboard/views/overview.tpl',
      1 => 1569867079,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e134528509c52_50357142 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- overview.tpl -->
<div class="row board">
  <h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['overview_headline'];?>
</h2>
  <p><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['welcome_message_overview'];?>
</p>
</div>

<div class="row activity">
  <h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['activity'];?>
</h3>

  <div class="col-md-6 col-xs-12 boxes">
   <h4><?php echo $_smarty_tpl->tpl_vars['overview_data']->value['jobs_posted'];?>
</h4>
   <h5 class="custom-h5"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['overview_jobs_posted'];?>
</h5>
 </div>

 <div class="col-md-6 col-xs-12 boxes">
   <h4><?php echo $_smarty_tpl->tpl_vars['overview_data']->value['applications'];?>
</h4>
   <h5>CANDIDATES FOR YOUR JOB</h5>
 </div>
</div>


<!--ad performance box-->
 <!--<div class="col-md-4 col-xs-12 boxes">
   <h4><?php echo $_smarty_tpl->tpl_vars['overview_data']->value['performance'];?>
 %</h4>
   <h5><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['overview_performance'];?>
</h5>
 </div>
</div>-->

<!--news-->
<!--<div class="row news mb40">
<h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['overview_news'];?>
</h3>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['news']->value, 'obj');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['obj']->value) {
?>
<h4><?php echo $_smarty_tpl->tpl_vars['obj']->value->date;?>
</h4>
<p><?php echo $_smarty_tpl->tpl_vars['obj']->value->msg;?>
</p>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div>--><?php }
}
