<?php
/* Smarty version 3.1.30, created on 2020-01-04 20:47:49
  from "/home1/fninport/public_html/jobboard/_tpl/dds/profile/changepass.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e114e55a4ecc1_22408614',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '783407580dbb3de75dbe0009be9f1ce9d7c01513' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/profile/changepass.tpl',
      1 => 1569867106,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e114e55a4ecc1_22408614 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- changepass -->
<div class="row">
    <div class="col-md-12 col-sm-12 mb20">
        <p class="profile-subheadline">
            <?php echo $_smarty_tpl->tpl_vars['translations']->value['profile']['passchange_desc'];?>

        </p>
    </div>
</div>

<form role="form" action="/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE']->value;?>
/pass-edited" method="post" >
    <div class="row">
        <div class="col-md-6 col-sm-12">

                    <p id='resultpass' style="color: black;font-size: 80%; width: auto;">Minimum 8-16 characters, 1 uppercase letter, 1 lowercase
            letter, and 1 number.</p>

                   <input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['new_pass'];?>
" name="pass1" id="pass1" maxlength="50" type="password" class="minput" required />
                   <p id="valpass" style="color: red; opacity: 0.75; font-size: 80%;margin-top: 10px;float: left;margin-left: 10px;"></p> 

                   <input placeholder="<?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['new_pass2'];?>
" name="pass2" id="pass2" maxlength="50" type="password" class="minput" required /> 

                    <div id="passrecovery-feedback-err" class="negative-feedback mt0 displayNone"><?php echo $_smarty_tpl->tpl_vars['translations']->value['login']['err_passes'];?>
</div>

        </div>

        <div class="col-md-6 col-sm-12">
            
        </div>

    </div>

    <div class="row mb50">
        <div class="col-md-6 col-sm-12 pushTop40">
            <button type="submit_forgotten_pass"  onclick="return SimpleJobScript.applicantPasswordValidation();" class="btn mbtn zeromlplLeft mt50" name="submit" id="submit" required><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>
        </div>
    </div>

    <style type="text/css">
        
   #dbtn
    {
    border-color: #f44336;
      background-color: #f44336;
      color: white;
    }

    #dbtn:hover
    {
      background-color: white;
      color: #f44336;
    }

    #valpass {
        margin-top: -10px!important;
        margin-bottom: 20px;
    }

    </style>

    <div class="row mb50">
        <div class="col-md-6 col-sm-12 pushTop40">
            <a class="greenLink" href="<?php echo BASE_URL;
echo $_smarty_tpl->tpl_vars['URL_PROFILE']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE_DELETE']->value;?>
">

                <button type="button"  class="btn mbtn zeromlplLeft" id="dbtn"><?php echo $_smarty_tpl->tpl_vars['translations']->value['profile']['delete_acc_label'];?>
</button>


            </a>
        </div>
    </div>
</form>

    <?php echo '<script'; ?>
 type="text/javascript">
        $(document).ready(function(){
            $('#pass1').on('change keydown paste input', function () {
                var $pass1 = this.value;
                console.log($pass1);
                validatePass($pass1);
            });

        function validatePass(pass) {
            var $result = $("#valpass");
            $result.text("");
            console.log('validatePass');
            var passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,16}$/;
            if (!passReg.test(pass)) {
                    $result.text("This password is Invalid");
                    $result.css({
                        "color": "red",
                        "font-size": "90%",
                        "font-weight": "bold"
                    });
                    $("#submit").attr("disabled", "disabled");
                }else{
                    $result.text("");
                    $result.css({
                        "color": "green",
                        "font-size": "80%"
                    });
                    $("#submit").removeAttr("disabled"); 
                }
            }
        });
    <?php echo '</script'; ?>
>

<?php }
}
