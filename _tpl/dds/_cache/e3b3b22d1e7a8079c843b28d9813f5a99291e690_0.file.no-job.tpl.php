<?php
/* Smarty version 3.1.30, created on 2020-01-06 08:55:55
  from "/home1/fninport/public_html/jobboard/_tpl/dds/err/no-job.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e134a7b5dbf43_60445499',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e3b3b22d1e7a8079c843b28d9813f5a99291e690' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/err/no-job.tpl',
      1 => 1569867088,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5e134a7b5dbf43_60445499 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="main-content nojob-margin">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 ttu">
				<h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['alljobs']['no_job'];?>
</h2>
				<i class="fa fa-safari" aria-hidden="true"></i>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOBS']->value;?>
"><h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['back'];?>
</h3></a> 
			</div>
		</div>
	</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
