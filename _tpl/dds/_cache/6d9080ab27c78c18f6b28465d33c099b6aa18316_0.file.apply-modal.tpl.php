<?php
/* Smarty version 3.1.30, created on 2020-01-06 08:31:14
  from "/home1/fninport/public_html/jobboard/_tpl/dds/jobs/apply-modal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e1344b29d2d14_40112983',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6d9080ab27c78c18f6b28465d33c099b6aa18316' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/jobs/apply-modal.tpl',
      1 => 1573481299,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e1344b29d2d14_40112983 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- apply-modal.tpl -->
<a data-toggle="modal" data-target="#applyModal" href="#" onclick="return false;"><button id="apply_online_now" type="button" data-toggle="modal" data-target="#jobpopup" class="btn btn-apply"><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['apply_btn'];?>
</button></a>
<br /><br />

<div class="modal fade trans-bg" id="jobpopup" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>	
				<div class="apply-popup">
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-xs-12 application-head">
								<h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['job_detail_section']['application_title'];?>
</h2>
								<p class="h-summary"><?php echo $_smarty_tpl->tpl_vars['translations']->value['job_detail_section']['application_desc'];?>
 </p>
							</div>
							<br><br>

							<div style="margin-left:auto;margin-right:auto;">

								<span>
								<a href="https://jobboard.ferret9.com/register-candidates">
								    <button id="new_user" type="button" class="btn btn-apply" style="display: inherit;">New User</button>
								</a>
								</span>
								<span>	
								<a href="https://jobboard.ferret9.com/login-candidates">
								    <button id="new_user" type="button" class="btn btn-apply" style="display: inherit;">Existing User</button>
								</a>
								</span>
							</div>

							
						


	



						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div><?php }
}
