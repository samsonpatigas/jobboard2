<?php
/* Smarty version 3.1.30, created on 2020-01-06 11:59:28
  from "/home1/fninport/public_html/jobboard/_tpl/dds/jobs/JOBS-LOOP.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e13758056a458_91268636',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1ec55c7e7329ebf3556489bf50d9537c88a83f53' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/jobs/JOBS-LOOP.tpl',
      1 => 1569867099,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e13758056a458_91268636 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- JOBS-LOOP.tpl -->


	
<div class="row latest-jobs search-result <?php if ($_smarty_tpl->tpl_vars['job']->value['spotlight'] == "1") {?>spotlight<?php }?>" style="padding-top: 20px; padding-bottom: 20px;">
	<div class="col-md-12 wish-list">
	
	</div>

	<?php if (!$_smarty_tpl->tpl_vars['COMPANY_JOB_LISTING']->value == '1') {?>
	<div class="col-md-1 colx-xs-12">
		<?php if ($_smarty_tpl->tpl_vars['job']->value['f9_confidential_ad'] == 1) {?>
		
			<?php if ($_smarty_tpl->tpl_vars['job']->value['public_profile_flag'] == '1') {?>
			<a href="<?php echo $_smarty_tpl->tpl_vars['job']->value['company_detail_url'];?>
" target="_blank">
				<div class="listing-logo">
					<img class="client-logo-border" src="/<?php echo $_smarty_tpl->tpl_vars['job']->value['company_logo_path'];?>
" alt="Company logo" />
				</div>
			</a>	
			<?php } else { ?>
				<div class="listing-logo">
					<img class="client-logo-border" src="/<?php echo $_smarty_tpl->tpl_vars['job']->value['company_logo_path'];?>
" alt="Company logo" />
				</div>
			<?php }?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['job']->value['f9_confidential_ad'] == 0) {?>
			<img class="client-logo-border" src="/_tpl/dds/img/f9_confidential_ad.png">
		<?php }?>
	</div>
	<?php }?>
	<br />

	<div class="col-md-7 colx-xs-12">

		<a rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['url_title'];?>
-<?php echo $_smarty_tpl->tpl_vars['job']->value['location_asci'];?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" >
			<h2 class="jobl-title"><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
 <?php if ($_smarty_tpl->tpl_vars['job']->value['new_flag']) {?><span class="new"><?php echo $_smarty_tpl->tpl_vars['translations']->value['job_detail_section']['new'];?>
</span><?php }?> </h2>
		</a>

		<ul>
			
			<li><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</li>
		</ul>
		<ul>
			<li><span><i class="fa fa-braille" aria-hidden="true"></i></span><?php echo $_smarty_tpl->tpl_vars['job']->value['job_type'];?>
</li>
			
			<li><span><i class="fa fa-calendar" aria-hidden="true"></i></span><?php echo $_smarty_tpl->tpl_vars['job']->value['created_on'];?>
</li>
		</ul>
	</div>
	<div class="col-md-4 colx-xs-12">

		<a rel="canonical" href="
			<?php if (!$_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value) {?>					
				<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_APPLICANTS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['url_title'];?>
-<?php echo $_smarty_tpl->tpl_vars['job']->value['location_asci'];?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>

			<?php } else { ?>
				<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['url_title'];?>
-<?php echo $_smarty_tpl->tpl_vars['job']->value['location_asci'];?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>

			<?php }?>
				">
			<button type="button" class="btn" style="width:45%">VIEW DETAILS</button>
		</a>

	</div>
	<div class="row" style="margin-top: 25px;">
		<div class="col-md-12 xol-xs-12">
				<a rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_JOB']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['url_title'];?>
-<?php echo $_smarty_tpl->tpl_vars['job']->value['location_asci'];?>
/<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" >
					
				</a>
		</div>
	</div>
</div>
<?php }
}
