<?php
/* Smarty version 3.1.30, created on 2020-01-06 11:59:28
  from "/home1/fninport/public_html/jobboard/_tpl/dds/auth/account-not-confirmed.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e137581008406_66229634',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aa9455f371598ad49e04ccc383ac072f443d194b' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/auth/account-not-confirmed.tpl',
      1 => 1578333563,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:jobs/JOBS-LOOP.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5e137581008406_66229634 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- /_tpl/dds/auth/account-not-confirmend.tpl -->
		<div class="action-req">
			<div class="container">
				<div class="row caution">
					<div class="col-md-12 col-xs-12">
						<div class="row">
							<div class="col-md-1 col-xs-12">
								<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
							</div>
							<div class="col-md-11 col-xs-12">
								<p>
								 <span><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['msg'];?>
. </span>
									<?php if ($_smarty_tpl->tpl_vars['APPLICANT_FLOW']->value == 'true') {?>
										<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['account_not_confirmed_text_applicants'];?>

										<br/><br/>
										You will then be able to apply for jobs listed on the job board
									<?php } else { ?>
										<?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['account_not_confirmed_text'];?>

									<?php }?>
								</p>
							</div>
						</div>
					</div>
				</div>


				<?php if ($_smarty_tpl->tpl_vars['APPLICANT_FLOW']->value == 'true') {?>
					<?php if ($_smarty_tpl->tpl_vars['more_jobs']->value) {?>

						<div class="col-md-12 col-xs-12 mt50 ml20-desk">
								<h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['alljobs']['all_jobs'];?>
</h2>
						</div>

						<div class="col-md-12 col-xs-12">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['more_jobs']->value, 'job', false, NULL, 'jobsLoop', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
?>
								<?php $_smarty_tpl->_subTemplateRender("file:jobs/JOBS-LOOP.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

						</div>

					<?php }?>
				<?php }?>
				
			</div>
		</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
