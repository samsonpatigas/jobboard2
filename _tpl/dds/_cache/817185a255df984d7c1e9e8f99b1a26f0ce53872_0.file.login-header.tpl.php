<?php
/* Smarty version 3.1.30, created on 2020-01-06 08:54:42
  from "/home1/fninport/public_html/jobboard/_tpl/dds/1.5/layout/login-header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e134a32d62627_62400367',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '817185a255df984d7c1e9e8f99b1a26f0ce53872' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/1.5/layout/login-header.tpl',
      1 => 1578071210,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e134a32d62627_62400367 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- /_tpl/dds/1.5/layout/login-header.tpl -->
<!doctype html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
	<title><?php if ($_smarty_tpl->tpl_vars['seo_title']->value) {
echo $_smarty_tpl->tpl_vars['seo_title']->value;
} else {
echo $_smarty_tpl->tpl_vars['html_title']->value;
}?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <meta name="description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>" />
  <meta name="keywords" content="<?php if ($_smarty_tpl->tpl_vars['seo_keys']->value) {
echo $_smarty_tpl->tpl_vars['seo_keys']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_keywords']->value;
}?>" />

    <!-- 
   Chrome and Opera Icons - to add support create your icon with different resolutions. Default is 192x192
     <link rel="icon" sizes="192x192" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav.png" >
    <link rel="icon" sizes="144x144" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-144.png" >
    <link rel="icon" sizes="96x96" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-96.png" >
    <link rel="icon" sizes="48x48" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-48.png" >
  -->
  <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav.png" >
  <!-- 
   Apple iOS icons
    <link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
  -->
  <link rel="apple-touch-icon" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-ios.png">
  <!-- iOS startup image -->
  <link rel="apple-touch-startup-image"  href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-startup.png">
  <!-- Apple additional meta tags -->
  <meta name="apple-mobile-web-app-title" content="<?php if ($_smarty_tpl->tpl_vars['seo_title']->value) {
echo $_smarty_tpl->tpl_vars['seo_title']->value;
} else {
echo $_smarty_tpl->tpl_vars['html_title']->value;
}?>">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <!-- Android web app capable -->
  <meta name="mobile-web-app-capable" content="yes">
  <!-- IE & Windows phone pin icons. Almost same size like ios so it is reused -->
  <meta name="msapplication-square150x150logo" content="fav-ios.png">

  <!-- facebook meta tags for sharing -->
  <meta property="og:locale" content="en_US" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php if ($_smarty_tpl->tpl_vars['seo_title']->value) {
echo $_smarty_tpl->tpl_vars['seo_title']->value;
} else {
echo $_smarty_tpl->tpl_vars['html_title']->value;
}?>" />
  <meta property="og:description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>" />
  <meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
" />
  <meta property="og:site_name" content="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
" />
  <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
share-image.png" />

  <!-- twitter metatags -->
  <meta name="twitter:card" content="summary_large_image"/>
  <meta name="twitter:description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>"/>
  <meta name="twitter:title" content="<?php if ($_smarty_tpl->tpl_vars['seo_title']->value) {
echo $_smarty_tpl->tpl_vars['seo_title']->value;
} else {
echo $_smarty_tpl->tpl_vars['html_title']->value;
}?>"/>

  <meta name="twitter:domain" content="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
"/>
  <meta name="twitter:image" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
share-image.png" />

  <!-- RSS -->
  <link rel="alternate" type="application/rss+xml" title="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
 RSS Feed" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
rss">
  

  <!-- CSS -->

  <!-- Attached CSS -->
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/style.css">
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/reset.css">
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/font-awesome.min.css">

  <link href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/tags/tagl/assets/css/taggle.css" rel="stylesheet">
  <link href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/css/dev.css" rel="stylesheet">

  <noscript><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['noscript_msg'];?>
</noscript>
  
  <style type="text/css">
  <?php echo $_smarty_tpl->tpl_vars['custom_css']->value;?>

  </style>

  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/js/jquery.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/js/bootstrap.min.js"><?php echo '</script'; ?>
>


  <?php if ($_smarty_tpl->tpl_vars['INDEED']->value == 'activated') {?>
  <?php echo '<script'; ?>
 type="text/javascript" src="https://gdc.indeed.com/ads/apiresults.js"><?php echo '</script'; ?>
>         
  <?php }?>

  <?php echo '<script'; ?>
 type="text/javascript">
    current_url = "<?php echo $_SERVER['HTTP_HOST'];
echo $_SERVER['REQUEST_URI'];?>
";
    if(current_url == "https://jobboard.ferret9.com/login-recruiters"){

    }
  <?php echo '</script'; ?>
>
</head>

<body>
<div class="page-loader"></div>

<!-- enclosed in the footer -->
<div id="wrapper">
  <div id="headerLogo" style="width: 100%; background-color:#ffb000e0;">
    <div class="row">
      <div class="col-md-4 col-sm-3 col-xs-12" style="margin-top: 20px; padding-left: 32px;">
        <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_SIGN_UP']->value;?>
"><i class="fa fa-chevron-circle-left"></i>&nbsp; <?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['back'];?>
</a>
      </div>
      <div class="col-md-8 col-sm-7 col-xs-12">
        <img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SITE_LOGO_PATH']->value;?>
" style="width: 300px;">
      </div>
    </div>
  </div>

  

<?php if ($_smarty_tpl->tpl_vars['PLAIN_SITE_REGISTER']->value == 'true') {?>

<?php }
}
}
