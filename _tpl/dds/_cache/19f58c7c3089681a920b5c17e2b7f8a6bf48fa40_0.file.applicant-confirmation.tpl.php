<?php
/* Smarty version 3.1.30, created on 2020-01-06 11:33:48
  from "/home1/fninport/public_html/jobboard/_tpl/dds/auth/applicant-confirmation.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e136f7c7c4b30_39008352',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '19f58c7c3089681a920b5c17e2b7f8a6bf48fa40' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/auth/applicant-confirmation.tpl',
      1 => 1578326587,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5e136f7c7c4b30_39008352 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- /_tpl/dds/auth/applicant-confirmation.tpl -->
<div class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['acc_confirmation'];?>
</h2>

				<?php if ($_smarty_tpl->tpl_vars['confirm_result']->value) {?>
					<i class="fa fa-check" aria-hidden="true"></i>
					<h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['hash_confirmation_successful'];?>
</h3>
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_PROFILE']->value;?>
">
						<button type="button" class="btn"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['sign_in'];?>
</button>
					</a>

				<?php } else { ?>
					<i class="fa fa-times" aria-hidden="true"></i>
					<h3><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['hash_confirmation_failed'];?>
</h3>
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_REGISTER_APPLICANTS']->value;?>
">
					 <button type="button" class="btn"><?php echo $_smarty_tpl->tpl_vars['translations']->value['registration']['new_acc'];?>
</button>
					</a>
				<?php }?>

			</div>
		</div>
	</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
