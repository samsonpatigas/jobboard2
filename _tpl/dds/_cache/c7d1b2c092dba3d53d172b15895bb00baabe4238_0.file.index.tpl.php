<?php
/* Smarty version 3.1.30, created on 2020-01-06 09:32:31
  from "/home1/fninport/public_html/jobboard/_tpl/dds/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e13530f68f774_60694285',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c7d1b2c092dba3d53d172b15895bb00baabe4238' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/index.tpl',
      1 => 1578062160,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/listing-sitemap.tpl' => 1,
    'file:1.5/layout/sjs-footer-v2.tpl' => 1,
  ),
),false)) {
function content_5e13530f68f774_60694285 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- _tpl/index.tpl -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Desert Dental Staffing | Hot Jobs</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <meta name="description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>" />
    <meta name="keywords" content="<?php if ($_smarty_tpl->tpl_vars['seo_keys']->value) {
echo $_smarty_tpl->tpl_vars['seo_keys']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_keywords']->value;
}?>" />


    <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav.png" >

    <link rel="apple-touch-icon" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-ios.png">
    <!-- iOS startup image -->
    <link rel="apple-touch-startup-image"  href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
fav-startup.png">
    <!-- Apple additional meta tags -->
    <meta name="apple-mobile-web-app-title" content="Desert Dental Staffing | Hot Jobs">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <!-- Android web app capable -->
    <meta name="mobile-web-app-capable" content="yes">
    <!-- IE & Windows phone pin icons. Almost same size like ios so it is reused -->
    <meta name="msapplication-square150x150logo" content="fav-ios.png">

    <!-- facebook meta tags for sharing -->
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Desert Dental Staffing | Hot Jobs" />
    <meta property="og:description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>" />
    <meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
" />
    <meta property="og:site_name" content="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
" />
    <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
share-image.png" />

    <!-- twitter metatags -->
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
} else {
echo $_smarty_tpl->tpl_vars['meta_description']->value;
}?>"/>
    <meta name="twitter:title" content="Desert Dental Staffing | Hot Jobs"/>

    <meta name="twitter:domain" content="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
"/>
    <meta name="twitter:image" content="<?php echo $_smarty_tpl->tpl_vars['MAIN_URL']->value;?>
share-image.png" />
    
    <!-- RSS -->
    <link rel="alternate" type="application/rss+xml" title="<?php echo $_smarty_tpl->tpl_vars['SITE_NAME']->value;?>
 RSS Feed" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
rss">
 
    <link rel="canonical" href="http://<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
home" />
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
    <?php echo '<script'; ?>
 defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://unpkg.com/axios/dist/axios.min.js"><?php echo '</script'; ?>
>

    <noscript><?php echo $_smarty_tpl->tpl_vars['translations']->value['website_general']['noscript_msg'];?>
</noscript>

<style type="text/css">
<?php echo $_smarty_tpl->tpl_vars['custom_css']->value;?>


div.header { background-image: url('uploads/images/orange.png') !important; }

.bg-dds-home {
  background: url('uploads/images/orange.png') no-repeat center center fixed; 
  background-size: cover;
}

.job-grid {
  padding-top: 20px;
  border-bottom: 1px;
  border-bottom-color: violet;
  border-bottom-style: solid;
}

.details-btn-container {
  justify-content: space-evenly;
  flex-direction: column;
  display: flex;
  text-align: end;
}

.details-btn-container .btn-holder {
  justify-content: flex-end;
  display: flex;
}

.btn-theme-color {
  background-color: #a533a5 !important
  color:white;
}

.client-logo-border {
  padding: 5px;
  border-color: #9f2b1942;
  border-width: 1px;
  border-style: solid;
}

.new-job-indicator {
  padding:2px;
  border-color: orange;
  border-width: 1px;
  border-style: solid;
}
</style>

</head>

<body>
  <header>
    <nav class="navbar is-fixed-top" style="background-color:#ffb000e0">
      <div class="navbar-brand">
        <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
">
          <img class="site-logo" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SITE_LOGO_PATH']->value;?>
" style="width: 243px;margin: 19px 25px 10px 25px;" alt="Desert Dental Staffing">
        </a>
      </div>
      <div id="navMenuExamplet" class="navbar-menu">
        <div class="navbar-end">
			<div class="navbar-item is-hoverable">
				<a class="navbar-item is-active is-hoverable" href="http://www.desertdentalstaffing.com/">
					Home
				</a>
			</div>
          <?php if ($_smarty_tpl->tpl_vars['SESSION_USERNAME']->value || $_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value) {?>
            <div class="navbar-item">
              <a class="navbar-link is-active" href="/profile/logout">
                Logout
              </a>            
            </div>
          <?php } else { ?>
            <div class="navbar-item has-dropdown is-hoverable">
              <a class="navbar-link  is-active" href="#">
                Sign In
              </a>
              <div class="navbar-dropdown is-boxed">
                <a class="navbar-item " href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_RECRUITERS']->value;?>
">
                  Client
                </a>
                <a class="navbar-item " href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_LOGIN_APPLICANTS']->value;?>
">
                  Job Seeker
                </a>
              </div>
            </div>
          <?php }?>
          <div class="navbar-item">
            <div class="field is-grouped">
              <p class="control">
                <a class="button is-primary" href="https://jobboard.ferret9.com/sign-up" style="">
                  <span class="icon">
                    <i class="fa fa-download"></i>
                  </span>
                  <span>New User Registration</span>
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </header>
  <section class="section">

  <section class="hero bg-dds-home is-bold">
    <div class="hero-body" style="padding-top: 100px; padding-bottom:100px;">
      <div class="container">
        <h1 class="title has-text-centered has-text-white">
          Jobboard
        </h1>
      </div>
    </div>
  </section>
<!-- ///////////// MAIN-CONTENT ///////////// -->
<div class="container"  id="app" style="min-height: 500px;">
  <div class="columns">
    <form action="" >
      <div class="box is-narrow" style="width: 250px;">
        <div style="padding-top: 10px;">
          <div class="is-size-5 has-text-weight-bold">Positions</div>                    
          <div v-for="pList in Positions_List">            
            <div>
              <label class="checkbox">
                <input 
                  type="checkbox" 
                  v-model="Positions" 
                  @change="sendViaAjax"
                  checked="checked" 
                  id="Positions" 
                  name="Positions" 
                  :value="pList" 
                />
                  %% pList %%
              </label>        
            </div>            
          </div>
        </div>
      </div>
    </form>
    <div class="column">
        <?php if ($_smarty_tpl->tpl_vars['more_jobs']->value) {?>
          
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['customizer_data']->value['listings_on_flag'] == '1') {?>
          <?php $_smarty_tpl->_subTemplateRender("file:snippets/listing-sitemap.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php }?>
      <div v-if="Jobs">
        <ul>
          <li v-for="job in displayedPosts">
            <div class="container">
              <div class="job-grid">
                <div class="columns">                      
                  <div class="column is-narrow" style="width: 110px;">
                    <img class="client-logo-border" v-bind:src="job.job_info.company_logo_path" style="height: 80px; width:auto;" v-if="job.job_info.f9_confidential_ad == 0"/>
                    <img class="client-logo-border" style="height: 80px; width:auto;" src="/_tpl/dds/img/f9_confidential_ad.png" v-if="job.job_info.f9_confidential_ad == 1">
                  </div>
                  <div class="column is-4 has-text-grey is-size-7">
                    <a rel="canonical" v-bind:href="'/job/' + job.job_info.url_title + '/' + job.job_info.id" >
                        <h2 style="display: inline-flex" class="is-size-6 has-text-weight-bold">%% job.job.f9_position %%</h2>
                        <span v-if = "job.job_info.new_flag">
                            <span class="new-job-indicator">New</span>
                        </span>
                    </a>
                    <ul>
                        <li>Job ID: %% 10000 + parseInt(job.job.id) %%</li>
                        <li>Position Type: %% job.job.f9_position_type %%</li>
                    </ul>
                  </div>
                  <div class="column is-4 has-text-grey is-size-7">
                    <ul>
                      <li><span class="tag is-primary">%% job.job.city_name %%</span></li>
                    </ul>
                  </div>
                  <div class="column details-btn-container has-text-grey is-size-7">
                    <div class="btn-holder">
                      <a v-bind:href="job.job.btn_link" class="button btn-theme-color" 
                        style="background-color: #0089A6; color: white; height: 45px;">
                          VIEW DETAILS / APPLY
                      </a>
                    </div>
                    <div class="btn-holder" style="display: none">
                      <button class="button is-link" @click="applicant_apply(job.job.btn_link)">Link</button>
                    </div>                    
                    <ul>
                      <li>Posted %% job.job_info.created_on %%</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>          
          </li>
        </ul>
      </div> 
      <div v-else="!Jobs">
        <h1 class="is-size-3">No Jobs to Show</h1>
      </div>
    </div>
    <nav>
      <ul class="pagination">
        <li class="page-item">
          <button type="button" @click="prevPage"> Previous </button>
        </li>
        <li class="page-item">
          <button type="button" @click="nextPage"> Next </button>
        </li>
      </ul>
    </nav>    
  </div>

</div>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/1.5/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/landing/landing.js"><?php echo '</script'; ?>
>

<?php if ($_smarty_tpl->tpl_vars['SESSION_USERNAME']->value || $_smarty_tpl->tpl_vars['SESSION_APPLICANT']->value) {?>
  <?php echo '<script'; ?>
 type="text/javascript">
    localStorage.setItem('is_logged_in', true);
    console.log('logged in')
  <?php echo '</script'; ?>
>
<?php } else { ?>
  <?php echo '<script'; ?>
 type="text/javascript">
    localStorage.setItem('is_logged_in', false);
    localStorage.setItem('is_client', false);
    console.log('JS: not logged in')
  <?php echo '</script'; ?>
>
<?php }
if (isset($_smarty_tpl->tpl_vars['_SESSION']->value['applicant'])) {?>
  <?php echo '<script'; ?>
 type="text/javascript">
    localStorage.setItem('is_applicant', false);
  <?php echo '</script'; ?>
>  
<?php }?>

<?php echo '<script'; ?>
 type="text/javascript">
  $('.counter').each(function() {
  var $this = $(this),
  countTo = $this.attr('data-count');

  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },
  {
    duration: 10000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
    }
  });  
});


init_is_logged_in = localStorage.getItem('is_logged_in');
init_currentUser = localStorage.getItem('currentUser');

var app = new Vue({
    delimiters: ['%%', '%%'],
    el: '#app',
    data: {
        Jobs:[],
        Cities: 
          {
            "86":"North Phoenix",
            "74":"East Valley",
            "89":"General Phoenix",
            "99":"North Arizona",
            "85":"Scottsdale",
            "87":"Tucson",
            "84":"West Valley" 
          },
        Positions:
          [
            "Dental Assistant",
            "Hygienist",
            "Front Office",
            "Cross-trained (Front Office/Dental Assistant)",
            "Dentist"
          ],
        Positions_List:
          [
            "Dental Assistant",
            "Hygienist",
            "Front Office",
            "Cross-trained (Front Office/Dental Assistant)",
            "Dentist"
          ],
        Page: 1,
        perPage: 5,
        pages: [],       
        is_logged_in: init_is_logged_in,
        currentUser: init_currentUser
    },
    mounted: function(){
      is_logged_in = localStorage.getItem('is_logged_in');
      currentUser = localStorage.getItem('currentUser');
      this.sendViaAjax();
    },
    methods: {
      sendViaAjax: function(){
        axios.post('/api_home_job_listing.php', {
          'Positions': this.Positions,
          'is_logged_in': this.is_logged_in,
          'currentUser': this.currentUser
        })
        .then(function (response) {
            app._data.Jobs = response.data;
            for(x=0;x<app._data.Jobs.length;x++){
              app.Jobs[x].job.city_name = app.Cities[app.Jobs[x].job.city_id];
              console.log(is_logged_in);

              if (is_logged_in){
                console.log('sendViaAjax logged in');
                app.Jobs[x].job.url_redirect = app.Jobs[x].job_info.url_title;
              }else{
                console.log('sendViaAjax not logged in: ' + app.Jobs[x].job_info.id);
                 app.Jobs[x].job.url_redirect = '/login_candidates/' + app.Jobs[x].job_info.id;
              }
            }
        })
        .catch(function (error) {
            console.log(error)
        });
        app.Page = 1;
      },
      setPages () {
        let numberOfPages = Math.ceil(app.Jobs.length / app.perPage);
        for (let index = 1; index <= numberOfPages; index++) {
          app.pages.push(index);
        }
      },      
      paginate () {
        let page = app.Page;
        let perPage = app.perPage;
        let from = (app.Page * app.perPage) - app.perPage;
        let to = (app.Page * app.perPage);
        return  app.Jobs.slice(from, to);
      },
      nextPage () {
        console.log('nextPage: ' + app.Page);
        if (app.Jobs.length / app.perPage > app.Page){
          app.Page = app.Page + 1;
        }
      },
      prevPage () {
        console.log('prevPage: ' + app.Page);
        if (app.Page > 1) {
          app.Page = app.Page - 1;
        }
      },
      applicant_apply (applicant_id) {
        alert('apply: ' + applicant_id);
        localStorage.setItem('job_id', applicant_id);
      }
    },
    computed:{
      displayedPosts () {
        return app.paginate();
      }
    },
    watch: {
      posts () {
        this.setPages();
      }
  },

});
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    localStorage.setItem('callbackJobURL', '');
<?php echo '</script'; ?>
>


<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer-v2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</body>
</html><?php }
}
