<?php
/* Smarty version 3.1.30, created on 2020-01-06 09:14:22
  from "/home1/fninport/public_html/jobboard/_tpl/dds/profile/profile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e134ecedf9882_23077616',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3ccdf2768fec3de31b3e067a18342217ffd57a5b' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/profile/profile.tpl',
      1 => 1578323648,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:1.5/layout/sjs-header.tpl' => 1,
    'file:profile/".((string)$_smarty_tpl->tpl_vars[\'ACTIVE\']->value).".tpl' => 1,
    'file:1.5/layout/sjs-footer.tpl' => 1,
  ),
),false)) {
function content_5e134ecedf9882_23077616 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- /_tpl/dds/profile/profile.tpl -->
<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="candidate-v2">
	<div class="row">

		<div class="col-md-2 col-sm-12">
			<ul>
				<a href="/">
					<li><i class="fa fa-user-circle" aria-hidden="true"></i>
						JOB BOARD
					</li>
				</a>
				<a href="/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE_EDIT']->value;?>
">
					<li class="<?php if ($_smarty_tpl->tpl_vars['ACTIVE']->value == $_smarty_tpl->tpl_vars['PROFILE_ROUTING_ARR']->value[$_smarty_tpl->tpl_vars['URL_PROFILE_EDIT']->value]) {?>active<?php }?>"><i class="fa fa-user-circle" aria-hidden="true"></i> 
						<?php echo $_smarty_tpl->tpl_vars['translations']->value['profile']['menu_edit'];?>

					</li>
				</a>
				<!--
				<a href="/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE_APPLICATIONS']->value;?>
">
					<li class="<?php if ($_smarty_tpl->tpl_vars['ACTIVE']->value == $_smarty_tpl->tpl_vars['PROFILE_ROUTING_ARR']->value[$_smarty_tpl->tpl_vars['URL_PROFILE_APPLICATIONS']->value]) {?>active<?php }?>"><i class="fa fa-address-card-o" aria-hidden="true"></i>
						<?php echo $_smarty_tpl->tpl_vars['translations']->value['profile']['menu_apps'];?>

					</li>
				</a>
				-->
				<a href="/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE_CHANGEPASSWORD']->value;?>
">
				    	<li class="<?php if ($_smarty_tpl->tpl_vars['ACTIVE']->value == $_smarty_tpl->tpl_vars['PROFILE_ROUTING_ARR']->value[$_smarty_tpl->tpl_vars['URL_PROFILE_CHANGEPASSWORD']->value]) {?>active<?php }?>"><i class="fa fa-gears" aria-hidden="true"></i>
				    		<?php echo $_smarty_tpl->tpl_vars['translations']->value['profile']['menu_pass'];?>

				    	</li>
				</a>				
				<a href="/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE']->value;?>
/logout">
					<li>
						<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
						<?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['logout_label'];?>

					</li>
				</a>

			</ul>
			<?php if (@constant('BANNER_MANAGER') == 'true') {?>
				<?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['banners_cdb']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

			<?php }?>
		</div>

        <div class="col-md-10 col-sm-12">
         <div class="employer-dashboard">
	          <div class="container">
	         	 <div class="row board">
                    <h2><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h2>
                    
                    <?php $_smarty_tpl->_subTemplateRender("file:profile/".((string)$_smarty_tpl->tpl_vars['ACTIVE']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


                 </div>
	     	  </div>
   		 </div>
  		</div>

	</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:1.5/layout/sjs-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
 type="text/javascript">
	localStorage.setItem("is_applicant", true);
	localStorage.setItem("is_logged_in", true);
<?php echo '</script'; ?>
>
<?php }
}
