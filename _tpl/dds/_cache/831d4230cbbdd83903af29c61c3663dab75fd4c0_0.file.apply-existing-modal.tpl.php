<?php
/* Smarty version 3.1.30, created on 2020-01-06 08:55:54
  from "/home1/fninport/public_html/jobboard/_tpl/dds/jobs/apply-existing-modal.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e134a7a556374_07470720',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '831d4230cbbdd83903af29c61c3663dab75fd4c0' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/_tpl/dds/jobs/apply-existing-modal.tpl',
      1 => 1575393517,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e134a7a556374_07470720 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- /_tpl/dds/jobs/apply-existing-modal.tpl -->
<style type="text/css">
.tag {
  background-color:white;
  color: black;
  padding-top: 0px;
  padding-bottom: 0px;
  display: grid;
}
.pTextArea {
	border-top-width: 1px;
    border-right-width: 1px;
    border-bottom-width: 1px;
    border-left-width: 1px;
    border-style: solid;
}
</style>
<form role="form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_APPLY']->value;?>
" enctype="multipart/form-data" >
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: left; padding-top: 10px; padding-left: 0px;">
	<label style="font-weight: bold">Comment:</label><br>
	<textarea 
		style="border-top-width: 10px; border-radius: 5px; margin-top: 15px; padding-top: 10px; paddiing-left: 5px;" 
		required id="apply_comments" 
		name="apply_comments" 
		maxlength="500" 
		rows="10" 
		cols="100" 
		placeholder="Leave a message...">
	</textarea>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: left; padding-top: 10px; padding-left: 0px;">
Want to amend your details? You can do so by <a href="https://jobboard.ferret9.com/profile/edit">changing your profile</a>
<button style="margin-left: 0px;margin-top: 20px;" type="submit" class="btn btn-modal-a" onclick="return SimpleJobScript.Apply(<?php echo $_smarty_tpl->tpl_vars['MAX_CV_SIZE']->value;?>
, 'existing');"><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['submit'];?>
</button>
</div>

<div style="text-align: left">
	
</div>
	<input type="hidden" name="job_id" id="job_id" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" />
	<input type="hidden" name="new_user" id="new_user" value="0" />
	<input type="hidden" name="employer_id" id="employer_id" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['employer_id'];?>
" />
	
</form>
<br /><br />

<div class="modal fade trans-bg" id="jobpopup" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>	
				<div class="apply-popup">
					<div class="container">
						<div class="row ">
							<div class="col-md-12 col-xs-12 application-head">
								<h2><?php echo $_smarty_tpl->tpl_vars['translations']->value['job_detail_section']['application_title'];?>
</h2>
								<p class="h-summary"><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['amend_text'];?>
<a href="/<?php echo $_smarty_tpl->tpl_vars['URL_PROFILE']->value;?>
/edit" target="_blank" class="green-link"><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['amend_text2'];?>
</a> <?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['amend_text3'];?>
 </p>
							</div>
							<br><br>
							<div id="new-user">
							  <form role="form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['URL_APPLY']->value;?>
" enctype="multipart/form-data" >
									<input type="hidden" name="job_id" id="job_id" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" />
									<input type="hidden" name="new_user" id="new_user" value="0" />

									<div class="row mlpl0">
										<div class="col-md-6 col-xs-12 pushTop40 mb50 tal ls25">
											  <?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT_NAME']->value) {?>
											  <label style="font-weight: bold">Name:</label>
											  <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_NAME']->value;?>
</div>
											  <?php }?>

											  <?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT_EMAIL']->value) {?>
											  <label style="font-weight: bold">Email:</label>
											  <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_EMAIL']->value;?>
</div>
											  <?php }?>

											  <?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT_PHONE']->value) {?>
											  <label style="font-weight: bold">Cell Phone:</label>
											  <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_PHONE']->value;?>
</div>
											  <?php }?>

											  <?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT_ADDRESS']->value) {?>
											  <label style="font-weight: bold">Street Address:</label>
											  <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_ADDRESS']->value;?>
</div>
											  <?php }?>

											  <?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT_EXPERIENCE']->value) {?>
											  <label style="font-weight: bold">Experience:</label>
											  <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_EXPERIENCE']->value;?>
</div>
											  <?php }?>

											  <?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT_SKILLS']->value) {?>
											  <label style="font-weight: bold">Skills:</label>
											  <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_SKILLS']->value;?>
</div>
											  <?php }?>

											  <hr />
											  <?php if ($_smarty_tpl->tpl_vars['SESSION_APPLICANT_PORTFOLIO']->value) {?>
											 <div><a href="<?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_PORTFOLIO_LINK']->value;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_PORTFOLIO']->value;?>
</a></div>
											  <?php }?>

											  <div>
											 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['SESSION_APPLICANT_SM_LINKS']->value, 'SM_OBJ');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['SM_OBJ']->value) {
?>
												 <a class="mr12" href="<?php if ($_smarty_tpl->tpl_vars['SM_OBJ']->value->whatsapp == 'true') {?>tel:<?php echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->whatsapp_numb;
} else {
echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->linkToShow;
}?>" target="_blank"><i class="fa fa-<?php echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->icon;?>
 fa-lg" aria-hidden="true"></i></a>
											 <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

											 </div>

										</div>


										<div class="col-md-6 col-xs-12 pushTop40 mb50 tal ls25">
											 <label style="font-weight: bold">Message:</label>
											 <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_MESSAGE']->value;?>
</div>

											 <label style="font-weight: bold">Resume:</label>
											 <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_CVTEXT']->value;?>
</div> 
											 <div style="margin-left: 20px"><?php echo $_smarty_tpl->tpl_vars['SESSION_APPLICANT_PHOTO']->value;?>
</div> 
											 <hr />
											 <div>
											 	<label style="font-weight: bold">Comment:</label><br>
												<textarea class="noTinymceTA minput opaque pTextArea" required id="apply_comments" name="apply_comments" maxlength="500" rows="3" cols="100" placeholder="Leave a message..."></textarea>
											 </div>
										</div>
								</div> 

								<div class="row mlpl0">
									<div class="col-md-6 col-xs-12 mlpl0">
										<button type="submit" class="btn btn-modal-a" onclick="return SimpleJobScript.Apply(<?php echo $_smarty_tpl->tpl_vars['MAX_CV_SIZE']->value;?>
, 'existing');"><?php echo $_smarty_tpl->tpl_vars['translations']->value['apply']['submit'];?>
</button>
										<button type="button" data-dismiss="modal" class="btn btn-modal-c"><?php echo $_smarty_tpl->tpl_vars['translations']->value['job_detail_section']['close'];?>
</button>
									</div>
									<div class="col-md-3 col-xs-12">
										<div class="modal-ajax" >
											<i id="fspinner_apply" class="displayNone fa fa-refresh fa-spin fa-lg fa-fw refresh-spinner"></i>
										</div>
									</div>
								</div>
							  </form>
				
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div><?php }
}
