<!-- JOBS-LOOP-v2.tpl -->
<div class="container">
    <div class="job-grid">
        <div class="columns">
            {if not $COMPANY_JOB_LISTING == '1'}
                <div class="column is-narrow">
                        {if  $job.f9_confidential_ad == 1}
                            <img class="client-logo-border" src="/{$job.company_logo_path}" alt="Client logo" />
                        {/if}
                        {if $job.f9_confidential_ad == 0}
                            <img class="client-logo-border" style="height: 80px; width:auto;" src="/_tpl/dds/img/f9_confidential_ad.png ">
                        {/if}
                </div>
                <div class="column is-4 has-text-grey is-size-7">
                        <a rel="canonical" href="{$BASE_URL}{$URL_JOB}/{$job.url_title}-{$job.location_asci}/{$job.id}" >
                            <span><h2 style="display: inline-flex" class="is-size-6 has-text-weight-bold">{$job.title}</h2><span>{if $job.new_flag}<span class="new-job-indicator">{$translations.job_detail_section.new}</span>{/if}</span>
                        </a>
                        <ul>
                            <li>{$job.job_type}</li>
                            <li>{$job.location}</li>
                        </ul>
                </div>
                <div class="column details-btn-container has-text-grey is-size-7">
                    <ul>
                        <li>Posted {$job.created_on}</li>
                    </ul>
                    <div class="btn-holder">
                        <a href="{$BASE_URL}{$URL_JOB}/{$job.url_title}-{$job.location_asci}/{$job.id}" class="button btn-theme-color">VIEW DETAILS</a>
                    </div>
                </div>
            {/if}
        </div>
    </div>
</div>
