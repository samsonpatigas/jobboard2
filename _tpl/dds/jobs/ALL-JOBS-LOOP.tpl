<!-- all-jobs-loop.tpl -->
<div class="row search-result {if $job.spotlight == "1"}spotlight{/if}" style="padding-top: 30px; padding-bottom: 30px">
	{if not $COMPANY_JOB_LISTING == '1'}
		<div class="col-md-1 colx-xs-12">
				{if $job.public_profile_flag == '1'}
				<a href="{$job.company_detail_url}" target="_blank">
					<div class="listing-logo">
						<img class="client-logo-border" src="/{$job.company_logo_path}" alt="Company logo" />
					</div>
				</a>	
				{else}
					<div class="listing-logo">
						<img class="client-logo-border" src="/{$job.company_logo_path}" alt="Company logo" />
					</div>
				{/if}
		</div>
	{/if}

	<div class="col-md-6 colx-xs-12" >
		
		<a rel="canonical" href="{$BASE_URL}{$URL_JOB}/{$job.url_title}-{$job.location_asci}/{$job.id}" >
			<h2 class="jobl-title">{$job.title} (Job ID: {$job.id}){if $job.new_flag}<span class="new">{$translations.job_detail_section.new}</span>{/if} </h2>
		</a>

		<ul>
			<li><i class="fa fa-building" aria-hidden="true"></i>{$job.company}</li>
			<li><i class="fa fa-braille" aria-hidden="true"></i>{$job.job_type}</li>
		</ul>
		<ul>
			<li><i class="fa fa-map-marker" aria-hidden="true"></i>{$job.location}</li>
			<li><i class="fa fa-calendar" aria-hidden="true"></i>{$job.created_on}</li>
		</ul>
	</div>
	<div class="col-md-5 col-xs-12">
		<a rel="canonical" href="
			{if !$SESSION_APPLICANT}
				{$BASE_URL}{$URL_LOGIN_APPLICANTS}
			{else}
				{$BASE_URL}{$URL_JOB}/{$job.url_title}-{$job.location_asci}/{$job.id}
			{/if}
		">
			<button type="button" class="btn more-ur" style="width:110px">VIEW DETAILS</button>
		</a>
	</div>

	<div class="hidden-md hidden-lg hidden-sm col-xs-12">
		<a rel="canonical" href="{$BASE_URL}{$URL_JOB}/{$job.url_title}-{$job.location_asci}/{$job.id}" >
			<button type="button" class="btn company-more-ur">VIEW DETAILS</button>
		</a>
	</div>
	{*
	<div id="dash" class="col-md-12 col-xs-12">
		<a rel="canonical" href="{$BASE_URL}{$URL_JOB}/{$job.url_title}-{$job.location_asci}/{$job.id}" >
	 		<p>{$job.description_listing}</p>
	 	</a>
	</div>
	*}

</div>
