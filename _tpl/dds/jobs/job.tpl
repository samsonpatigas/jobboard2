<!-- /_tpl/dds/jobs/job.tpl -->
{include file="1.5/layout/sjs-header.tpl"}

 <!-- FACEBOOK og protocol -->
<meta property="og:url"  content="http://{$job_url}" />
<meta property="og:type"  content="website" />
<meta property="og:title" content="{$seo_title}" />
<meta property="og:site_name" content="{$BASE_URL}"/>
<meta property="og:description" content="{if $seo_desc}{$seo_desc}{else}{$meta_description}{/if}" />
<meta property="og:image" content="{$BASE_URL}{$job.company_logo_path}" />

{assign var="job_id" value={$job.id}}

{include file="snippets/detail-modals.tpl"}

<div class="job-details">
            <!--job.tpl -->
            <div class="container">
                <div class="row top-heading">
                    <div class="col-md-12 col-xs-12">
                        {* <a href="/profile/applications"><button type="button" class="btn btn-back">{$translations.website_general.backbtn_label}</button></a> *}
                        <h1>{$job.title}</h1>
                    </div>
                </div>
                <div class="row top-heading">
                    <div class="col-md-9 col-xs-12 details">
                        <h2 class="jobd-title" style="width:100%">{$job.title} {if $REMOTE_PORTAL == 'deactivated'}{$translations.website_general.in} {$job.location}{/if} {if $job.new_flag}<span class="new">{$translations.job_detail_section.new}</span>{/if}</h2>
                        {if false}
                            {if $FAVORITES_PLUGIN and $FAVORITES_PLUGIN == 'true'}
                                {if $favourites_job_ids and $job.id|in_array:$favourites_job_ids}
                                    <span id="desk-favourites-block-{$job.id}" ><a title="{$translations.alljobs.favourites_tooltip_remove}" href="#" onclick="return SimpleJobScript.removeFromFavourites({$job.id}, '{$BASE_URL}_tpl/{$THEME}/img/', 'desk-');"><i class="fa fa-heart fa-lg ml10" aria-hidden="true"></i></a></span>
                                {else}
                                    <span id="desk-favourites-block-{$job.id}" ><a title="{$translations.alljobs.favourites_tooltip_add}" href="#" onclick="return SimpleJobScript.addToFavourites({$job.id}, '{$BASE_URL}_tpl/{$THEME}/img/', 'desk-');"><i class="fa fa-heart-o fa-lg ml10" aria-hidden="true"></i></a></span>
                                {/if}
                            {/if}
                        {/if}

                        <ul class="top-ul control-panel">
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-building" aria-hidden="true"></i>
                                </span> {$job.company}
                            </li>
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-braille" aria-hidden="true"></i>
                                </span> {$job.job_type}
                            </li>
                        </ul>
                        <ul class="control-panel">
                            <li data-toggle="tooltip" title="{$translations.detail_sidebar.tooltip_email}" style="width: 50%">
                                <span>
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                </span>
                                <a class="undsc" data-toggle="modal" data-target="#emailModal" href="#" onclick="return false;">
                                    {$translations.detail_sidebar.control_email}
                                </a>
                            </li>
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-id-card" aria-hidden="true"></i>
                                </span>Job ID: {10000 + $job.id}
                            </li>
                        </ul>
                        <ul class="control-panel">
                            {if $REMOTE_PORTAL == 'deactivated'}
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </span> {$job.location}
                            </li>
                            {/if}
                            <li style="width:50%">
                                <span>
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span> {$job.post_date}
                            </li>

                        </ul>
                        <ul class="control-panel">
                            <li style="width:100%">
                                <span>
                                    <i class="fa fa-certificate" aria-hidden="true"></i>
                                </span>Category: {$job.category_name}
                            </li>
                        </ul>

                        <ul class="control-panel">
                            <li style="width:100%">
                                <span>
                                    <i class="fa fa-circle" aria-hidden="true"></i>
                                </span>Position: {$job.f9_position}
                            </li>
                        </ul>                        
                        <div class="border-light"></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="detail-font" >
                                <p>{$job.description}</p>
                            </div>
                        </div>
                            <div class="border-light"></div>
                            {if $job.salary}<p class="price-apply">Pay Rate: {$job.salary}</p>{/if}
                                {if $job.apply_online == 1}
                                    {if $SESSION_APPLICANT == 'true'}
                                        {include file="jobs/apply-existing-modal.tpl"}
                                    {else}
                                        {include file="jobs/apply-modal.tpl"}
                                    {/if}
                                {else}
                                    <div class="hta-p">username: {$SESSION_USERNAME} applicant: {$SESSION_APPLICANT}
                                        <a href="http://{$job.apply_desc}" target="_blank">
                                            <button 
                                                type="button" 
                                                class="btn btn-apply" 
                                                id="apply_now"
                                            >
                                                {$translations.apply.apply_btn}
                                            </button>
                                        </a>
                                    </div>
                                {/if}

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 jd-ads-m">
                                {if $smarty.const.ADSENSE == 'true'}
                                    {include file="$adsense_detail_rectangle"}
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12 co-name">
                        {if $job.f9_confidential_ad == 0}
                            {if $job.public_profile_flag == '1'}
                                <a href="{$job.company_detail_url}">
                                    <img class="co-logo" src="/{$job.company_logo_path}" alt="company logo" />
                                </a>
                            {else}
                                <img class="co-logo" src="/{$job.company_logo_path}" alt="company logo" />
                            {/if}
                        {/if}
                        {if $job.f9_confidential_ad == 1}
                            <img src="/_tpl/dds/img/f9_confidential_ad.png" class="similar-co-logo">
                        {/if}
                            <h2 class="co-title">{$job.company}</h2>
                            <p class="co-summary">
                                {$job.company_desc_excerpt}
                                {if $job.public_profile_flag == '1'}
                                    <a 
                                        href="{$job.company_detail_url}" 
                                        target="_blank">
                                            <button 
                                                type="button" 
                                                class="btn btn-more" >
                                                    {$translations.website_general.more}
                                            </button>
                                    </a>
                                {/if}

                            </p>
                            
                        </div>

                        <div class="col-md-3 col-xs-12">
                        {if $smarty.const.BANNER_MANAGER == 'true'}
                            {include file="$banners_detail_rectangle"}
                        {/if}	
                        </div>

                    </div>

                    {if !$related_jobs}
                    <div class="row top-heading">
                        <h2 class="heading">{$translations.detail_sidebar.related_jobs_title}</h2>

                        {foreach from=$related_jobs item=job}

                            <div class="col-md-6 col-xs-12 similar-detail {if $job.spotlight == '1'}spotlight-related{/if}">
                            <div class="col-md-2 col-xs-12">
                                {if $job.f9_confidential_ad == 1}
                                    <img src="/{$job.company_logo_path}" class="similar-co-logo">
                                {/if}
                                {if $job.f9_confidential_ad == 0}
                                    <img src="/_tpl/dds/img/f9_confidential_ad.png" class="similar-co-logo">
                                {/if}
                            </div>

                            <div class="col-md-10 col-xs-12">
                                <h3 class="similer-position">{$job.title}</h3>
                                
                                <ul class="similer-top-ul control-panel">
                                    <li class="similer-list-item"><span><i class="fa fa-building" aria-hidden="true"></i></span> {$job.company}</li>
                                    <li class="similer-list-item"><span><i class="fa fa-braille" aria-hidden="true"></i></span> {$job.job_type}</li>
                                </ul>
                                <ul class="similer-top-ul control-panel">
                            
                                    {if $REMOTE_PORTAL == 'deactivated'}
                                        <li class="similer-list-item"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span> {$job.location}</li>
                                    {/if}
                                    <li class="similer-list-item"><span><i class="fa fa-calendar" aria-hidden="true"></i></span> {$job.post_date}</li>
                                </ul>

                                {if $job.salary}<p class="similer-price-apply">{$job.salary}</p>{/if}
                                <a rel="canonical" href="{$BASE_URL}{$URL_JOB}/{$job.url_title}-{$job.location_asci}/{$job.id}">
                                    <button type="button" class="btn similer-btn-more">{$translations.website_general.more}</button>
                                </a>

                                {if $FAVORITES_PLUGIN and $FAVORITES_PLUGIN == 'true'}
                                    {if $job.id|in_array:$favourites_job_ids}
                                        <span id="desk-favourites-block-{$job.id}" ><a title="{$translations.alljobs.favourites_tooltip_remove}" href="#" onclick="return SimpleJobScript.removeFromFavourites({$job.id}, '{$BASE_URL}_tpl/{$THEME}/img/', 'desk-');"><i class="fa fa-heart fa-lg ml10 mt7" aria-hidden="true"></i></a></span>
                                     {else}
                                         <span id="desk-favourites-block-{$job.id}" ><a title="{$translations.alljobs.favourites_tooltip_add}" href="#" onclick="return SimpleJobScript.addToFavourites({$job.id}, '{$BASE_URL}_tpl/{$THEME}/img/', 'desk-');"><i class="fa fa-heart-o fa-lg ml10 mt7" aria-hidden="true"></i></a></span>
                                     {/if}
                                 {/if}

                            </div>
                            </div>
                          {/foreach}

                </div>
                <!-- related jobs -->
                {/if}

            </div>
        </div>

        {include file="snippets/listing-sitemap.tpl"}

{literal}
<script type="text/javascript">
    $(document).ready(function() {
        SimpleJobScript.I18n = {/literal}{$translationsJson}{literal};
        SimpleJobScript.initApplyValidation();

        $('#cv').change(function() {
            var fname = $('input[type=file]').val().split('\\').pop();
            if( fname )
                $('#cvLabel').html(fname);
            else
                $('#cvLabel').html($('#cvLabel').html());
        });

        $('.popup').click(function(event) {
            var width  = 575,
                height = 400,
                left   = ($(window).width()  - width)  / 2,
                top    = ($(window).height() - height) / 2,
                url    = this.href,
                opts   = 'status=1' +
                         ',width='  + width  +
                         ',height=' + height +
                         ',top='    + top    +
                         ',left='   + left;
            
            window.open(url, 'twitter', opts);
            return false;
          });

    });

</script>
{/literal}
{if $SESSION_APPLICANT}
    <script>
        localStorage.setItem('callbackJobURL', '');
    </script>
{else}
    <script>
        jobURL = window.location.href;
        localStorage.setItem('callbackJobURL', jobURL);
    </script>
{/if}

{include file="1.5/layout/sjs-footer.tpl"}