<?php

require_once '_config/config.php';

if (isset($_POST['interested'])) {
	$class = new JobApplication();
	$mailer = new Mailer();

	$APP_ID = $_POST['interested'];

	// change application status to rejected
	$class->reviewApplication($APP_ID);

	// get candidate email and job id
	$data = $class->getCandidateDataByJobApplicationId($APP_ID);

	$job = new Job($data['job_id']);
	$job_data = $job->GetInfo();

	// notify him
	$mailer->reviewCandidateApplication($data['candidate_email'], $job_data);

	echo json_encode(array('result' => '1')); die();
	redirect_to('http://jobboard.ferret9.com/dashboard/URL_DASHBOARD_SEARCHABLE');
} else {
	echo json_encode(array('result' => '0')); die();
	redirect_to('http://jobboard.ferret9.com/dashboard/URL_DASHBOARD_SEARCHABLE');
}
exit;

?>