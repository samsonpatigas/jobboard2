<?php
	$smarty->assign('PLAIN_SITE', true);
	// echo $_POST['callbackJobURL']; die();
	if (isset($_POST['signin_email']) && isset($_POST['signin_pass'])) {
		$class = new Applicant();
		$email = $db->getConnection()->real_escape_string(trim($_POST['signin_email']));
		$password = md5(trim($_POST['signin_pass']));

		if ($class->authenticate($email, $password)) {
			//set session
			$applicant = $class->getData();

			if (!$class->isConfirmed($applicant['id'])) {
 				redirect_to(BASE_URL . URL_ACCOUNT_NOT_CONFIRMED . '/app');
 				exit;
 			}

			$_SESSION['applicant'] = $applicant['id'];
			$_SESSION['applicant_name'] = $applicant['fullname'];

			//start of auto-reactivation
			$reactivate = new Maintenance();
			$reactivate->activateInActiveCandidate($applicant['id']);

			if ($_POST['redirect'] == ""){
				redirect_to('https://jobboard.ferret9.com' . '/profile/edit'); //this redirects to "Edit Profile"
			}else{
				redirect_to($_POST['redirect']);
			}
		} else {
			$smarty->assign('login_failed', 1);
			$smarty->assign('relogin_email', $email);
			$template = 'auth/login-applicants.tpl';
		}
	} else {
		redirect_to(BASE_URL . URL_LOGIN_APPLICANTS);
	}
	
?>