<?php 
    
    if (isset($id)) {
        $cid = intval($extra);
        //echo $cid;

        if($cid == 0){
            $sql1 = 'UPDATE jobs SET is_Seen = 1 where id = '.$id;
            $db->query($sql1);
        }

        if ($id === "publish") {
            sendUpdates();
            $cid = intval($extra);
            $sql = 'UPDATE jobs SET review_status = 1 WHERE id = ' . $cid;
            $db->query($sql);
        } 
        if ($id === "unpublish") {

            $cid = intval($extra);
            $sql = 'UPDATE jobs SET review_status = 0 WHERE id = ' . $cid;
            $db->query($sql);
        } 
        if ($id === "publish" || $id === "unpublish") {
            $ID = intval($extra);
        } else {
            $ID = intval($id);
        }

        $sql = 'SELECT a.id AS id,emp.name AS empname,emp.email as email,a.type_id AS type_id,c.name as type_name,a.employer_id as employer_id,a.category_id AS category_id,a.title AS title,a.description AS description,a.created_on AS created_on,a.expires AS expires,a.is_active AS is_active,a.review_status AS review_status,a.views_count AS views_count, a.city_id AS city_id,a.apply_online AS apply_online,a.apply_desc AS apply_desc,a.spotlight AS spotlight,a.salary AS salary, a.f9_date_posted AS f9_date_posted,a.f9_title AS f9_title,a.f9_position AS f9_position,a.f9_city AS f9_city,a.f9_state AS f9_state,a.f9_zip AS f9_zip,a.f9_salary_range AS f9_salary_range,a.f9_yrs_of_experience AS f9_yrs_of_experience,a.f9_language AS f9_language,a.f9_gender AS f9_gender,a.f9_specialties AS f9_specialties,a.f9_short_description AS f9_short_description,a.f9_post_peroid AS f9_post_peroid,a.f9_admin_notes AS f9_admin_notes,b.name AS category_name,b.var_name AS category_varname,c.var_name AS type_var_name,c.name as type_name,a.expires AS closed_on,DATEDIFF(NOW(),created_on) AS days_old,cit.name AS city_name,cit.ascii_name AS city_asci,company.name AS company_name,company.id as "company_id",company.description AS company_desc,company.hq AS company_hq,company.url AS company_url,company.logo_path AS company_logo_path,company.public_page AS company_public_profile_flag FROM '.DB_PREFIX.'jobs a LEFT JOIN '.DB_PREFIX.'cities cit on a.city_id = cit.id, '.DB_PREFIX.'company company, ' .DB_PREFIX.'categories b, '.DB_PREFIX.'types c, '.DB_PREFIX.'employer emp WHERE a.category_id = b.id AND c.id = a.type_id AND company.employer_id=a.employer_id AND a.employer_id = emp.id AND a.id = ' . $ID;
        $data = $db->query($sql);

        $smarty->assign("data", $data);
    }

    $template = 'reviews_doctor.tpl';

    function sendUpdates() {
      global $db;
      $sanitizer = new Sanitizer;
      $current_uri = explode('/', $_SERVER['REQUEST_URI']);
      $job_number =  $current_uri[count($current_uri)-1];
      
      $sql_email = 'SELECT b.id, b.created_on, c.email as "From", b.company as "By", b.title, a.fullname, a.email as "To", a.f9_position, b.f9_position, b.city_id, b.f9_position_type, b.category_id, b.f9_date_posted, b.f9_confidential_ad FROM applicant as a 
        LEFT JOIN jobs as b ON a.f9_position = b.title
        LEFT JOIN employer as c ON b.employer_id = c.id
        WHERE date_sub(CURDATE(), INTERVAL 7 DAY) AND b.is_active = 1';

        $result_email = $db->query($sql_email);
        

      while ($row = $result_email->fetch_assoc()) {
        $to      .= '<a>' . $row['To'] . ', </a>'; 
        $subject = $row['title'];
        $sanitized_title = $sanitizer->sanitize_title_with_dashes($row['title']);
        $sanitized_from = $sanitizer->sanitize_title_with_dashes($row['By']);
        $sanitized_link = $sanitized_title . $sanitized_from;
        $message = '
          <!DOCTYPE html>
          <html>
          <head>
          <meta charset="UTF-8">
          <title></title>
          </head>
          <body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;">
          <div style="padding:10px; background:#333; font-size:24px; color:#CCC;">
          <a href="http://www.desertdentalstaffing.com"><img src="https://jobboard.ferret9.com/uploads/logos/main-logo.png" width="36" height="30" alt="yoursitename" style="border:none; float:left;"></a></div>
          <div style="padding:24px; font-size:17px;">Hello '.$row['fullname'].',<br /><br /> your profile have match the job titled '. $row['title'] . ' publish by ' .$row['By'] . '<br /><br /><a href="https://jobboard.ferret9.com/job/' . $sanitized_link . "/" . $job_number . '">Click here to apply https://jobboard.f9portal.com/job/' . $sanitized_link . "/" . $job_number . '</a><br /><br /></div>
          </body>
          </html>';
      }

      $To = strip_tags($to);
      // var_dump($To); die();
      $TextMessage = strip_tags(nl2br($message),"<br>");
      $Subject = strip_tags($subject);
      
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      
      // More headers
      $headers .= 'From: <desertdentalstaffing@gmail.com>' . "Desert Dental Staffing\r\n";
      $headers .= 'Cc: desertdentalstaffing@gmail.com' . "\r\n";
      /*
      $today = date('Y-m-d');
      $datetime1 = new DateTime($today);
      $datetime2 = date('Y-m-d H:i:s', strtotime('+7 day', $today));
      $interval = $datetime2->diff($datetime1);
      $interval = date_diff($datetime1, $datetime2);
      */
      $To_1 = explode(",", $To );
      /*
      var_dump($To_1);
      echo 'to: ' . $To_1[0];
      echo 'subject: ' . $Subject;
      echo 'text message: ' . $TextMessage;
      echo 'header: ' . $header;
      */
      mail($To_1[0], $Subject, $TextMessage, $headers);
      
      /*
      if($diff == '7'){
        mail($To, $Subject, $TextMessage, $headers);
      }
      */
    }

?>