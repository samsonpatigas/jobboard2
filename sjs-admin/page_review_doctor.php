<?php 

	$sql = 'SELECT COUNT(id) as total FROM jobs';
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    $TOTAL = $row['total'];

    //pagination
    $paginatorLink = BASE_URL  . "review_doctors";
    $paginator = new Paginator($TOTAL, SUBSCRIBERS_PER_PAGE, @$_REQUEST['p']);
    $paginator->setLink($paginatorLink);
    $paginator->paginate();
    $offset = $paginator->getFirstLimit();

    $sql = 'SELECT a.id as "id", a.created_on, a.title, b.name, a.review_status, a.is_Seen, b.email FROM jobs as a
            LEFT JOIN employer as b ON a.employer_id = b.id 
            ORDER BY a.created_on DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
    $data = $db->query($sql);

    if(isset($_POST['reviewed'])){

        $sql = 'SELECT a.id as "id", a.created_on, a.title, b.name, a.review_status, a.is_Seen, b.email FROM jobs as a
            LEFT JOIN employer as b ON a.employer_id = b.id 
            WHERE a.is_Seen = 1 
            ORDER BY a.created_on DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("reviewed", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['notreviewed'])){
        $sql = 'SELECT a.id as "id", a.created_on, a.title, b.name, a.review_status, a.is_Seen, b.email FROM jobs as a
            LEFT JOIN employer as b ON a.employer_id = b.id 
            WHERE a.is_Seen = 0
            ORDER BY a.created_on DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("notreviewed", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['allreviewed'])){
        $sql = 'SELECT a.id as "id", a.created_on, a.title, b.name, a.review_status, a.is_Seen, b.email FROM jobs as a
            LEFT JOIN employer as b ON a.employer_id = b.id 
            ORDER BY a.created_on DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("allreviewed", "background-color: #2C3E50!important;");
    }
      if(isset($_POST['published'])){
        $sql = 'SELECT a.id as "id", a.created_on, a.title, b.name, a.review_status, a.is_Seen, b.email FROM jobs as a
            LEFT JOIN employer as b ON a.employer_id = b.id 
            WHERE a.review_status = 1
            ORDER BY a.created_on DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("publish", "background-color: #2C3E50!important;");
    }
     if(isset($_POST['notpublished'])){
        $sql = 'SELECT a.id as "id", a.created_on, a.title, b.name, a.review_status, a.is_Seen, b.email FROM jobs as a
            LEFT JOIN employer as b ON a.employer_id = b.id 
            WHERE a.review_status = 0
            ORDER BY a.created_on DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("notpublish", "background-color: #2C3E50!important;");
    }
    if(isset($_POST['allpublished'])){
        $sql = 'SELECT a.id as "id", a.created_on, a.title, b.name, a.review_status, a.is_Seen, b.email FROM jobs as a
            LEFT JOIN employer as b ON a.employer_id = b.id 
            ORDER BY a.created_on DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("allpublish", "background-color: #2C3E50!important;");
    }

    if(isset($_POST['btnsearch'])){
        $sql = 'SELECT a.id as "id", a.created_on, a.title, b.name, a.review_status, a.is_Seen, b.email FROM jobs as a
                LEFT JOIN employer as b ON a.employer_id = b.id 
                WHERE a.title LIKE"%'.$_POST['search'].'%" OR b.name LIKE"%'.$_POST['search'].'%" OR b.email LIKE"%'.$_POST['search'].'%"
                ORDER BY a.created_on DESC limit ' .$offset . ', ' . SUBSCRIBERS_PER_PAGE;
        $data = $db->query($sql);
        $smarty->assign("searchtext", $_POST['search']);
        $smarty->assign("btnsearch", "background-color: #2C3E50!important;");
    }

    $c = array();
    while ($row = $data->fetch_assoc()) {
     $c[] = $row;
    }

    $smarty->assign("companies", $c);
 	$smarty->assign("pages", $paginator->pages_link);
	$template = 'review_doctors.tpl';

?>
