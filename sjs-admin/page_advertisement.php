<?php 

	$smarty->assign('ACTIVE', 8);

	$cl = new Campaign();

	if ($id == 'create' && $id != NULL) { 
		escape($_POST);

		if (!empty($_FILES["banner"]['tmp_name'])) {

			//file type validation
			$explode = explode(".", $_FILES['banner']['name']);
			$ext = end($explode);
			if (!in_array($ext, array("jpeg","jpg", "png", "bmp", "gif"))) {
				$campaigns = $cl->getCampaigns();
				$areas = $cl->getAreas();

				$smarty->assign('FILE_ERR', true);
				$smarty->assign('areas', $areas);
				$smarty->assign('campaigns', $campaigns);
				$smarty->assign('count', count($campaigns));

				$template = 'advertisement.tpl';
				return;
			}

			$newNamePrefix = time() . '_';
			$manipulator = new ImageManipulator($_FILES['banner']['tmp_name']);

			$size = getimagesize($_FILES["banner"]['tmp_name']);
			$fileType = $size[2];

			// do not resize leaderboard and joblisting banners
			if (intval($area_select) != 4 && intval($area_select) != 5) {

				if (intval($area_select) == 6 && $size[0] > 230) {
					$newImage = $manipulator->resample(230, 230);
				} else {
					if ($size[0] > 285) {
						//needs a resize
				        $newImage = $manipulator->resample(285, 285);		        
					}			
				}
			}

			$DIR_CONST = '';
			if (defined('__DIR__'))
				$DIR_CONST = __DIR__;
			else
				$DIR_CONST = dirname(__FILE__);

			$final_path = $DIR_CONST . '/../' . BANNERS_UPLOAD_FOLDER . $newNamePrefix . $_FILES['banner']['name'];
			$db_file_path = BANNERS_UPLOAD_FOLDER . $newNamePrefix . $_FILES['banner']['name'];
			$manipulator->save($final_path, $fileType);
		}

		$active = (isset($campaign_active_switch)) ? "1" : "0";
		$data = array('area_id' => $area_select, 'c_name' => $name, 'is_active' => $active, 'url' => $url, 'banner_path' => $db_file_path);

		$cl->createCampaign($data);
		sleep(0.1);
	}

	if ($id == 'delete' && $id != NULL) {
		$cl->deleteCampaign($extra);
		sleep(0.1);
	}

	$campaigns = $cl->getCampaigns();
	$areas = $cl->getAreas();

	$smarty->assign('areas', $areas);
	$smarty->assign('campaigns', $campaigns);
	$smarty->assign('count', count($campaigns));

	$template = 'advertisement.tpl';
?>
