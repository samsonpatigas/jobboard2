<?php
	require_once '../_config/config.php';
	require_once '../_lib/class.Types.php';
	
		$sql = "SELECT a.id, a.created_on, b.title, d.name, c.fullname, a.status FROM job_applications as a 
		LEFT JOIN jobs as b ON a.job_id = b.id 
		LEFT JOIN applicant as c ON a.applicant_id = c.id
		LEFT JOIN cities as d ON b.city_id = d.id
		ORDER BY a.created_on DESC";

		$result = $db->query($sql);

		$a = array();
		while ($row = $result->fetch_assoc()){
			if($row['status'] == 0){
				$row['status'] = '<i class="fa  fa-file-text" aria-hidden="true" style="color:#777;"> applied</i>';
			}
			if($row['status'] == 1){
				$row['status'] = '<i class="fa fa-close" aria-hidden="true" style="color:#e74c3c;"> rejected</i>';
			}
			if($row['status'] == 2){
				$row['status'] = '<i class="fa fa-check" aria-hidden="true" style="color:#2ecc71;"> under review</i>';
			}
			$a['data'][] = $row;
		}
		echo (json_encode($a));
?>