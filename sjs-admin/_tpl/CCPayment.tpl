{include file="header.tpl"}
<!-- CCPayment.tpl -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<div class="admin-content">
    <div  id="app" class="admin-wrap-content" style="padding-right: 35px !important;">
      <h1>Credit Card Payment</h1>
        <div>
            <label>Amount:</label>
            <input v-model="info.value"/>
            &nbsp
            <button v-on:click="sendViaAjax">Save Payment</button>
        </div>
        <div v-if="update_successful">Update Successful</div>
    </div>
</div>    
{include file="footer.tpl"}
<script>
    var app = new Vue({
      el: '#app',
      delimiters: ['%%', '%%'],
      created() {
        window.addEventListener('keydown', (e) => {
          app.update_successful = false
        });
      },
      data () {
        return {
            info: null,
            update_successful: false
        }
      },
      mounted () {
        axios
          .get('https://jobboard.ferret9.com/sjs-admin/getCCprice')
          .then(
            response => (this.info = response.data)
            )
      },
      methods: {
        sendViaAjax: function(){          
            axios
                .post('https://jobboard.ferret9.com/sjs-admin/setCCprice', {
                  ccprice: app.info.value
                })
                .then(function (response) {
                  app.update_successful = true
            })
        }
      }
    })    
</script>