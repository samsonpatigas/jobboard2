<?php
/* Smarty version 3.1.30, created on 2019-11-02 10:06:46
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/job-details.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5dbd9b86879fa0_04274038',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8a978c7fe9c4082a0fbabda94902e64f6b3a6268' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/job-details.tpl',
      1 => 1569868463,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbd9b86879fa0_04274038 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_highlight_keywords')) require_once '/home1/fninport/public_html/jobboard/_lib/smarty/libs/plugins/modifier.highlight_keywords.php';
?>
<div id="job-details">
	<h3 class="blue-font">
	 <?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
 
	</h3>
	<p>
		<span class="fading">at</span>
		<?php if ($_smarty_tpl->tpl_vars['job']->value['url'] && $_smarty_tpl->tpl_vars['job']->value['url'] != 'http://') {?>
		<a target="_blank" href="http://<?php echo $_smarty_tpl->tpl_vars['job']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['job']->value['company'];?>
</a>
		<?php } else { ?>
		<strong><?php echo $_smarty_tpl->tpl_vars['job']->value['company'];?>
</strong>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['REMOTE_PORTAL']->value == 'activated') {?>
		<strong>(<?php echo $_smarty_tpl->tpl_vars['translations']->value['jobs']['location_anywhere'];?>
)</strong>
		<?php } else { ?>
		<span class="fading">in</span> <span class="dark-font" ><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</span>
		<?php }?>
	</p>
	<div id="job-description" class="container-fluid">
	<?php echo smarty_modifier_highlight_keywords($_smarty_tpl->tpl_vars['job']->value['description'],$_SESSION['keywords_array']);?>

	</div><br />
</div><!-- #job-details -->
<?php }
}
