<?php
/* Smarty version 3.1.30, created on 2019-10-04 14:34:49
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/news.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d974a795e7393_78835429',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8e7818f49a4646fa2542fcdc56cd8561ff9c6362' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/news.tpl',
      1 => 1569868464,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d974a795e7393_78835429 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
 <div class="admin-wrap-content">

	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb20">
		<label class="admin-label">News</label>
		<div class="alert alert-info fade in main-color">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
		     <i class="fa fa-info-circle info-fa" aria-hidden="true"></i>&nbsp;
		  	Employer dashboard overview board. You can use it to inform companies about news in the job board.
		</div>
	</div>

	<?php if ($_smarty_tpl->tpl_vars['count']->value > 0) {?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <table class="table">
	    <thead>
	      <tr>
	        <th>Date</th>
	        <th>Message</th>
	        <th>Delete</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['news']->value, 'obj', false, 'val');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['val']->value => $_smarty_tpl->tpl_vars['obj']->value) {
?>
	      	<tr>
	       		 <td><?php echo $_smarty_tpl->tpl_vars['obj']->value['date_formated'];?>
</td>
	       		 <td><?php echo $_smarty_tpl->tpl_vars['obj']->value['message'];?>
</td>
	       		 <td><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
news/delete/<?php echo $_smarty_tpl->tpl_vars['obj']->value['id'];?>
" title="Delete this message" onclick="if(!confirm('Are you sure you want to delete this item?'))return false;"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a></td>
	      	</tr>
	      	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	    </tbody>
	  </table>
	  <?php }?>

	    <br />
	    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 0px; margin-left: 0px;">
			<label>Add new entry</label>
			<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
news/add" role="form">
				<div class="form-group">
				  <textarea required class="form-control" rows="5" name="msg" id="msg"></textarea>
				</div>
				<div class="form-group" style="margin-top: 30px;">
				   <button type="submit"  class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Submit</button>
				</div>
			</form>
		</div>

	</div>
  </div>
</div><!-- #content -->

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
