<?php
/* Smarty version 3.1.30, created on 2019-08-30 14:05:35
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/customizer-css.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d691f1f93a3e0_72137726',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7f4a002134f7540545a7b63814392268c92cddf3' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/customizer-css.tpl',
      1 => 1565267001,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d691f1f93a3e0_72137726 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
 <div class="admin-wrap-content">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	<label class="admin-label">SJS CUSTOMIZER</label>
	<div class="subheading">Add custom CSS styles</div>
	<div class="textarea-feedback" >[tip: find element class you wish to change in browser developer tools/inspector, than paste it here and change values]</div>
	<p>(<a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customizer/">&larr;go back</a>)</p>
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mt25">
	    <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customize-css" role="form" enctype="multipart/form-data">

			<div class="form-group">
 
	    	<div class="form-group">
			  <textarea class="form-control" rows="20" name="cssarea" id="cssarea"><?php echo $_smarty_tpl->tpl_vars['data']->value;?>
</textarea>
			</div>

			</div>
			<br /><br />

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
 				<button style="margin-top: 30px;" type="submit"  class="btn btn-default btn-primary mbtn" name="submit" id="submit" class="mt25" >SAVE</button>
 			</div>

		</form>
    </div>

    </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['updated']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Design updated');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
