<?php
/* Smarty version 3.1.30, created on 2018-11-19 11:55:31
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder-setting.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bf2a4b3d5bd31_91752226',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '76a5f2736ae805cdcd007fa0bf13b2e80494b0d5' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/feeder-setting.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bf2a4b3d5bd31_91752226 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
	<div class="admin-wrap-content " >
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-bottom: 20px;">
		<label class="admin-label">DATA FEEDER SETTINGS</label>
		<a href="/sjs-admin/feeder"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a><br />

		<div class="alert alert-info fade in main-color">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
		    <i class="fa fa-info-circle info-fa" aria-hidden="true"></i>&nbsp;
		  	Set your admin company email to receive job application notifications. Other company details can be managed <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
login-as/144/Admin" target="_blank">here</a> by clicking on MY COMPANY tab.
		</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	
		<form action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
feeder-settings" method="post">
			<div>
				<div class="form-group mb20">
					<label>Email</label>
					<input required value="<?php echo $_smarty_tpl->tpl_vars['EMAIL']->value;?>
" class="form-control minput" type="text" name="email" id="email" size="100" />
				</div>
			<div class="button-holder-np form-group" >
				<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Save</button>
			</div>
		</form>

	</div>
 </div>
</div><!-- #content -->

<?php if ($_smarty_tpl->tpl_vars['update_popup']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Entry successfully updated');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
