<?php
/* Smarty version 3.1.30, created on 2019-06-03 09:50:36
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/customizer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5cf4df5c941e19_05921158',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4180ef5a118213af7345d456fa5fce433f4891aa' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/customizer.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5cf4df5c941e19_05921158 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
	<div class="admin-wrap-content" >
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				SJS Customizer
			</label>
			<div class="subheading">Fast & easy way to customize your project. Color customization coming soon.</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customize-homepage" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customize-homepage">Homepage</a></label>
			</div>

		    <div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customize-jobs" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customize-jobs">Job listing</a></label>
			</div>
 
			<div class="settings-row">
				<a class="right blue" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customize-css" title="Change password"><i class="fa fa-gear blueColor mr5" aria-hidden="true"></i></a>
				<label class="settings"><a class="blue-font" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customize-css">Add Custom CSS</a></label>
			</div> 

		</div>
</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
