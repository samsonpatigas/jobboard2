<?php
/* Smarty version 3.1.30, created on 2019-05-30 13:59:38
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/cleaner.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5cefd3bae0fbe3_72536572',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f10feb680adf74ceb915dc0055094feccd644d09' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/cleaner.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5cefd3bae0fbe3_72536572 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
  <div class="admin-wrap-content" >
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb25">
			<label class="admin-label">
				Database cleaner
			</label>
			<div class="subheading">Clean old entries to reduce project database size.</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<ul class="list-group cleaner">
			  <li class="list-group-item">Temporary jobs - <strong><?php echo $_smarty_tpl->tpl_vars['tmp_count']->value;?>
</strong><div class="float-right"><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cleaner/tmp"><button type="button" class="mbtn btn btn-default alizarinBtn">CLEAN</button></a></div></li>
			  	 <li class="list-group-item">Expired jobs - <strong><?php echo $_smarty_tpl->tpl_vars['exp_count']->value;?>
</strong><div class="float-right"><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cleaner/exp"><button type="button" class="mbtn btn btn-default alizarinBtn"><?php if ($_smarty_tpl->tpl_vars['EXPIRED_JOBS_ACTION']->value == 'deactivate') {?>DEACTIVATE<?php } else { ?>CLEAN<?php }?></button></a></div></li>

			  	  <li class="list-group-item">Expired job stats - <strong><?php echo $_smarty_tpl->tpl_vars['old_hits_count']->value;?>
</strong><div class="float-right"><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cleaner/hits"><button type="button" class="mbtn btn btn-default alizarinBtn">CLEAN</button></a></div></li>

			</ul>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php if ($_smarty_tpl->tpl_vars['popup']->value) {?>
				<div class="alert alert-info fade in main-color">
				    <a href="#" class="close" data-dismiss="alert">&times;</a>
				    <i class="fa fa-check" aria-hidden="true"></i>
				    Redundant entries have been successfully cleaned. Consider to schedule an automatic cron job for this task.
				</div>
			<?php }?>
		</div>
  </div>
</div>



<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
