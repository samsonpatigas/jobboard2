<?php
/* Smarty version 3.1.30, created on 2019-10-04 14:34:10
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/subscribers.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d974a526263d8_11659061',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1b348658f620aa9d8086999540912711b0198d93' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/subscribers.tpl',
      1 => 1569868471,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d974a526263d8_11659061 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
	<div class="admin-wrap-content">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	<label class="admin-label">Subscribers</label>
	<div class="subheading">List of users receiving job alerts. There are 2 ways to send newsletter. 1) - export CSV and run newsletter via transaction email provider. 2) - setup a cronjob for "_tpl/cron_weekly_newsletter.php" file</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt15">
		<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
subscribers-csv/"><button type="submit" class="btn btn-default btn-primary mbtn" style="background-color: #E74C3C">Export to CSV</button></a>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php if ($_smarty_tpl->tpl_vars['subscribers']->value) {?>
			<ul class="applicants-list ">  
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subscribers']->value, 'subscriber', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subscriber']->value) {
?> 
				    <li class="p15"> 

				    <span><?php echo $_smarty_tpl->tpl_vars['subscriber']->value['email'];?>
</span>

			    	<div style="float:right;">

			    		<?php if ($_smarty_tpl->tpl_vars['subscriber']->value['confirmed'] == '1') {?>
	                	<span class="green mr10">confirmed</span>
	                	<?php } else { ?>
	                	<span class="red mr10">unconfirmed</span>
	                	<?php }?>

				    	<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
subscribers/delete/<?php echo $_smarty_tpl->tpl_vars['subscriber']->value['id'];?>
/" title="Delete this subscriber" onclick="if(!confirm('Are you sure you want to delete this subscriber?'))return false;"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>

				    </div>


					</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</ul>
			<?php } else { ?>
				<ul class="applicants-list ">  
					<li class="p15"> 
					No subscribers for the moment
					</li>
				</ul>
			<?php }?>
			<div class="pagination"><?php echo $_smarty_tpl->tpl_vars['pages']->value;?>
</div>
    </div>
</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
