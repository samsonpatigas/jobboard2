<?php
/* Smarty version 3.1.30, created on 2018-12-03 00:39:20
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/indeed.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c047b38174017_57162651',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '499d20cc224e85d75edcbba96f7d17ed8e7437a3' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/indeed.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5c047b38174017_57162651 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
	<div class="admin-wrap-content" >
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
		<label class="admin-label">
			Indeed Settings
		</label>
		<div class="subheading">Read <a href="https://simplejobscript.com/indeed-setup-guide/" target="_blank">guide</a>. If the <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cities/list" target="_blank">locations</a> are countries - select homepage dropdown to "countries". If they are cities, select the "cities". It will help Indeed to pull more relevant jobs.</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="banBtnDiv">
			<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
indeed/manage"><button class="btn btn-default btn-primary mbtn" style=" background-color: #E74C3C">MANAGE SEARCH</button></a>
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<form id="indeed-form" name="indeed-form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
indeed/settings" role="form" >

		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">PLUGIN STATUS:</label>

					<select class="form-control minput" id="indeed_activate_select" name="indeed_activate_select">
								<option <?php if ($_smarty_tpl->tpl_vars['data']->value['activate_select'] == 'activated') {?>selected<?php }?> value="activated">Activated</option>
								<option <?php if ($_smarty_tpl->tpl_vars['data']->value['activate_select'] == 'deactivated') {?>selected<?php }?> value="deactivated">Deactivated</option>
					</select>

		    	</div>
			</div>

			<div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">HOMEPAGE DROPDOWN SEARCH BY:</label>

					<select class="form-control minput" id="indeed_homepage_dropdown_select" name="indeed_homepage_dropdown_select">
								<option <?php if ($_smarty_tpl->tpl_vars['data']->value['homepage_dropdown'] == 'cities') {?>selected<?php }?> value="cities">Cities</option>
								<option <?php if ($_smarty_tpl->tpl_vars['data']->value['homepage_dropdown'] == 'countries') {?>selected<?php }?> value="countries">Countries</option>
					</select>

		    	</div>
			</div>

		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">PUBLISHER ID (*):</label>
		    		<input name="indeed_publisher_id" id="indeed_publisher_id" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['publisher_id'];?>
" class="form-control minput"  />
		    	</div>
			</div>

		    <div class="settings-row">
		    	<div class="float-block">
					<label class="settings">SHOW BOTH INDEED AND MY JOBS:</label>
					<input <?php if ($_smarty_tpl->tpl_vars['data']->value['show_both_jobs_flag'] == '1') {?>checked<?php }?> name="indeed_show_both_jobs_flag" type="checkbox" />
		    	</div>
			</div>


		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">DEFAULT QUERY:</label>
		    		<input name="indeed_query" id="indeed_query" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['query'];?>
" class="form-control minput"  />
		    	</div>
			</div>


		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">DEFAULT COUNTRY (*):</label>
		    		<input name="indeed_country" id="indeed_country" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['country'];?>
" class="form-control minput"  />
		    	</div>
			</div>

		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">DEFAULT CITY (*):</label>
		    		<input name="indeed_location" id="indeed_location" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['location'];?>
" class="form-control minput"  />
		    	</div>
			</div>


		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">DEFAULT JOB TYPE:</label>
		    		<input name="indeed_jobtype" id="indeed_jobtype" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['jobtype'];?>
" class="form-control minput"  />
		    	</div>
			</div>


		    <div class="settings-row">
		    	<div class="float-block">
		    		<label class="settings">JOBS OLD (IN DAYS):</label>
		    		<input name="indeed_jobs_old" id="indeed_jobs_old" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['jobs_old'];?>
" class="form-control minput"  />
		    	</div>
			</div>

			<div class="float-block">
				<div class="form-group" style="margin-top: 10px;">
					<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Save</button>
				</div>
			</div>

		</form>
	</div>
    
</div><!-- #content -->
</div>

<?php if ($_smarty_tpl->tpl_vars['updated_popup']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Indeed settings have been updated');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }
}
