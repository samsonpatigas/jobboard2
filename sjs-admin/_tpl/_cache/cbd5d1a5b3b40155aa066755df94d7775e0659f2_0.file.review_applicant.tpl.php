<?php
/* Smarty version 3.1.30, created on 2019-12-11 10:39:49
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/review_applicant.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5df11bd57a9804_24427742',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cbd5d1a5b3b40155aa066755df94d7775e0659f2' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/review_applicant.tpl',
      1 => 1573486429,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5df11bd57a9804_24427742 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- MODAL FOR MORE DETAILS -->
<!-- /sjs-admin/_tpl/review_applicant -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #008080; color: #fff;">
          <h4 class="modal-title" style="font-size: 20px;">Applicant Informations</h4>
         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
            <br>
            <div class="row">
                <div class="col-xs-12 col-md-15">
    			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'companie', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['companie']->value) {
?> 
    				<?php if ($_smarty_tpl->tpl_vars['companie']->value) {?>
    				<ul class="list-group">
    					
    				  <li class="list-group-item"><strong>Applicant: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['candidate_name'];?>
</li>
    				  <li class="list-group-item"><strong>Email: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['candidate_email'];?>
</li>
    				  <li class="list-group-item"><strong>Phone: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['phone'];?>
</li>
    				  <li class="list-group-item"><strong>Gender: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_gender'];?>
</li>
    				  <li class="list-group-item"><strong>Address 1: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_address_1'];?>
</li>
    				  <li class="list-group-item"><strong>Address 2: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_address_2'];?>
</li>
    				  <li class="list-group-item"><strong>City/State: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_city'];?>
,&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_state'];?>
</li>
<!--				  <li class="list-group-item"><strong>Country: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_country'];?>
</li> -->
    				  <li class="list-group-item"><strong>Zip Code: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_zip'];?>
</li>
<!--				  <li class="list-group-item"><strong>Language: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_language'];?>
</li> -->
<!--				  <li class="list-group-item"><strong>Location: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['location'];?>
</li>
    				  <li class="list-group-item"><strong>Occupation: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['occupation'];?>
</li> -->
    				  <li class="list-group-item"><strong>Skills: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['skills'];?>
</li>
    				  <li class="list-group-item"><strong>Experience: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['f9_yrs_experience'];?>
</li>

				</ul>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<!-- END -->
		
<div class="admin-content">
	<div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<label class="admin-label">Applicant details</label>
				<div class="subheading"><a href="/sjs-admin/review_applicants"><div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div></a>

				</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'companie', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['companie']->value) {
?> 
				<?php if ($_smarty_tpl->tpl_vars['companie']->value) {?>
				<ul class="list-group">
				  <li class="list-group-item"><strong>Email: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['candidate_email'];?>
</li>
				  <li class="list-group-item"><strong>Applicant: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['companie']->value['candidate_name'];
echo $_smarty_tpl->tpl_vars['companie']->value['comment'];?>
</li>
				  <li class="list-group-item"><strong>Visibility Status: </strong>&nbsp;<?php if ($_smarty_tpl->tpl_vars['companie']->value['public_profile'] == '0') {?><span class="red">Private</span><?php } else { ?><span class="green">Public</span><?php }?></li>
				  <li class="list-group-item" style="text-align:right"><a id='about' data-toggle='modal' data-target='#myModal' href=''><strong>Show more informations </strong><i class='fa fa-info-circle fa-lg' aria-hidden='true'></i></a></li>
				</ul>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			<?php if ($_smarty_tpl->tpl_vars['companie']->value['public_profile'] == '0') {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
review_applicant/publish/<?php echo $_smarty_tpl->tpl_vars['companie']->value['candidate_id'];?>
">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">PUBLISH ACCOUNT</button>
				</a>
			<?php } else { ?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
review_applicant/unpublish/<?php echo $_smarty_tpl->tpl_vars['companie']->value['candidate_id'];?>
">
					<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">UNPUBLISH ACCOUNT</button>
				</a>
			<?php }?>
		</div>

<!-- 		ADDED ADMIN NOTES -->

<style type="text/css">
	
	#note {

		margin-top: 1px;

	}

	#save {

		margin-left: 365px;
		margin-top: -10px;
	}

	#mnote {

		margin-left: 600px;
		margin-top: -398px;

	}

	#mnote p {

		margin-left: 30px;
	}

</style>

<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mt25" id="note">

	<?php if ($_smarty_tpl->tpl_vars['companie']->value['note'] == null) {?>

	<h4>Admin Notes<red style="color: red;padding: 10px;">*</red><small style="color: red;">( No notes added for this user. )</small></h4>

	<textarea id="apply_msg" name="apply_msg" class="apply_msg" placeholder="Add some notes?" maxlength="500" rows="5" cols="76" form="usrform"></textarea>

	<?php } else { ?>

	<h4>Admin Notes<red style="color: red;padding: 10px;">*</red></h4>

	<textarea id="apply_msg" name="apply_msg" class="apply_msg" maxlength="500" rows="5" cols="76" form="usrform"><?php echo $_smarty_tpl->tpl_vars['companie']->value['note'];?>
</textarea>

	<?php }?>

		<div class="textarea-feedback tal" id="textarea_feedback"></div>

</div>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25" id="save">

	 <form action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
review_applicant/note/<?php echo $_smarty_tpl->tpl_vars['companie']->value['candidate_id'];?>
" method="POST" id="usrform">
		
		<button type="submit" class="btn btn-default btn-primary mbtn" style="margin-top: 25px; background-color: #E74C3C">Save</button>

	</form>

</div>

<!-- ADDED 01/03/2019 -->

	<?php echo '<script'; ?>
 type="text/javascript">
		
		$(".apply_msg").focus(function() {
    if(document.getElementById('apply_msg').value === ''){
        document.getElementById('apply_msg').value +='• ';
	}
});
$(".apply_msg").keyup(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        document.getElementById('apply_msg').value +='• ';
	}
	var txtval = document.getElementById('apply_msg').value;
	if(txtval.substr(txtval.length - 1) == '\n'){
		document.getElementById('apply_msg').value = txtval.substring(0,txtval.length - 1);
	}
});

	<?php echo '</script'; ?>
>

	<!-- ENDPOINT -->

<!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25" id="mnote">

		<h4>Your Note<red style="color: red;padding: 10px;">*</red></h4>

		<?php if ($_smarty_tpl->tpl_vars['companie']->value['note'] != null) {?>

			<p class="green"><?php echo $_smarty_tpl->tpl_vars['companie']->value['note'];?>
</p>
		<?php } else { ?>

			<p class="red">No notes added for this user.</p>

		<?php }?>


</div> -->

<!-- END -->

	</div>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
