<?php
/* Smarty version 3.1.30, created on 2020-01-04 20:44:13
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/login_header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e114d7deb1620_28370286',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '62190cc75ba9fddf8a1ee5d63823b92a9c340018' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/login_header.tpl',
      1 => 1573586006,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e114d7deb1620_28370286 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- /sjs-admin/_tpl/login_header.tpl -->
<!doctype html>
<html lang="en">
<head>
	<title><?php if (@constant('SITE_NAME')) {
echo @constant('SITE_NAME');?>
 | Admin<?php } else { ?>Job board Admin<?php }?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
" />
	<meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['meta_keywords']->value;?>
" />
	<link rel="shortcut icon" href="jobboard.f9portal.com/../../fav.png" type="image/x-icon" />

	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/bootstrap/css/bootstrap.min.css" type="text/css" /> 
	<link href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/bootstrap/css/flat-ui.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
_tpl/<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
/css/main.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/admin/lib/style.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/admin/lib/flat-green.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/jquery-1.11.2.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/admin/js/functions.js" type="text/javascript"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/admin/js/main.js" type="text/javascript"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
js/admin/js/overlay.js" type="text/javascript"><?php echo '</script'; ?>
>
</head>

<body>
	

	
<?php }
}
