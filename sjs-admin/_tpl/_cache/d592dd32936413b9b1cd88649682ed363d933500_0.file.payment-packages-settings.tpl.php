<?php
/* Smarty version 3.1.30, created on 2018-10-24 17:29:21
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/payment-packages-settings.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bd09de1a88bb8_24943743',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd592dd32936413b9b1cd88649682ed363d933500' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/payment-packages-settings.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bd09de1a88bb8_24943743 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="admin-content">
	<div class="admin-wrap-content" >

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				Packages Settings
			</label>

			<div class="subheading mt10" id="mode-desc">
				Create your pricing plans and assign package resources. ( <a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/">&larr;go back</a> ) <br /><span class="alizarin"> Manage PayPal credentials <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/fees">HERE</a>.</span>
			</div>

			<hr />
		</div>

		<form id="packages_form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/packages/updated" role="form">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ml0 pl0">
				<div class="package-box">
					
					<span class="bl"><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['package_data']->value[0]['name'], 'UTF-8');?>
</span>
					<br /><br />

					<div class="form-group">
						<label>Name: </label><input class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['id'];?>
_name" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['name'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Jobs: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['id'];?>
_jl" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['jobs_left'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Job period: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['id'];?>
_jp" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['job_period'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Resume downloads: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['id'];?>
_cvd" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['cv_downloads'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Price: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['id'];?>
_price" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[0]['price'];?>
" />&nbsp;
					</div>

				</div>

				<div class="package-box ml75Desk"></span>
								
					<span class="bl"><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['package_data']->value[1]['name'], 'UTF-8');?>
</span>
					<br /><br />

					<div class="form-group">
						<label>Name: </label><input class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['id'];?>
_name" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['name'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Jobs: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['id'];?>
_jl" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['jobs_left'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Job period: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['id'];?>
_jp" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['job_period'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Resume downloads: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['id'];?>
_cvd" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['cv_downloads'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Price: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['id'];?>
_price" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[1]['price'];?>
" />&nbsp;
					</div>

				</div>

				<div class="package-box ml75Desk">

					<span class="bl"><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['package_data']->value[2]['name'], 'UTF-8');?>
</span>
					<br /><br />

					<div class="form-group">
						<label>Name: </label><input class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['id'];?>
_name" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['name'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Jobs: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['id'];?>
_jl" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['jobs_left'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Job period: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['id'];?>
_jp" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['job_period'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Resume downloads: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['id'];?>
_cvd" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['cv_downloads'];?>
" />&nbsp;
					</div>

					<div class="form-group">
						<label>Price: </label><input type="number" class="form-control minput" type="text" name="pid<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['id'];?>
_price" value="<?php echo $_smarty_tpl->tpl_vars['package_data']->value[2]['price'];?>
" />&nbsp;
					</div>

				</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ml0 pl0">
					<div class="button-holder-admin" class="mt30">
						<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" >Save</button>

						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/" style="text-decoration: none;">
							<button type="button" class="btn btn-default btn-warning mbtn" name="button" id="button" >Go back</button>
						</a>
					</div>
			</div>

		</form>

    </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['POPUP']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Settings saved');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
