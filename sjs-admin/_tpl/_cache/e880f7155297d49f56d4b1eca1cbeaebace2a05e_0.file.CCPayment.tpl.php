<?php
/* Smarty version 3.1.30, created on 2019-12-19 09:33:15
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/CCPayment.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5dfb983b3ded72_52467610',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e880f7155297d49f56d4b1eca1cbeaebace2a05e' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/CCPayment.tpl',
      1 => 1576769590,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5dfb983b3ded72_52467610 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- CCPayment.tpl -->
<?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://unpkg.com/axios/dist/axios.min.js"><?php echo '</script'; ?>
>
<div class="admin-content">
    <div  id="app" class="admin-wrap-content" style="padding-right: 35px !important;">
      <h1>Credit Card Payment</h1>
        <div>
            <label>Amount:</label>
            <input v-model="info.value"/>
            &nbsp
            <button v-on:click="sendViaAjax">Save Payment</button>
        </div>
        <div v-if="update_successful">Update Successful</div>
    </div>
</div>    
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
>
    var app = new Vue({
      el: '#app',
      delimiters: ['%%', '%%'],
      created() {
        window.addEventListener('keydown', (e) => {
          app.update_successful = false
        });
      },
      data () {
        return {
            info: null,
            update_successful: false
        }
      },
      mounted () {
        axios
          .get('https://jobboard.ferret9.com/sjs-admin/getCCprice')
          .then(
            response => (this.info = response.data)
            )
      },
      methods: {
        sendViaAjax: function(){          
            axios
                .post('https://jobboard.ferret9.com/sjs-admin/setCCprice', {
                  ccprice: app.info.value
                })
                .then(function (response) {
                  app.update_successful = true
            })
        }
      }
    })    
<?php echo '</script'; ?>
><?php }
}
