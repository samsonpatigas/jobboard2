<?php
/* Smarty version 3.1.30, created on 2020-01-04 20:43:04
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/success/success.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5e114d3851b928_17594213',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e0adf84fe94d0a8e80928527b07e7203b9d512b9' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/success/success.tpl',
      1 => 1569868472,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5e114d3851b928_17594213 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
  <div class="admin-wrap-content">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<label class="admin-label" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['admin']['operation_success_label'];?>
</label>
		<div class="alert alert-info fade in main-color mt10">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
		     <i class="fa fa-info-circle info-fa" aria-hidden="true"></i>&nbsp;
		    <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

		</div>
	</div>
  </div>
</div><!-- #content -->

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
