<?php
/* Smarty version 3.1.30, created on 2019-07-15 06:56:07
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/job.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d2c15773061c7_98165488',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a8e3e1bb3259f8fc5a448cf5074218170fb73e6a' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/job.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:job-details.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d2c15773061c7_98165488 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
		<div class="admin-content">
		  <div class="admin-wrap-content">
			<strong><a href="<?php echo $_smarty_tpl->tpl_vars['back_link']->value;?>
" title="home"><button class="btn btn-default back-button">back</button></a></strong><br />
		
			<div id="job-content">
				<?php $_smarty_tpl->_subTemplateRender("file:job-details.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			
				<div id="job-bottom">
					<div id="number-views">
						<span class="blue-font">Published on: &nbsp;</span><span class="moderate-black"><?php echo $_smarty_tpl->tpl_vars['job']->value['post_date'];?>
</span><br />
						<span class="blue-font">Viewed: &nbsp;</span><span class="moderate-black"><?php echo $_smarty_tpl->tpl_vars['job']->value['views_count'];?>
 times </span> 
					</div>
					<div class="clear"></div>
				</div>
			</div>

			<div id="job-applicants">
				<h4 class="dark-font" style="margin-bottom: 0px"><?php echo count($_smarty_tpl->tpl_vars['applicants']->value);?>
 applicant(s)<?php if (count($_smarty_tpl->tpl_vars['applicants']->value) > 0) {?>, latest on top<?php }?></h4>

				<ul class="applicants-list">  
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['applicants']->value, 'applicant', false, NULL, 'cvs', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['applicant']->value) {
?> 
					    <li style="overflow: hidden;"> 
					    	<div style="float: left;">
					    		<span class="applicant-name"><?php echo $_smarty_tpl->tpl_vars['applicant']->value['name'];?>
</span><br /><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['applicant']->value['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['applicant']->value['email'];?>
</a>
						        <div style="font-size: 11px;">
						            applied 
									 on <strong><?php echo $_smarty_tpl->tpl_vars['apply_dates']->value[$_smarty_tpl->tpl_vars['applicant']->value['id']];?>
</strong>
						        </div>
					    	</div>
					    	<?php if ($_smarty_tpl->tpl_vars['applicant']->value['cv_path'] != '') {?>
					    	<div style="float: right;">
					    		 <div title="Download cv"><br /><a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['applicant']->value['cv_path'];?>
"><i class="<?php echo $_smarty_tpl->tpl_vars['cvs']->value[$_smarty_tpl->tpl_vars['applicant']->value['id']];?>
" aria-hidden="true"></i></a></div>
					    	</div>
					    	 <?php }?>
					    </li>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				</ul>
			</div>
		</div><!-- /content -->
		</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
