<?php
/* Smarty version 3.1.30, created on 2018-10-26 14:33:32
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/edit-post.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bd317accc4ec8_60911665',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dde27e374dbb7f063b85f1b092026cbb81b22121' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/edit-post.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bd317accc4ec8_60911665 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


		<div class="admin-content">
	     <div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<label class="admin-label">
				Edit job
			</label>
			<span class="back-area" >
			 	<a href="<?php echo $_smarty_tpl->tpl_vars['HTTP_REFERER']->value;?>
" style="float: right;"><button type="button" class="btn btn-default back-button" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['back'];?>
</button></a>
			 </span>
		</div>

		<br />

		<div class="container-fluid ">

			<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
post-edited/" role="form">
				<input type="hidden" id="job_id" name="job_id" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
" />
				<input type="hidden" id="referer" name="referer" value="<?php echo $_smarty_tpl->tpl_vars['referer']->value;?>
" />

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb20">

					<!-- catgory-->
					<div class="form-group mb20">
						<label class="grayLabel" for="type"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_type_label'];?>
</label>
						<select id="type_select" name="type_select" class="form-control minput">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['types']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
								<option <?php if ($_smarty_tpl->tpl_vars['value']->value == $_smarty_tpl->tpl_vars['job']->value['type_name']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

						</select>
					</div>

					<!-- job types-->
					<div class="form-group mb20">
						<label class="grayLabel" for="type"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_category_label'];?>
</label>
						<select id="cat_select" name="cat_select" class="form-control minput">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cats']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
								<option <?php if ($_smarty_tpl->tpl_vars['id']->value == $_smarty_tpl->tpl_vars['job']->value['category_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

						</select>
					</div>

					<?php if ($_smarty_tpl->tpl_vars['remote_portal']->value == 'deactivated') {?>
						<div class="grayLabel form-group mb20">
							<label for="description">Location</label>
							<select id="location_select" name="location_select" class="form-control minput">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'value', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
								<option <?php if ($_smarty_tpl->tpl_vars['id']->value == $_smarty_tpl->tpl_vars['job']->value['city_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</select>
						</div>
					<?php }?>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="grayLabel form-group mb20">
						<label for="title"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_title_label'];?>
</label>
						<input required name="title" id="title" maxlength="400" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
" />
					</div>

					<div class="grayLabel form-group ">
						<label for="salary"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['salary_label'];?>
</label>
						<input <?php if ($_smarty_tpl->tpl_vars['lock_post']->value) {?>disabled<?php }?> name="salary" id="salary" maxlength="100" type="text" class="form-control minput" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
" />
					</div>
				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mlpl0">
						<div class="form-group mb20 grayLabel">
							<label for="description"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['post_desc_label'];?>
</label>
							<textarea id="description" name="description"><?php echo $_smarty_tpl->tpl_vars['job']->value['description'];?>
</textarea>
						</div>
					</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">

					 <div class="form-group mb20">
					 	<input type="checkbox" onchange="applyChanged(this.checked);" name="apply_online_switch" id="apply_online_switch" data-size="mini" <?php if ($_smarty_tpl->tpl_vars['job']->value['apply_online'] == '1') {?>checked<?php }?> /><label style="margin-left: 10px; margin-bottom" class="switch-label mt25 grayLabel"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_label'];?>
</label><span class="apply-desc-span"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['apply_desc'];?>
</span>
					 </div>

					 <div id="apply-desc-block" class="form-group mb20 <?php if ($_smarty_tpl->tpl_vars['job']->value['apply_online'] == '1') {?> displayNone<?php }?>" >
						<label class="green"><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['howto_apply_label'];?>
</label>
						<input id="howtoTA" class="form-control minput" rows="5" cols="5" id="howtoapply" name="howtoapply" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['apply_desc'];?>
"></input>
					 </div>
				</div>
					 <br /><br />
				 </div>
				  <br />

				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="form-group mb20">
					  	<button  type="submit" onclick="return validateDesc();" class="btn btn-default btn-primary mbtn" name="submit" id="submit" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['change_password_submit'];?>
</button>

				  		<a href="<?php echo $_smarty_tpl->tpl_vars['HTTP_REFERER']->value;?>
"><button type="button" class="right-btn btn btn-default btn-warning mbtn" ><?php echo $_smarty_tpl->tpl_vars['translations']->value['dashboard_recruiter']['cancel'];?>
</button></a>
					  </div>
				  </div>

			</form>
			<br />
			</div>
	</div><!-- /content -->
	</div>


<?php echo '<script'; ?>
 type="text/javascript">
	$(document).ready(function(){
		var theme = "<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
";
		tinymce.init({selector:'textarea:not(.noTinymceTA)', content_css : "/_tpl/" + theme + "/1.5/css/custom-editor.css", height : 300, resize: 'both' , theme: 'modern', toolbar1: 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image', toolbar2: 'preview media | forecolor emoticons', plugins: ["paste advlist autolink lists link image charmap preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars media nonbreaking save table contextmenu directionality emoticons template textcolor colorpicker textpattern "], paste_retain_style_properties: "color font-style font-size",paste_webkit_styles: "color font-style font-size" });
	});

	function applyChanged(val) {
		if (val == false) {
			$('#apply-desc-block').removeClass('displayNone');
		} else {
			$('#howtoTA').val('');
			$('#apply-desc-block').addClass('displayNone');
		}
	}

	function validateDesc() {
		if (tinymce.activeEditor.getContent() == "") {
			alert("Please, fill in the job description");
			return false;
		} else return true;
	}

<?php echo '</script'; ?>
>
		

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
