<?php
/* Smarty version 3.1.30, created on 2019-11-24 09:25:56
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/edit-page.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ddaa10467bef4_76727951',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '23e9599c976a96670ed690195041bd480ffb7b1d' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/edit-page.tpl',
      1 => 1573584873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5ddaa10467bef4_76727951 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /sjs-admin/_tpl/edit-page.tpl -->
		<div class="admin-content">
		  <div class="admin-wrap-content" style="padding-right: 35px !important;">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<label class="admin-label">Edit page</label>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['id'] == '53' || $_smarty_tpl->tpl_vars['data']->value['id'] == '14') {?>
				<br />
				<div class="subheading">
				If you change URL for this page also update "_config/constants.php" via file manager.
				</div>
				<br />
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['id'] == '19' || $_smarty_tpl->tpl_vars['data']->value['id'] == '20' || $_smarty_tpl->tpl_vars['data']->value['id'] == '2' || $_smarty_tpl->tpl_vars['data']->value['id'] == '21' || $_smarty_tpl->tpl_vars['data']->value['id'] == '55') {?>
					<br /><br />

					<?php if ($_smarty_tpl->tpl_vars['data']->value['id'] == '19') {?>

					<div class="subheading"> <?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>
 page is dynamic. Design can be changed only in "_tpl/default/rss/rss.tpl" file. ( <a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/">&larr;go back</a> )</div>

					<?php } elseif ($_smarty_tpl->tpl_vars['data']->value['id'] == '20') {?>

					<div class="subheading"> <?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>
 page is dynamicaly generated and does not use any template. You can delete it from the menu. ( <a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/">&larr;go back</a> )</div>

					<?php } elseif ($_smarty_tpl->tpl_vars['data']->value['id'] == '21') {?>
						<div class="subheading"> <?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>
 page is dynamic. Design can be changed only in "_tpl/default/subscription/subscribe.tpl" file.( <a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/">&larr;go back</a> )</div>
					<?php } else { ?>
						<div class="subheading"> <?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>
 page is dynamic. Content can be changed only in "_tpl/default/static/static_<?php echo $_smarty_tpl->tpl_vars['data']->value['url'];?>
.tpl" file. You can delete it and create new page with your layout. ( <a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/">&larr;go back</a> )</div>
					<?php }?>


				<?php } else { ?>
					<div class="subheading">( <a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/">&larr;go back</a> )</div>
				<?php }?>

				<br />
			</div>
		<?php if ($_smarty_tpl->tpl_vars['data']->value['id'] != '19' && $_smarty_tpl->tpl_vars['data']->value['id'] != '20' && $_smarty_tpl->tpl_vars['data']->value['id'] != '2' && $_smarty_tpl->tpl_vars['data']->value['id'] != '21' && $_smarty_tpl->tpl_vars['data']->value['id'] != '55') {?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
pages/update" role="form">
					<input type="hidden" name="id" id="id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">
					<input type="hidden" name="was_external" id="was_external" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['is_external'];?>
">

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="form-group mb30">
						<label for="title">Title</label>
						<input  required name="title" id="title" maxlength="50" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>
" class="form-control minput"  />
					</div>
					<div id="url-block" class="form-group <?php if ($_smarty_tpl->tpl_vars['data']->value['is_external'] == '1') {?>displayNone<?php }?>">
						<label for="url">URL</label>
						<input  name="url" id="url" maxlength="50" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['url'];?>
" class="form-control minput"  />
					</div>

					<?php if ($_smarty_tpl->tpl_vars['data']->value['id'] != '19' && $_smarty_tpl->tpl_vars['data']->value['id'] != '20' && $_smarty_tpl->tpl_vars['data']->value['id'] != '2' && $_smarty_tpl->tpl_vars['data']->value['id'] != '21' && $_smarty_tpl->tpl_vars['data']->value['id'] != '55') {?>
					<div class="form-group mb85mobile mb20 mt20">
		 			 	<input <?php if ($_smarty_tpl->tpl_vars['data']->value['is_external'] == '1') {?>checked<?php }?> type="checkbox" onchange="Jobber.pagesExternalSwitched(this.checked);" name="external_switch" id="external_switch" data-size="mini" />
		 			 	<span style="position: absolute; margin-top: -1px">
		 				 	<label style="margin-left: 20px;">external page</label>
		 			 	</span>
		 			 </div>
		 			 <?php }?>

					<div class="form-group mb30">
						<label for="link_order">Order (1-50)</label>
						<input type="number" required class="form-control minput" name="link_order" id="link_order" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['link_order'];?>
" />
					</div>

		 		    <div id="external-page-block" class="form-group <?php if ($_smarty_tpl->tpl_vars['data']->value['is_external'] == '0') {?>displayNone<?php }?> mb30" >
		 			 	<label for="url">External URL <span class="fs13">(eg. http://website.com)</span></label>
						<input  name="external_url" id="external_url" type="text" class="form-control minput" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_external'] == '1') {?>value="<?php echo $_smarty_tpl->tpl_vars['data']->value['external_url'];?>
"<?php }?> />
		 			</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

					<div id="keywords-block" class="form-group mb30 <?php if ($_smarty_tpl->tpl_vars['data']->value['is_external'] == '1') {?>displayNone<?php }?>">
						<label for="keywords">Meta keywords</label>
						<textarea maxlength="50" class="noTinymceTA form-control" name="keywords" id="keywords" rows="3" ><?php echo $_smarty_tpl->tpl_vars['data']->value['keywords'];?>
</textarea>
					</div>
					<div id="desc-block" class="form-group mb30 <?php if ($_smarty_tpl->tpl_vars['data']->value['is_external'] == '1') {?>displayNone<?php }?>">
						<label for="desc">Meta description</label>
						<textarea class="noTinymceTA form-control" name="desc" id="desc" rows="5" ><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
					</div>

				</div>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['id'] != '19' && $_smarty_tpl->tpl_vars['data']->value['id'] != '20' && $_smarty_tpl->tpl_vars['data']->value['id'] != '2' && $_smarty_tpl->tpl_vars['data']->value['id'] != '21' && $_smarty_tpl->tpl_vars['data']->value['id'] != '55') {?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mlpl0">
						<div id="content-block" class="form-group mb30 <?php if ($_smarty_tpl->tpl_vars['data']->value['is_external'] == '1') {?>displayNone<?php }?>">
							<label for="page_content">Content</label>
							<textarea class="form-control minput" name="page_content" id="page_content" rows="10" style="min-width: 300px !important;"><?php echo $_smarty_tpl->tpl_vars['data']->value['content'];?>
</textarea>
						</div>
					</div>
				</div>
				<?php }?>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group" style="margin-top: 10px;">
						<button <?php if ($_smarty_tpl->tpl_vars['data']->value['id'] == '19' || $_smarty_tpl->tpl_vars['data']->value['id'] == '20' || $_smarty_tpl->tpl_vars['data']->value['id'] == '2' || $_smarty_tpl->tpl_vars['data']->value['id'] == '21' || $_smarty_tpl->tpl_vars['data']->value['id'] == '55') {?>disabled<?php }?> type="submit" class="btn btn-default btn-primary mbtn" name="submit" id="submit" >Submit</button>
					</div>
				</div>

				</form>
			</div>
<?php }?>


		</div><!-- #content -->
		</div>


<?php echo '<script'; ?>
 type="text/javascript">
	$(document).ready(function(){
		var theme = "<?php echo $_smarty_tpl->tpl_vars['THEME']->value;?>
";
		tinymce.init({selector:'textarea:not(.noTinymceTA)', content_css : "/_tpl/" + theme + "/1.5/css/custom-editor.css", height : 400, resize: 'both' , theme: 'modern', toolbar1: 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image', toolbar2: 'preview media | forecolor emoticons', plugins: ["paste advlist autolink lists link image charmap preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars media nonbreaking save table contextmenu directionality emoticons template textcolor colorpicker textpattern "], paste_retain_style_properties: "color font-style font-size",paste_webkit_styles: "color font-style font-size" });
	});
<?php echo '</script'; ?>
>
	

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
