<?php
/* Smarty version 3.1.30, created on 2019-10-03 16:05:33
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/seo-edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d960e3d99d104_83155158',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e38701ec78ac087be19050f2b326e4eccdd406f6' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/seo-edit.tpl',
      1 => 1569868470,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d960e3d99d104_83155158 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
  <div class="admin-wrap-content" >
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<form id="publish_seo" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/<?php echo $_smarty_tpl->tpl_vars['CURRENT_ID']->value;?>
/update">
	<div class="list settings">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fields']->value, 'setting');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['setting']->value) {
?>

				<?php $_smarty_tpl->_assignInScope('name', $_smarty_tpl->tpl_vars['setting']->value[0]);
?>
				<?php $_smarty_tpl->_assignInScope('value', $_smarty_tpl->tpl_vars['setting']->value[1]);
?>
				<?php $_smarty_tpl->_assignInScope('desc', $_smarty_tpl->tpl_vars['setting']->value[2]);
?>
				<?php $_smarty_tpl->_assignInScope('title', $_smarty_tpl->tpl_vars['setting']->value[3]);
?>

				<div class="row settings-row fs13" >
					<div class="row-fluid mt15" >

						<div class="col-lg-2 col-md-2 col-sm-2">
							<strong><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
:</strong>
						</div>

						<div class="col-lg-3 col-md-3  col-sm-8 ml20Desk">
							<input class="form-control minput" type="text" name="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8', true);?>
" size="120" />&nbsp;
						</div>

						<div class="col-lg-3 col-md-3 col-sm-8">
							<?php echo $_smarty_tpl->tpl_vars['desc']->value;?>

						</div>
					</div>
				</div>

		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			<div class="button-holder-admin" style="margin-top: 30px;">
				<button type="submit" onclick="jobberBase.messages.add('Settings saved');" class="btn btn-default btn-primary mbtn" name="submit" id="save" >Save</button>

				<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
seo/" style="text-decoration: none;">
					<button type="button" class="btn btn-default btn-warning mbtn" name="button" id="button" >Go back</button>
				</a>
			</div>
		</div>
		</form>

	</div>
</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
