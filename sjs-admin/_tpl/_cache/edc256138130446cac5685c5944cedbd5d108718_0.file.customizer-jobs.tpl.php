<?php
/* Smarty version 3.1.30, created on 2019-08-30 14:05:19
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/customizer-jobs.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d691f0fd9c3f0_27799369',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'edc256138130446cac5685c5944cedbd5d108718' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/customizer-jobs.tpl',
      1 => 1565267001,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d691f0fd9c3f0_27799369 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
 <div class="admin-wrap-content">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	<label class="admin-label">SJS CUSTOMIZER</label>
	<div class="subheading">Job listing page</div>
	<p>(<a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customizer/">&larr;go back</a>)</p>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt25">
	    <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
customize-jobs" role="form" enctype="multipart/form-data">

			<div class="form-group">
 
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0 mb25">

			   		<label>Subheader image</label><br />

				   	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mlpl0">
						<label id="bannerLabel" for="jobs_subheader_bg_path">Upload new</label>
					 	<input type="file" name="jobs_subheader_bg_path" id="jobs_subheader_bg_path" class="form-control inputfile minput" />
					 	<div class="textarea-feedback" >[recommended size ratio 1600x500]</div>
			 	
					</div>

					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mlpl0">
						<img width="250" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['customizer_data']->value['jobs_subheader_bg_path'];?>
" />
					</div>

				</div>
				<br /><br /><br />

				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mlpl0">
				   <label>Logo width (use "%" or "px")</label>
				   <input value="<?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['jobs_logo_width'];?>
" class="form-control" type="text" name="jobs_logo_width" id="jobs_logo_width" size="100" />
				   <br />

				   <label>Mobile logo width</label>
				   <input value="<?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['jobs_logo_width_mobile'];?>
" class="form-control" type="text" name="jobs_logo_width_mobile" id="jobs_logo_width_mobile" size="100" />
				   <br />

				   <label>Logo padding</label>
				   <input value="<?php echo $_smarty_tpl->tpl_vars['customizer_data']->value['jobs_logo_padding'];?>
" class="form-control" type="text" name="jobs_logo_padding" id="jobs_logo_padding" size="100" />
				   <br />
				   
				</div>
 
				<?php if ($_smarty_tpl->tpl_vars['PROFILE_PLUGIN']->value == '1') {?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
						<div class="form-group" >
				
						   <input type="checkbox" name="jobs_candidates_on_flag" id="jobs_candidates_on_flag" data-size="mini" <?php if ($_smarty_tpl->tpl_vars['customizer_data']->value['jobs_candidates_on_flag'] == '1') {?>checked<?php }?> /><label style="margin-left: 10px;" class="switch-label mt25">Turn top menu candidate links ON/OFF</label>
						   <div class="textarea-feedback" >[If you do not wish candidates to add resumes and sign up, you can disable it from menu]</div>
						</div>
						<br />
					</div>
				<?php }?>

			</div>
			<br /><br />

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mlpl0">
 				<button style="margin-top: 30px;" type="submit"  class="btn btn-default btn-primary mbtn" name="submit" id="submit" class="mt25" >SAVE</button>
 			</div>

		</form>
    </div>

    </div>
</div>


<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function() {

		$('#jobs_subheader_bg_path').change(function() {
			var fname = $('input[type=file]').val().split('\\').pop();
			if( fname )
				$('#bannerLabel').html(fname);
			else
				$('#bannerLabel').html($('#bannerLabel').html());
        });

        });
<?php echo '</script'; ?>
>


<?php if ($_smarty_tpl->tpl_vars['updated']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Design updated');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
