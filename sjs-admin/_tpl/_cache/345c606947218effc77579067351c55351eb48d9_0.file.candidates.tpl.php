<?php
/* Smarty version 3.1.30, created on 2018-10-26 14:32:40
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/candidates.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bd317784a03d9_16771863',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '345c606947218effc77579067351c55351eb48d9' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/candidates.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bd317784a03d9_16771863 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
<div class="admin-content">
 <div class="admin-wrap-content">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	<label class="admin-label">Candidates</label>
	<div class="subheading">Red color indicates that candidate has not confirmed his email address yet.</div>
	</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="applicants-list ">  
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['candidates']->value, 'candidate', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['candidate']->value) {
?> 
				    <li class="p40"> 

				    <span><span class="<?php if ($_smarty_tpl->tpl_vars['candidate']->value['candidate_confirmed'] == '1') {?>green<?php } else { ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['candidate']->value['candidate_name'];?>
</span>&nbsp;/&nbsp;<?php echo $_smarty_tpl->tpl_vars['candidate']->value['candidate_email'];?>
</span>

			    	<div style="float:right;">

				    	<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/<?php echo $_smarty_tpl->tpl_vars['candidate']->value['candidate_id'];?>
"><button type="submit" class="btn btn-default btn-primary mbtn" style="width: 85px !important; background-color: #E74C3C">Detail</button></a>

				    </div>


					</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</ul>
			<br />
			<div class="pagination"><?php echo $_smarty_tpl->tpl_vars['pages']->value;?>
</div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['deletedPopup']->value == 'true') {?>
      <?php echo '<script'; ?>
 type="text/javascript">
       setTimeout(function(){
       	jobberBase.messages.add('Candidate has been notified and deleted');
       }, 1000);
      <?php echo '</script'; ?>
>
    <?php }?>
 </div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
