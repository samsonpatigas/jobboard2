<?php
/* Smarty version 3.1.30, created on 2019-05-22 00:51:28
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/cities.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ce48f000eb301_46455017',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24b15170ffd841b5ee91bed8b1eb4e56295a3579' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/cities.tpl',
      1 => 1539093762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5ce48f000eb301_46455017 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="admin-content">
		<div class="admin-wrap-content" style="padding-right: 35px !important;">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<label class="admin-label">Locations</label>
				<?php if ($_smarty_tpl->tpl_vars['REMOTE_PORTAL']->value == 'activated') {?>
				<div class="subheading">Remote portal is activated. Cities wont be shown. Change <a target="_blank" href="/sjs-admin/settings/main/"> it.</a></div> <br />
				<?php }?>
			</div>

				<div class="ml15">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="list">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'city');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['city']->value) {
?>
							<div class="row settings-row p15" id="item<?php echo $_smarty_tpl->tpl_vars['city']->value['id'];?>
" >
								<div class="icons">
									<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cities/prepare-edit/<?php echo $_smarty_tpl->tpl_vars['city']->value['id'];?>
/" title="Edit this city"><i class="fa fa-gear fa-lg mr10" aria-hidden="true"></i></a>
									<a href="javascript:void(0);" title="Delete this city" onclick="jobberBase.deleteCity('<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cities/delete/', <?php echo $_smarty_tpl->tpl_vars['city']->value['id'];?>
)"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
								</div>
								<span><?php echo $_smarty_tpl->tpl_vars['city']->value['name'];?>
</span> - ( <?php echo $_smarty_tpl->tpl_vars['city']->value['ascii_name'];?>
 )
							</div>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

						</div>
						<p style="margin: 20px -10px;"><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
cities/prepare-add/" title="Add a new city"><span title="Add new city" class="blueColor fui-plus-circle iconSize" aria-hidden="true"></span></a></p>
					</div>
				</div>
		</div><!-- #content -->
		</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php if ($_smarty_tpl->tpl_vars['cityAdded']->value) {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Location has been created');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['cityEdited']->value) {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Location has been updated');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }
}
}
