<?php
/* Smarty version 3.1.30, created on 2019-12-11 12:50:16
  from "/home1/fninport/public_html/jobboard/sjs-admin/_tpl/review_applicants.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5df13a68bf69f1_57217877',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '741ac801da89cf47666066a3f08e378495e3b8e5' => 
    array (
      0 => '/home1/fninport/public_html/jobboard/sjs-admin/_tpl/review_applicants.tpl',
      1 => 1575983919,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5df13a68bf69f1_57217877 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- /sjs-admin/_tpl/review_applicants.tpl -->
<div class="admin-content">
 <div class="admin-wrap-content">
    <div class="row">
    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 ">
            <form method="POST">
                    <label class="admin-label">Filter by Publish: </label>
                    <div class="checkbox">
                        <button name="published" id="published" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['publish']->value;?>
" type="submit">Published</button>
                    </div>
                    <div class="checkbox">
                        <button name="notpublished" id="notpublished" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['notpublish']->value;?>
" type="submit">Not Published</button>                     
                    </div>
                    <div class="checkbox">
                        <button name="allpublished" id="allpublished" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['allpublish']->value;?>
" type="submit">All</button>                   
                    </div>
        
                    <label class="admin-label">Filter by Review: </label>
                    <div class="checkbox">
                        <button name="reviewed" id="reviewed" class="btn btn-default btn-primary mbtn" type="submit" style="<?php echo $_smarty_tpl->tpl_vars['reviewed']->value;?>
">Reviewed</button>
                    </div>
                    <div class="checkbox">
                        <button name="notreviewed" id="notreviewed" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['notreviewed']->value;?>
" type="submit">Not Reviewed</button>
                    </div>
                    <div class="checkbox">
                        <button name="allreviewed" id="allreviewed" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['allreviewed']->value;?>
" type="submit">All</button>
                    </div>
                    <label class="admin-label">Filter by Search: </label>
                    <input name="search" id="search" type="text" class="mbtn" placeholder="Search" value="<?php echo $_smarty_tpl->tpl_vars['searchtext']->value;?>
">
                    <div class="checkbox"> 
                        <button name="btnsearch" id="btnsearch" class="btn btn-default btn-primary mbtn" style="<?php echo $_smarty_tpl->tpl_vars['btnsearch']->value;?>
" type="submit">Search</button>
                    </div>          
            </form>
        </div>
            <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                <ul class="applicants-list ">  
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['candidates']->value, 'candidate', false, NULL, 'obj', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['candidate']->value) {
?> 
                        <li class="p40"> 
                           <span>
                                <span class="<?php if ($_smarty_tpl->tpl_vars['candidate']->value['public_profile'] == '1') {?>green<?php } else { ?>red<?php }?>" style="<?php if ($_smarty_tpl->tpl_vars['candidate']->value['is_Seen'] == '0') {?>font-weight: bold;<?php } else { ?> <?php }?>"><?php echo $_smarty_tpl->tpl_vars['candidate']->value['candidate_name'];?>
</span>&nbsp;/&nbsp;<?php echo $_smarty_tpl->tpl_vars['candidate']->value['candidate_email'];?>

                            </span>
                            <div style="float:right;">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
review_applicant/<?php echo $_smarty_tpl->tpl_vars['candidate']->value['candidate_id'];?>
"><button type="submit" class="btn btn-default btn-primary mbtn" style="width: 85px !important; background-color: #E74C3C">DetailS</button></a>
                            </div>
                        </li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </ul>
                <br />
                <div class="pagination"><?php echo $_smarty_tpl->tpl_vars['pages']->value;?>
</div>
             </div>

 
</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
