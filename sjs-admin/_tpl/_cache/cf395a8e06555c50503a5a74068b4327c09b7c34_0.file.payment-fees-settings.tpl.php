<?php
/* Smarty version 3.1.30, created on 2018-10-24 18:10:11
  from "/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/payment-fees-settings.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bd0a77397ce06_20461753',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cf395a8e06555c50503a5a74068b4327c09b7c34' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard-dev/sjs-admin/_tpl/payment-fees-settings.tpl',
      1 => 1539093761,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bd0a77397ce06_20461753 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<div class="admin-content">
	<div class="admin-wrap-content" >

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				Fees settings
			</label>
			<div class="subheading mt10">Charge companies flat fees for job posting, premium / spotlight ads and access to Resume database ( <a style="opacity: 0.8;" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/">&larr;go back</a> ).</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<form id="fees_form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/fees/updated" role="form">
			<div class="list settings">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fields']->value, 'setting');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['setting']->value) {
?>

				<?php $_smarty_tpl->_assignInScope('name', $_smarty_tpl->tpl_vars['setting']->value[0]);
?>
				<?php $_smarty_tpl->_assignInScope('value', $_smarty_tpl->tpl_vars['setting']->value[1]);
?>
				<?php $_smarty_tpl->_assignInScope('title', $_smarty_tpl->tpl_vars['setting']->value[2]);
?>
				<?php $_smarty_tpl->_assignInScope('desc', $_smarty_tpl->tpl_vars['setting']->value[3]);
?>
				<?php $_smarty_tpl->_assignInScope('input_type', $_smarty_tpl->tpl_vars['setting']->value[4]);
?>
				<?php $_smarty_tpl->_assignInScope('input_options', $_smarty_tpl->tpl_vars['setting']->value[5]);
?>

				<?php if ($_smarty_tpl->tpl_vars['input_type']->value == 'select') {?>
				 <?php $_smarty_tpl->_assignInScope('select_options', $_smarty_tpl->tpl_vars['setting']->value[6]);
?>
				<?php }?>

				<div class="row settings-row fs13" >
					<div class="row-fluid mt15" >

						<div class="col-lg-2 col-md-2 col-sm-2">
							<strong><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
:</strong>
						</div>

						<?php if ($_smarty_tpl->tpl_vars['input_type']->value == 'select') {?>
							 <div class="col-lg-3 col-md-3 col-sm-8 ml20Desk">
								<select class="form-control minput mbm20" name="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
">
									<?php
$__section_tmp_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp'] : false;
$__section_tmp_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['select_options']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_tmp_0_total = $__section_tmp_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_tmp'] = new Smarty_Variable(array());
if ($__section_tmp_0_total != 0) {
for ($__section_tmp_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] = 0; $__section_tmp_0_iteration <= $__section_tmp_0_total; $__section_tmp_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']++){
?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['select_options']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)];?>
" <?php if ($_smarty_tpl->tpl_vars['select_options']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)] == $_smarty_tpl->tpl_vars['value']->value) {?>selected<?php }?> ><?php echo $_smarty_tpl->tpl_vars['select_options']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmp']->value['index'] : null)];?>
</option>
									<?php
}
}
if ($__section_tmp_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_tmp'] = $__section_tmp_0_saved;
}
?>
								</select>
							</div>
						<?php } else { ?>
							<div class="col-lg-3 col-md-3 col-sm-8 ml20Desk">
								<input class="form-control minput" type="text" name="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" />&nbsp;
							</div>
						<?php }?>

						<div class="col-lg-3 col-md-3 col-sm-8">
							<?php echo $_smarty_tpl->tpl_vars['desc']->value;?>

						</div>
					</div>
				</div>

				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				<div class="mt30">
					<div class="button-holder-admin" class="mt30">
						<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" >Save</button>

						<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
payment-settings/" style="text-decoration: none;">
							<button type="button" class="btn btn-default btn-warning mbtn" name="button" id="button" >Go back</button>
						</a>
					</div>
				</div>

			</div>
		</form>

		</div>
</div>
</div>

<?php if ($_smarty_tpl->tpl_vars['POPUP']->value == 'true') {
echo '<script'; ?>
 type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Settings saved');
   }, 1000);
<?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
