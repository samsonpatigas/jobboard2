<?php
/* Smarty version 3.1.30, created on 2019-07-09 17:24:03
  from "/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/candidate.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5d24bfa347acc7_52035604',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b999a603066f9069b1a58f1e50db04ef5e8c5e52' => 
    array (
      0 => '/home3/fninpor1/public_html/jobboard/sjs-admin/_tpl/candidate.tpl',
      1 => 1556559330,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5d24bfa347acc7_52035604 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="admin-content">
	<div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<label class="admin-label">Candidate details</label>
			<div class="subheading"><a href="/sjs-admin/candidates">
					<div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div>
				</a></div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
			<?php if ($_smarty_tpl->tpl_vars['data']->value) {?>

			<ul class="list-group">
				<li class="list-group-item"><strong>Email: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['email'];?>
</li>
				<li class="list-group-item"><strong>Name: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['fullname'];?>
</li>
				<li class="list-group-item"><strong>Positions: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['f9_position'];?>
</li>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['skills_formated']) {?>
				<!-- <li class="list-group-item"><strong>Skills: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['skills_formated'];?>
</li> -->
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['location']) {?>
				<li class="list-group-item"><strong>Location: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['location'];?>
</li>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['phone_carrier']) {?>
				<li class="list-group-item"><strong>Phone Carrier: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['phone_carrier'];?>
</li>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['phone']) {?>
				<li class="list-group-item"><strong>Phone: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['phone'];?>
</li>
				<?php }?>

				<li class="list-group-item"><strong>Cross Street: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['f9_cross_st'];?>
</li>

				<li class="list-group-item"><strong>Profile message: </strong>&nbsp;<a data-toggle="modal"
						data-target="#candidateMsgModal" href="#" onclick="return false;"><button type="submit"
							class="btn btn-default btn-primary mbtn"
							style="width: 85px !important; margin-left: 10px; font-size: 11px; background-color: #E74C3C">View</button></a>
				</li>				

				<?php if ($_smarty_tpl->tpl_vars['data']->value['weblink']) {?>
				<li class="list-group-item"><strong>Portfolio: </strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['weblink'];?>
</li>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['sm_links']) {?>
				<li class="list-group-item">
					<strong><?php echo $_smarty_tpl->tpl_vars['translations']->value['js']['social_media_label'];?>
: </strong>&nbsp;&nbsp;&nbsp;
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['sm_links'], 'SM_OBJ');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['SM_OBJ']->value) {
?>
					<a class="mr12"
						href="<?php if ($_smarty_tpl->tpl_vars['SM_OBJ']->value->whatsapp == 'true') {?>tel:<?php echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->whatsapp_numb;
} else {
echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->linkToShow;
}?>"
						target="_blank"><i class="fa fa-<?php echo $_smarty_tpl->tpl_vars['SM_OBJ']->value->icon;?>
 fa-lg mt10" aria-hidden="true"></i></a>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				</li>
				<?php }?>


				<?php if ($_smarty_tpl->tpl_vars['data']->value['cv_path'] != '') {?>
				<li class="list-group-item"><strong>CV: </strong><a title="Download cv" target="_blank"
						href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['data']->value['cv_path'];?>
"><i class="ml10 <?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
" aria-hidden="true"></i></a></li>
				<?php }?>
				<li class="list-group-item"><strong>Account status: </strong>&nbsp;<?php if ($_smarty_tpl->tpl_vars['data']->value['confirmed'] == '1') {?><span
						class="green">Activated</span><?php } else { ?><span class="red">Deactivated</span><?php }?></li>
				<li class="list-group-item"><strong>Visibility status: </strong>&nbsp;<?php if ($_smarty_tpl->tpl_vars['data']->value['public_profile'] == '1') {?><span
						class="green">Public</span><?php } else { ?><span class="red">Private</span><?php }?>

					<!-- ADDED -->

					<?php if ($_smarty_tpl->tpl_vars['data']->value['public_profile'] == '0') {?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/publish/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">
						<button type="submit" class="btn btn-default btn-primary mbtn"
							style="margin-left: 10px; font-size: 11px; background-color: #E74C3C">PUBLISH ACCOUNT</button>
					</a>
					<?php } else { ?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/unpublish/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">
						<button type="submit" class="btn btn-default btn-primary mbtn"
							style="margin-left: 10px; font-size: 11px; background-color: #E74C3C">UNPUBLISH ACCOUNT</button>
					</a>
					<?php }?>
					<!-- 	END -->

				</li>
				
				<li class="list-group-item"><a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
login-as-candidate/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">&nbsp;Login
						in as this candidate &rarr;</a></li>
				
			</ul>

			<?php }?>

			<form method="POST" action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">
				<li class="list-group-item">
					<input required name="email_app" id="email_app" maxlength="400" type="email" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['email'];?>
"
						class="form-control minput" style="display: inline;" />
					<button name="update_email_app" class="btn btn-default btn-primary mbtn"
						style="margin-left: 10px; font-size: 11px; background-color: #E74C3C">Update Email</button>
				</li>
				<li class="list-group-item">
					<input name="pass_app" id="pass_app" maxlength="400" type="password" class="form-control minput"
						placeholder="Enter new Password" style="display: inline;" />
					<!-- <span toggle="#pass_app" class="fa fa-fw fa-eye field-icon toggle-password" style="float: right;margin-right: 85px;margin-top: -28px;position: relative;z-index: 2;"></span> -->
					<button id="posbtn" name="update_pass_app" class="btn btn-default btn-primary mbtn"
						style="margin-left: 10px; font-size: 11px; background-color: #E74C3C">Update Password</button>
				</li>
			</form>

			<?php if ($_smarty_tpl->tpl_vars['data']->value['confirmed'] == '1') {?>
			<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/unconfirm/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" title="Unconfirm account">
				<button type="submit" class="btn btn-default btn-primary mbtn"
					style="margin-top: 25px; float: left ;margin-left: 40px; background-color: #E74C3C !important;">DEACTIVATE
					ACCOUNT</button>
			</a>
			<?php } else { ?>
			<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/confirm/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" title="Confirm account">
				<button type="submit" class="btn btn-default btn-primary mbtn"
					style="margin-top: 25px; float: left ;margin-left: 40px; background-color: #E74C3C">ACTIVATE ACCOUNT</button>
			</a>
			<?php }?>
			<a style="display: none;" class="ml20Desk" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/delete/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
"
				title="Delete this candidate"
				onclick="if(!confirm('Are you sure you want to delete this candidate?'))return false;"><button type="submit"
					class="btn btn-default btn-primary mbtn"
					style="margin-top: 25px; float: right;margin-right: 70px; background-color: #E74C3C !important;">DELETE
					CANDIDATE</button></a>

			<!-- ADDED DIV FOR jQuery: Flex Schedule -->
			<!-- border:1px solid black;  -->
			<div style="margin-top: 80px;height: 330px;">

				<!-- <?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
Blackline.png -->

				<h4>Flex Schedule<red style="color: red;padding: 10px;">*</red>
				</h4>

				<img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
Blackline.png" id="divider" alt="divider" width="380" style="position: absolute;
z-index: 10;
margin-top: 125px;
margin-left: 70px;
  -ms-transform: rotate(90deg); /* IE 9 */
  -webkit-transform: rotate(90deg); /* Safari 3-8 */
  transform: rotate(90deg);">

				<!-- <style type="text/css">
			
	@-moz-document url-prefix() {

    	img .divider {

        margin-left: 500px;
    }
}

</style> -->

				<!-- datepicker available -->
				<!-- WORKING -->

				<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
				<link rel="stylesheet" type="text/css"
					href="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.css">
				<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"><?php echo '</script'; ?>
>
				<?php echo '<script'; ?>
 src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"><?php echo '</script'; ?>
>
				<?php echo '<script'; ?>

					src="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.js">
				<?php echo '</script'; ?>
>

				<style type="text/css">
					.ui-datepicker {

						width: 300px;
						height: auto;
						margin: 5px auto 0;
						font: 12pt Arial, sans-serif;
						-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
						-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
						box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);

					}

					.ui-datepicker-header {
						background: #7527a0;
						color: white;
						/* border:1px solid black;*/
					}

					.ui-datepicker-title {
						text-align: center;
						/*border:1px solid black;*/
					}

					.ui-datepicker th {
						/*border:1px solid black;*/
						/*	width: 500px;*/
						color: #7527a0;
						/* margin-bottom: -500px;*/
						/* font-size: 0.6em;*/
						padding: 5px !important;
						height: 15px;

					}

					.ui-datepicker td {
						/*border:1px solid black;*/
						/*	width: 500px;*/
						color: #7527a0;
						/* margin-bottom: -500px;*/
						/* font-size: 0.6em;*/
						padding: 1px !important;
						height: 15px;

					}

					/*div.ui-datepicker{
 font-size:10px;
}*/

					.ui-datepicker td span,
					.ui-datepicker td a {

						/*border:1px solid black;*/
						text-align: center;

					}

					.ui-datepicker .ui-datepicker-calendar .ui-state-highlight a {
						background: #00009c !important;
						color: white !important;
					}

					.ui-datepicker-today a.ui-state-highlight {

						background: #fdff00 !important;
						color: black !important;

					}

					.ui-datepicker-today.ui-datepicker-current-day a.ui-state-highlight {

						background: #fdff00 !important;
						color: black !important;

					}
				</style>

				<input class="minput opaque" type="text" name="avdate" id="avdate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['avdate'];?>
" hidden>
				<input class="minput opaque" type="text" name="navdate" id="navdate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['navdate'];?>
" hidden>

				<form name="select-multiple">
					<!-- width: 335px;height: 280px; -->
					<div id="multiple-date-select" style="margin-top: 40px;margin-left: 85px;">
						<h4 style="font-size: 12pt">Available On&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Not Available On</h4>
					</div>

					<div class="col-6 col-md-4"
						style="z-index: 1000;overflow-y: scroll;margin-top: 12px;margin-left: 0px;display: inline-block; height: 250px;text-align: center;width: 247px;">
						<ol id="list" style="margin-top: 10px;" align="center"></ol>
					</div>

					<div class="col-6 col-md-4"
						style="width: 247px;overflow-y: scroll;margin-top: 11px;margin-left: 12px;display: inline-block; height: 250px;text-align: center; margin-bottom: 40px;">
						<ol id="nlist" style="margin-top: 10px;" align="center"></ol>
					</div>

				</form>

				<?php echo '<script'; ?>
 type="text/javascript">
					var arr = [];
					var dvalue = document.getElementById("avdate").value;
					var narr = [];
					var ndvalue = document.getElementById("navdate").value;

					if (dvalue != null) {

						var res = dvalue.split(", ");

						document.getElementById("list").innerHTML = "";

						for (i = 0; i < res.length; i++) {
							var x = i + 1;
							var y = document.createElement("li");
							y.style.padding = "5px";
							var t = document.createTextNode(res[i]);
							y.appendChild(t);
							document.getElementById("list").appendChild(y);

						}

					}

					if (dvalue == "") {

						document.getElementById("list").innerHTML = "<p style='color: red;padding: 10px;'>No Dates</p>";

					}

					if (ndvalue != null) {

						var res = ndvalue.split(", ");

						document.getElementById("nlist").innerHTML = "";

						for (i = 0; i < res.length; i++) {
							var x = i + 1;
							var y = document.createElement("li");
							y.style.padding = "5px";
							var t = document.createTextNode(res[i]);
							y.appendChild(t);
							document.getElementById("nlist").appendChild(y);

						}



					}

					if (ndvalue == "") {

						document.getElementById("nlist").innerHTML = "<p style='color: red;padding: 10px;'>No Dates</p>";

					}
				<?php echo '</script'; ?>
>

				<!-- WORKING -->
				<!-- END -->


			</div>

			<!-- END -->

		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top: 30px;">


			<!-- COMMENTED THIS 01/03/2019 "MAYBE CAN BE USED SOONER OR LATER" -->

			<!-- 	<h4>Note<red style="color: red;padding: 10px;">*</red></h4>

		<?php if ($_smarty_tpl->tpl_vars['data']->value['note'] != null) {?>

			<p class="green"><?php echo $_smarty_tpl->tpl_vars['data']->value['note'];?>
</p>
		<?php } else { ?>

			<p class="red">No notes added for this user.</p>

		<?php }?> -->

			<!-- END -->

			<?php if ($_smarty_tpl->tpl_vars['data']->value['note'] == null) {?>

			<!-- 	if(isset()) echo "a is set\n"; -->

			<?php if (isset($_GET['success'])) {?>

			<h4>Admin Notes<red style="color: red;padding: 10px;">*</red><small id="notifnote" style="color: green;">Notes
					Added Successfully!</small></h4>

			<textarea id="apply_msg" name="apply_msg" class="apply_msg" placeholder="Add some notes?" maxlength="500"
				rows="10" cols="59" form="usrform"></textarea>

			<?php } else { ?>

			<h4>Admin Notes<red style="color: red;padding: 10px;">*</red><small id="notifnote" style="color: red;">( No notes
					added for this user. )</small></h4>

			<textarea id="apply_msg" name="apply_msg" class="apply_msg" placeholder="Add some notes?" maxlength="500"
				rows="10" cols="59" form="usrform"></textarea>

			<?php }?>

			<?php } else { ?>

			<?php if (isset($_GET['success'])) {?>

			<h4>Admin Notes<red style="color: red;padding: 10px;">*</red><small id="notifnote" style="color: green;">Notes
					Added Successfully!</small></h4>

			<textarea id="apply_msg" name="apply_msg" class="apply_msg" maxlength="500" rows="10" cols="59"
				form="usrform"><?php echo $_smarty_tpl->tpl_vars['data']->value['note'];?>
</textarea>

			<?php } else { ?>

			<h4>Admin Notes<red style="color: red;padding: 10px;">*</red><small id="notifnote" style="color: red;"></small>
			</h4>

			<textarea id="apply_msg" name="apply_msg" class="apply_msg" maxlength="500" rows="10" cols="59"
				form="usrform"><?php echo $_smarty_tpl->tpl_vars['data']->value['note'];?>
</textarea>

			<?php }?>

			<?php }?>


			<div class="textarea-feedback tal" id="textarea_feedback"></div>
			<form action="<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/note/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" method="POST" id="usrform">

				<button type="submit" class="btn btn-default btn-primary mbtn"
					style="margin-top: 10px; background-color: #E74C3C; margin-bottom: 10px;">Save</button>
				<!-- 		<div id="divnote" style="margin-top: 100px"><p id="notifnote" style="color: green;"><?php echo $_smarty_tpl->tpl_vars['notif']->value;?>
</p></div> -->

			</form>

			<!-- ADDED SUCESS NOTIF 1/16/2019 -->

			<!-- 	<?php echo '<script'; ?>
 type="text/javascript">

function addednote() {

  document.getElementById("notifnote").innerHTML = "Notes Added Succesfully!";
  document.getElementById("notifnote").style.color="green";

}
		

	<?php echo '</script'; ?>
> -->

			<!-- ENDPOINT -->


			<style type="text/css">
				@-moz-document url-prefix() {

					.apply_msg {

						width: 570px;
					}
				}
			</style>

			<!-- ADDED 01/03/2019 TEXT AREA WITH BULLETS -->

			<!-- 	<?php echo '<script'; ?>
 type="text/javascript">
		
		$(".apply_msg").focus(function() {
    if(document.getElementById('apply_msg').value === ''){
        document.getElementById('apply_msg').value +='• ';
	}
});
$(".apply_msg").keyup(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        document.getElementById('apply_msg').value +='• ';
	}
	var txtval = document.getElementById('apply_msg').value;
	if(txtval.substr(txtval.length - 1) == '\n'){
		document.getElementById('apply_msg').value = txtval.substring(0,txtval.length - 1);
	}
});

	<?php echo '</script'; ?>
> -->

			<!-- ENDPOINT -->


			<!-- 	ADDED FOR ENROLLED IN DDS Payroll -->

			<h4>Enrolled in DDS Payroll<red style="color: red;padding: 10px;">*</red>
			</h4>

			<?php if (isset($_smarty_tpl->tpl_vars['data']->value['ddspayroll'])) {?>

			<?php if ($_smarty_tpl->tpl_vars['data']->value['ddspayroll'] == 1) {?>

			<?php $_smarty_tpl->_assignInScope('payrollyes', "checked");
?>

			<?php } else { ?>

			<?php $_smarty_tpl->_assignInScope('payrollno', "checked");
?>

			<?php }?>

			<?php } else { ?>

			<?php $_smarty_tpl->_assignInScope('payrollno', "checked");
?>

			<?php }?>


			<ul class="list-unstyled">
				<li class="col-xs-2">
					<label class="container">Yes
						<input type="radio" name="ddspayroll" id="ddspayroll1" value="1" onclick="ddspay();" <?php echo $_smarty_tpl->tpl_vars['payrollyes']->value;?>
>
						<span class="checkmark"></span>
					</label>
				</li>
				<li class="col-xs-2">
					<label class="container">No
						<input type="radio" name="ddspayroll" id="ddspayroll0" value="0" onclick="ddspay();" <?php echo $_smarty_tpl->tpl_vars['payrollno']->value;?>
>
						<span class="checkmark"></span>
					</label>
				</li>
			</ul>

			<?php echo '<script'; ?>
 type="text/javascript">
				function ddspay() {

					if (document.getElementById('ddspayroll1').checked) {

						window.location = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/ddspayroll1/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
';
						console.log("this is YEs");

					}

					if (document.getElementById('ddspayroll0').checked) {

						window.location = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/ddspayroll0/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
';
						console.log("this is no");

					}

				}
			<?php echo '</script'; ?>
>

			<br>
			<br>
			<div style="display:none;">
				<h4>Receive Notifications by:<red style="color: red;padding: 10px;">*</red>
				</h4>

				<?php if (isset($_smarty_tpl->tpl_vars['data']->value['notifyby'])) {?>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['notifyby'] == "Email") {?>

				<?php $_smarty_tpl->_assignInScope('email', "checked");
?>

				<?php } else { ?>

				<?php $_smarty_tpl->_assignInScope('text', "checked");
?>

				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['data']->value['notifyby'] == "Email,Text message") {?>

				<?php $_smarty_tpl->_assignInScope('email', "checked");
?>
				<?php $_smarty_tpl->_assignInScope('text', "checked");
?>

				<?php }?>

				<?php } else { ?>

				<?php $_smarty_tpl->_assignInScope('email', '');
?>
				<?php $_smarty_tpl->_assignInScope('text', '');
?>

				<?php }?>

				<ul class="list-unstyled">
					<li class="col-xs-2">
						<label class="container">Email
							<input type="checkbox" name="notification" id="enotification" onclick="notif();" value="Email" <?php echo $_smarty_tpl->tpl_vars['email']->value;?>
>
							<span class="checkmark"></span>
						</label>
					</li>
					<li class="col-xs-5">
						<label class="container">Text message
							<input type="checkbox" name="notification" id="tmnotification" onclick="notif();" value="Text message"
								<?php echo $_smarty_tpl->tpl_vars['text']->value;?>
>
							<span class="checkmark"></span>
						</label>
					</li>
				</ul>
			</div>
			<?php echo '<script'; ?>
 type="text/javascript">
				function notif() {

					if (document.getElementById('enotification').checked) {

						window.location = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/email/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
';
						console.log("this is email");

					}

					if (document.getElementById('tmnotification').checked) {

						window.location = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/text/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
';
						console.log("this is text");

					}

					if (document.getElementById('enotification').checked && document.getElementById('tmnotification').checked) {

						window.location = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/both/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
';
						console.log("this is both");

					}

					if (document.getElementById('enotification').checked == false && document.getElementById('tmnotification')
						.checked == false) {

						window.location = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL_ADMIN']->value;?>
candidate/none/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
';
						console.log("none is checked");

					}


				}
			<?php echo '</script'; ?>
>

			<style type="text/css">
				.container {
					display: block;
					position: relative;
					padding-left: 35px;
					margin-bottom: 12px;
					margin-top: 5px;
					margin-left: 20px;
					cursor: pointer;
					font-size: 15px;
					-webkit-user-select: none;
					-moz-user-select: none;
					-ms-user-select: none;
					user-select: none;
				}

				.container input {
					position: absolute;
					opacity: 0;
					cursor: pointer;
					height: 0;
					width: 0;
				}

				.checkmark {

					position: absolute;
					top: 0;
					left: 0;
					height: 25px;
					width: 25px;
					background-color: #eee;
				}

				.container:hover input~.checkmark {
					background-color: #ccc;
				}

				.container input:checked~.checkmark {
					background-color: #2196F3;
				}

				.checkmark:after {
					content: "";
					position: absolute;
					display: none;
				}

				.container input:checked~.checkmark:after {
					display: block;
				}

				.container .checkmark:after {
					left: 9px;
					top: 5px;
					width: 5px;
					height: 10px;
					border: solid white;
					border-width: 0 3px 3px 0;
					-webkit-transform: rotate(45deg);
					-ms-transform: rotate(45deg);
					transform: rotate(45deg);
				}
			</style>

			<!-- 	ENDPOINT -->

			<!-- NEW FOR ADD NOTES UPDATE 12/19/18 -->

			<!-- END -->

		</div>


		<div id="candidateMsgModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Profile message</h4>
					</div>
					<div class="modal-body">
						<?php echo $_smarty_tpl->tpl_vars['data']->value['message'];?>

					</div>
					<div class="modal-footer">
						<button style="float: left" type="button" class="btn btn-default btn-warning mbtn"
							data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>


<?php echo '<script'; ?>
 type="text/javascript">
	$(".toggle-password").click(function () {
		$(this).toggleClass("fa-eye fa-eye-slash");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});
<?php echo '</script'; ?>
>


<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
