{include file="header.tpl"} 

<div class="admin-content">
	<div class="admin-wrap-content" >
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				Add new search item for Indeed
			</label>
			<div class="subheading">Select a category and enter the item. Please note that the VALUE field format must follow Indeed API specification. See Indeed Publisher XML API documentation for more details. The NAME field is only presented in the frontend.</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nmpl">
		 <form  method="post" action="{$BASE_URL_ADMIN}indeed/add" role="form" >
		
				    <div class="settings-row" style="overflow: hidden;">
			
					   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				    	  <div class="float-block">
				    		<label class="settings">CATEGORY:</label>
				    			<select id="indeed_category_select" name="indeed_category_select" class="form-control minput">
									{foreach from=$indeed_categories key=id item=value}
										<option value="{$id}">{$value}</option>
									{/foreach}
								</select>
				    	 </div>
				    	</div>

						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						  <div class="float-block">
				    		<label class="settings">NAME:</label>
				    		<input name="name" id="name" type="text"  class="form-control minput"  />
				    	  </div>
				    	</div>

				    	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				    	  <div class="float-block">
				    		<label class="settings">VALUE:</label>
				    		<input name="value" id="value" type="text"  class="form-control minput"  />
				    	 </div>
				    	</div>

					</div>

			<div class="float-block" style="float: left">
				<div class="form-group" style="margin-top: 10px;">
				    <a href="{$BASE_URL_ADMIN}indeed/manage" style="text-decoration: none;">
						<button type="button" class="btn btn-default btn-warning mbtn ml20Desk" name="button" id="button" >Go back</button>
					</a>
					<button type="submit" class="btn btn-default btn-primary mbtn " name="submit" id="submit" >Add</button>
				</div>
			</div>

		 </form>
		</div>
</div>
</div>

{if $added_popup == 'true'}
<script type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('New Indeed item has been added');
   }, 1000);
</script>
{/if}


{include file="footer.tpl"}