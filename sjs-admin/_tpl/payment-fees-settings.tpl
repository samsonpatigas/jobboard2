{include file="header.tpl"} 

<div class="admin-content">
	<div class="admin-wrap-content" >

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				Fees settings
			</label>
			<div class="subheading mt10">Charge companies flat fees for job posting, premium / spotlight ads and access to Resume database ( <a style="opacity: 0.8;" href="{$BASE_URL_ADMIN}payment-settings/">&larr;go back</a> ).</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<form id="fees_form" method="post" action="{$BASE_URL_ADMIN}payment-settings/fees/updated" role="form">
			<div class="list settings">
				{foreach from=$fields item=setting}

				{assign var=name value=$setting[0]}
				{assign var=value value=$setting[1]}
				{assign var=title value=$setting[2]}
				{assign var=desc value=$setting[3]}
				{assign var=input_type value=$setting[4]}
				{assign var=input_options value=$setting[5]}

				{if $input_type == 'select'}
				 {assign var=select_options value=$setting[6]}
				{/if}

				<div class="row settings-row fs13" >
					<div class="row-fluid mt15" >

						<div class="col-lg-2 col-md-2 col-sm-2">
							<strong>{$title}:</strong>
						</div>

						{if $input_type == 'select'}
							 <div class="col-lg-3 col-md-3 col-sm-8 ml20Desk">
								<select class="form-control minput mbm20" name="{$name}">
									{section name=tmp loop=$select_options}
										<option value="{$select_options[tmp]}" {if $select_options[tmp] == $value}selected{/if} >{$select_options[tmp]}</option>
									{/section}
								</select>
							</div>
						{else}
							<div class="col-lg-3 col-md-3 col-sm-8 ml20Desk">
								<input class="form-control minput" type="text" name="{$name}" value="{$value}" />&nbsp;
							</div>
						{/if}

						<div class="col-lg-3 col-md-3 col-sm-8">
							{$desc}
						</div>
					</div>
				</div>

				{/foreach}
				<div class="mt30">
					<div class="button-holder-admin" class="mt30">
						<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" >Save</button>

						<a href="{$BASE_URL_ADMIN}payment-settings/" style="text-decoration: none;">
							<button type="button" class="btn btn-default btn-warning mbtn" name="button" id="button" >Go back</button>
						</a>
					</div>
				</div>

			</div>
		</form>

		</div>
</div>
</div>

{if $POPUP == 'true'}
<script type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Settings saved');
   }, 1000);
</script>
{/if}

{include file="footer.tpl"}