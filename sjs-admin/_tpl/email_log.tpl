{include file="header.tpl"}
<!-- /sjs-admin/_tpl/email_log.tpl -->
<div class="admin-content">
	<div class="admin-wrap-content">	
	                                	
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready(function() {
        $('#example').DataTable({
            "ajax": "https://jobboard.ferret9.com/sjs-admin/page_email_log_api.php",
            "columns": [
                { "data": "created_at" },
                { "data": "send_to_email" },
                { "data": "email_subject" },
                { "data": "email_content" },
                { "data": "email_data" }
            ]
        });
    });
    </script>
    <!-- Scripts -->
    <script>
        setTimeout(function(){
        location.reload();
        },216000);
        </script>
</head>

<body class="is-logged-in has-sidebar">
    <div id="app" style="margin-top: -40px;">
        <section id="section" class="h-pad-xl v-pad-xs">
            <div id="title">
                <h2 class="v-margin-sm" style="
                box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
                -webkit-border-radius: .25rem;
                border-radius: .25rem;
                padding: 20px;
                background: linear-gradient(40deg,#3ab8bc,#39b6b3);
                color: #fff;
                font-size: 20px;
                font-weight: 500;
                ">Email Logs</h2>
            </div>
            <div id="content">
                <div class="fw-container">
                    <div class="fw-body">
                        <div class="content">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Created At</th>
                                        <th>Send To Email</th>
                                        <th>Subject</th>
                                        <th>Content</th>
                                        <th>Data</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Created on</th>
                                        <th>Jobs</th>
                                        <th>Category</th>
                                        <th>Location</th>
                                        <th>Type</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer"></footer>
    </div>
</body>
</div>
	</div>
</div>

{include file="footer.tpl"}
