{include file="header.tpl"}

<div class="admin-content">
	<div class="admin-wrap-content" >

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb15">
			<label class="admin-label">
				Packages Settings
			</label>

			<div class="subheading mt10" id="mode-desc">
				Create your pricing plans and assign package resources. ( <a style="opacity: 0.8;" href="{$BASE_URL_ADMIN}payment-settings/">&larr;go back</a> ) <br /><span class="alizarin"> Manage PayPal credentials <a href="{$BASE_URL_ADMIN}payment-settings/fees">HERE</a>.</span>
			</div>

			<hr />
		</div>

		<form id="packages_form" method="post" action="{$BASE_URL_ADMIN}payment-settings/packages/updated" role="form">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ml0 pl0">
				<div class="package-box">
					
					<span class="bl">{$package_data[0].name|upper}</span>
					<br /><br />

					<div class="form-group">
						<label>Name: </label><input class="form-control minput" type="text" name="pid{$package_data[0].id}_name" value="{$package_data[0].name}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Jobs: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[0].id}_jl" value="{$package_data[0].jobs_left}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Job period: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[0].id}_jp" value="{$package_data[0].job_period}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Resume downloads: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[0].id}_cvd" value="{$package_data[0].cv_downloads}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Price: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[0].id}_price" value="{$package_data[0].price}" />&nbsp;
					</div>

				</div>

				<div class="package-box ml75Desk"></span>
								
					<span class="bl">{$package_data[1].name|upper}</span>
					<br /><br />

					<div class="form-group">
						<label>Name: </label><input class="form-control minput" type="text" name="pid{$package_data[1].id}_name" value="{$package_data[1].name}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Jobs: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[1].id}_jl" value="{$package_data[1].jobs_left}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Job period: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[1].id}_jp" value="{$package_data[1].job_period}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Resume downloads: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[1].id}_cvd" value="{$package_data[1].cv_downloads}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Price: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[1].id}_price" value="{$package_data[1].price}" />&nbsp;
					</div>

				</div>

				<div class="package-box ml75Desk">

					<span class="bl">{$package_data[2].name|upper}</span>
					<br /><br />

					<div class="form-group">
						<label>Name: </label><input class="form-control minput" type="text" name="pid{$package_data[2].id}_name" value="{$package_data[2].name}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Jobs: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[2].id}_jl" value="{$package_data[2].jobs_left}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Job period: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[2].id}_jp" value="{$package_data[2].job_period}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Resume downloads: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[2].id}_cvd" value="{$package_data[2].cv_downloads}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Price: </label><input type="number" class="form-control minput" type="text" name="pid{$package_data[2].id}_price" value="{$package_data[2].price}" />&nbsp;
					</div>

				</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ml0 pl0">
					<div class="button-holder-admin" class="mt30">
						<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" >Save</button>

						<a href="{$BASE_URL_ADMIN}payment-settings/" style="text-decoration: none;">
							<button type="button" class="btn btn-default btn-warning mbtn" name="button" id="button" >Go back</button>
						</a>
					</div>
			</div>

		</form>

    </div>
</div>

{if $POPUP == 'true'}
<script type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Settings saved');
   }, 1000);
</script>
{/if}

{include file="footer.tpl"}