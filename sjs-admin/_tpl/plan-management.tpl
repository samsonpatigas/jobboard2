{include file="header.tpl"}
		
<div class="admin-content">

 	<div class="admin-wrap-content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<label class="admin-label">Company package plan management</label>
			<div class="subheading">Manually change resources assigned for this specific company
				<a href="{$BASE_URL_ADMIN}company/{$data.emp_id}">
					<div class="subheading" style="color: #000;opacity: 0.65;">(&larr;go back)</div>
				</a>
			</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt25">
		<label class="mb10">COMPANY DETAILS</label>
		{if $data}
			<ul class="list-group">
			  <li class="list-group-item"><strong>Employer email: </strong>&nbsp;{$data.email}</li>
			  <li class="list-group-item"><strong>Employer name: </strong>&nbsp;{$data.employer_name}</li>
			  <li class="list-group-item"><strong>Company name: </strong>&nbsp;{$data.company_name}</li>
			</ul>
		{/if}
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt25">
		<label class="mb10">PACKAGE PLAN DETAILS</label>
		{if $package_data}
			<form id="epmf" method="post" action="{$BASE_URL_ADMIN}company/{$data.emp_id}/plan-management" role="form">
				<input type="hidden" id="package_updated" name="package_updated" value="1" />

				<div class="package-box">

					<div class="form-group">
						<label>Jobs left: </label><input type="number" class="form-control minput" type="text" name="jl" value="{$package_data.jobs_left}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Resume downloads left: </label><input type="number" class="form-control minput" type="text" name="cvdl" value="{$package_data.cv_downloads_left}" />&nbsp;
					</div>

					<div class="form-group">
						<label>Job period: </label><input type="number" class="form-control minput" type="text" name="jp" value="{$package_data.job_period}" />&nbsp;
					</div>

				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pl0">
					<div class="button-holder-admin" >
						<button type="submit" class="btn btn-default btn-primary mbtn" name="submit" >Save</button>

						<a href="{$BASE_URL_ADMIN}company/{$data.emp_id}" style="text-decoration: none;">
							<button type="button" class="btn btn-default btn-warning mbtn" name="button" id="button" >Go back</button>
						</a>
					</div>
				</div>

			</form>

		{/if}
	</div>


	</div>
</div>

{if $RES_UPDATED_POPUP == 'true'}
<script type="text/javascript">
   setTimeout(function(){
   	jobberBase.messages.add('Resources have been updated');
   }, 1000);
</script>
{/if}

{include file="footer.tpl"}