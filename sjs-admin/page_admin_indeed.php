<?php 
	global $db;
	$smarty->assign('ACTIVE', 15);

	if ($id === "settings") {
		escape($_POST);

		$jobs_flag = (isset($indeed_show_both_jobs_flag)) ? 1 : 0;

		$sql = 'UPDATE indeed_settings SET
		        activate_select="' . $indeed_activate_select . '", homepage_dropdown="' . $indeed_homepage_dropdown_select . '", publisher_id="' . $indeed_publisher_id . '", show_both_jobs_flag=' . $jobs_flag . ', query="' . $indeed_query . '", country="' . $indeed_country . '", location="' . $indeed_location . '", jobtype="' . $indeed_jobtype . '", jobs_old="' . $indeed_jobs_old . '" WHERE id=1';

		$db->query($sql);
		clear_main_cache();
		$smarty->assign('updated_popup', true);
		$template = 'indeed.tpl';
	} else if ($id === "delete") {

		$exp = explode("-", $extra);
		$categoryId = $exp[0];
		$itemId = $exp[1];

		$sql = 'DELETE FROM indeed_search_options WHERE id=' . intval($itemId);
		$db->query($sql);
		$smarty->assign('deleted_popup', true);

		// job types
		$sql = 'SELECT id, name, value FROM indeed_search_options WHERE category_id =' . $categoryId. ' ORDER BY id DESC';
		$result = $db->query($sql);
		$items = array(); $ids = array();
		while ($row = $result->fetch_assoc()) {
			array_push($ids, $row['id']);
			$arr = array("name" => $row['name'], "value" => $row['value']);
			$items[$row['id']] = $arr;
		}

		$smarty->assign('indeed_field_ids', implode(",", $ids));
		$smarty->assign('ids', $ids);
		$smarty->assign('items', $items);
		$smarty->assign('indeed_category_id', $categoryId);

		$smarty->assign('indeed_headline', "Edit Indeed Search Settings");

		$template = 'indeed-edit.tpl';

	} else if ($id === "manage") {

		if ($extra !== "" && !empty($extra)) {

			if (isset($_POST['indeed_category_id'])) {
				escape($_POST);
				$item_ids = explode(",", $field_ids);
				
				foreach ($item_ids as $item_id) {
					// update each item
					$name = $_POST['name_' . $item_id];
					$value = $_POST['value_' . $item_id];
					$sql = 'UPDATE indeed_search_options SET name="' . $name . '", value="' . $value . '" WHERE id=' . intval($item_id) . ' AND category_id=' . intval($indeed_category_id);
					$db->query($sql);
				}
				clear_main_cache();
				$smarty->assign('updated_popup', true);
			}

			// job types
			$sql = 'SELECT id, name, value FROM indeed_search_options WHERE category_id =' . $extra . ' ORDER BY id DESC';
			$result = $db->query($sql);
			$items = array(); $ids = array();
			while ($row = $result->fetch_assoc()) {
				array_push($ids, $row['id']);
				$arr = array("name" => $row['name'], "value" => $row['value']);
				$items[$row['id']] = $arr;
			}

			$smarty->assign('indeed_field_ids', implode(",", $ids));
			$smarty->assign('ids', $ids);
			$smarty->assign('items', $items);
			$smarty->assign('indeed_category_id', $extra);

			if ($extra === "1")
				$smarty->assign('indeed_headline', "Job Types Indeed Settings");
			else if ($extra === "2")
				$smarty->assign('indeed_headline', "Countries Indeed Settings");
			else if ($extra === "3")
				$smarty->assign('indeed_headline', "Cities Indeed Settings");

			$template = 'indeed-edit.tpl';
		} else {
			$template = 'indeed-manage.tpl';
		}

	} else if ($id === "add") {

		if (isset($_POST['indeed_category_select'])) {
			escape($_POST);
			//
			$sql = 'INSERT INTO indeed_search_options (id, category_id, name, value) VALUES (
				DEFAULT,
				' . $indeed_category_select . ',
					"' . $name . '",
					"' . $value . '")';
			$db->query($sql);
			clear_main_cache();
			$smarty->assign('added_popup', true);
		}

		$cats = array(
			"1" => "Job Type",
			"2" => "Country",
			"3" => "City"
		);
		$smarty->assign('indeed_categories', $cats);
		$template = 'indeed-add.tpl';
	} else {
		$template = 'indeed.tpl';
	}

	$sql = 'SELECT * FROM indeed_settings WHERE id = 1';
	$result = $db->query($sql);
	$row = $result->fetch_assoc();

	$smarty->assign('data', $row);

	
?>
