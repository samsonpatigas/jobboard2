<?php
	require_once '../_config/config.php';
	require_once '../_lib/class.Types.php';
	
		$sql = 'SELECT a.created_on, a.title, b.name, c.name as "category", d.name as "location", e.name as "type" from jobs as a 
		LEFT JOIN employer as b ON a.employer_id = b.id
	    LEFT JOIN categories as c ON a.category_id = c.id
	    LEFT JOIN cities as d ON a.city_id = d.id
	    LEFT JOIN types as e ON a.type_id = e.id
		ORDER BY a.created_on DESC';

		$result = $db->query($sql);

		$a = array();
		while ($row = $result->fetch_assoc()){
			$a['data'][] = $row;
		}
		echo (json_encode($a));
?>