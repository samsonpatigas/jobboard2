<?php 

	$smarty->assign('ACTIVE', 10);

	if ($id == 'update' && $id != NULL) { 

		$data = array('home_sidebar' => addslashes($_POST['home_sidebar']), 'home_leaderboard' => addslashes($_POST['home_leaderboard']), 'detail_rectangle' => addslashes($_POST['detail_rectangle']), 'backoffice_rectangle' => addslashes($_POST['backoffice_rectangle']), 'adsense_count' => $_POST['adsense_count']);

		try {
			require_once APP_PATH . '_lib/class.Adsense.php';
		} catch (Exception $e) {
			define('ADSENSE', false);
		}

		$adsense = new Adsense();
		$adsense->update($data);
		$smarty->assign('updated', true);
	}

    //get current values
	$sql = 'SELECT listing_side_rectangle as "home_sidebar_value", listing_leaderboard as "home_leaderboard_value", detail_rectangle as "detail_rectangle_value", backoffice_rectangle as "backoffice_rectangle_value" FROM '.DB_PREFIX.'adsense WHERE id=1';
	$result = $db->query($sql);
	$row = $result->fetch_assoc();
	$smarty->assign('current_form', $row);

	$quickSQL = 'SELECT count from adsense_count';
	$result = $db->query($quickSQL);
	$row = $result->fetch_assoc();
	$smarty->assign('adsense_count', $row['count']);

	$template = 'adsense.tpl';

?>