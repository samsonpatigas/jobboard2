<?php
	if(!file_exists('_config/config.php')) 
	{
	 die('[index.php] _config/config.php not found');
	}
	require_once '_config/config.php';

	$DIR_CONST = '';
	if (defined('__DIR__'))
	$DIR_CONST = __DIR__;
	else
	$DIR_CONST = dirname(__FILE__);

	$SUCCESS_TPL_JOBS = FALSE;

	if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	  session_unset();
	  session_destroy();
	}
	$_SESSION['LAST_ACTIVITY'] = time();
    $filters = $_POST;

    $data = file_get_contents('php://input');
    $filters = json_decode($data,true);
    $job_listing = new Job();

    echo json_encode($job_listing->api_home_job_listing($filters));
?>