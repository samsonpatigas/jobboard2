<?php

  if(!file_exists('_config/config.php')) {
     die('[index.php] _config/config.php not found');
  }
  // require_once '_tools/email_debug.php';
  require_once '_config/config.php';
  require 'vendor/autoload.php';
  require_once 'constants/SampleCodeConstants.php';

  use net\authorize\api\contract\v1 as AnetAPI;
  use net\authorize\api\controller as AnetController;

  define("AUTHORIZENET_LOG_FILE", "cclog.json");

    global $db;
    $sql = "SELECT value FROM payment_settings_fees WHERE name = 'CreditCard'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    $amount = $row['value'];
    

  $DIR_CONST = '';
  if (defined('__DIR__'))
    $DIR_CONST = __DIR__;
  else
    $DIR_CONST = dirname(__FILE__);

function chargeCreditCard($amount)
{
    /* Create a merchantAuthenticationType object with authentication details
       retrieved from the constants file */
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName(\SampleCodeConstants::MERCHANT_LOGIN_ID);
    $merchantAuthentication->setTransactionKey(\SampleCodeConstants::MERCHANT_TRANSACTION_KEY);
    
    // Set the transaction's refId
    $refId = 'ref' . time();
    $data = file_get_contents('php://input');
    $data2 = json_decode($data);


    // Create the payment data for a credit card
    $creditCard = new AnetAPI\CreditCardType();
    $creditCard->setCardNumber($data2->cardNumber);
    $creditCard->setExpirationDate($data2->expiryYear . "-" . $data2->expiryMonth);
    $creditCard->setCardCode($data2->cvv);

    // Add the payment data to a paymentType object
    $paymentOne = new AnetAPI\PaymentType();
    $paymentOne->setCreditCard($creditCard);

    // Create order information
    $order = new AnetAPI\OrderType();
    $order->setInvoiceNumber("10101");
    $order->setDescription("Regular Job Board Posting");

    // Set the customer's Bill To address
    $customerAddress = new AnetAPI\CustomerAddressType();
    $customerAddress->setFirstName($data2->employer_name);
    // $customerAddress->setLastName("Johnson");
    // $customerAddress->setCompany("Souveniropolis");
    // $customerAddress->setAddress("14 Main Street");
    // $customerAddress->setCity("Pecan Springs");
    // $customerAddress->setState("TX");
    // $customerAddress->setZip("44628");
    $customerAddress->setCountry("USA");

    // Set the customer's identifying information
    $customerData = new AnetAPI\CustomerDataType();
    $customerData->setType("individual");
    $customerData->setId($data2->employer_userid);
    $customerData->setEmail($data2->employer_email);

    // Add values for transaction settings
    $duplicateWindowSetting = new AnetAPI\SettingType();
    $duplicateWindowSetting->setSettingName("duplicateWindow");
    // $duplicateWindowSetting->setSettingValue($row['value']);
    $duplicateWindowSetting->setSettingValue("59");

    // Add some merchant defined fields. These fields won't be stored with the transaction,
    // but will be echoed back in the response.
    $merchantDefinedField1 = new AnetAPI\UserFieldType();
    $merchantDefinedField1->setName("customerLoyaltyNum");
    $merchantDefinedField1->setValue("1128836273");

    $merchantDefinedField2 = new AnetAPI\UserFieldType();
    $merchantDefinedField2->setName("favoriteColor");
    $merchantDefinedField2->setValue("blue");

    // Create a TransactionRequestType object and add the previous objects to it
    $transactionRequestType = new AnetAPI\TransactionRequestType();
    $transactionRequestType->setTransactionType("authCaptureTransaction");
    // $transactionRequestType->setAmount($row['value']);
    $transactionRequestType->setAmount("59");
    $transactionRequestType->setOrder($order);
    $transactionRequestType->setPayment($paymentOne);
    $transactionRequestType->setBillTo($customerAddress);
    $transactionRequestType->setCustomer($customerData);
    $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
    $transactionRequestType->addToUserFields($merchantDefinedField1);
    $transactionRequestType->addToUserFields($merchantDefinedField2);

    // Assemble the complete transaction request
    $request = new AnetAPI\CreateTransactionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setTransactionRequest($transactionRequestType);

    // Create the controller and get the response
    $controller = new AnetController\CreateTransactionController($request);
    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
    

    if ($response != null) {
        // Check to see if the API request was successfully received and acted upon
        if ($response->getMessages()->getResultCode() == "Ok") {
            // Since the API request was successful, look for a transaction response
            // and parse it to display the results of authorizing the card
            $tresponse = $response->getTransactionResponse();
        
            if ($tresponse != null && $tresponse->getMessages() != null) {
                /*
                echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
                echo " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
                echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
                echo " Auth Code: " . $tresponse->getAuthCode() . "\n";
                echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";
                */
                

                $successResponse = array(
                    'TransactionID' => $tresponse->getTransId(),
                    'ResponseCode' => $tresponse->getResponseCode(),
                    'MessageCode' => $tresponse->getMessages()[0]->getCode(),
                    'AuthCode' => $tresponse->getAuthCode(),
                    'Description' => $tresponse->getMessages()[0]->getDescription(),
                    $customerAddress,
                    $order,
                    $creditCard
                );
                $CCData = array();
                $CCData['transcation_id'] = $tresponse->getTransId(); 
                $CCData['tr_status'] = $tresponse->getMessages()[0]->getCode();
                $CCData['tr_timestamp'] = '';
                $CCData['employer_id'] = $data2->employer_userid;
                $CCData['job_id'] = $data2->job_id;
                $CCData['paypal_payer_id'] = '';
                $CCData['payer_status'] = '';
                $CCData['payer_email'] = $data2->employer_email;
                $CCData['payer_name'] = $data2->employer_name;
                $CCData['payer_surname'] = '';
                $CCData['amount'] = $amount;
                $CCData['currency'] = 'USD';
                $CCData['CCardNum'] = $data2->cardNumber;
                $CCData['authcode'] = $tresponse->getAuthCode();
                // var_dump($CCData); die();
                $saveCCPayment = new Employer();
                $saveCCPayment->createCCPayment($CCData);

                echo json_encode($successResponse);
            } else {
                echo "Transaction Failed \n";
                if ($tresponse->getErrors() != null) {
                    echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                    echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                }
            }
            // Or, print errors if the API request wasn't successful
        } else {
            echo "Transaction Failed \n";
            $tresponse = $response->getTransactionResponse();
        
            if ($tresponse != null && $tresponse->getErrors() != null) {
                echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
            } else {
                echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
                echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
            }
        }
    } else {
        echo  "No response returned \n";
    }

        $to      = 'quarxdmz@gmail.com'; 
        $subject = 'Credit Card Payment - Transaction ID: ' . $tresponse->getTransId(); 
        $message = '
          <!DOCTYPE html>
          <html>
          <head>
          <meta charset="UTF-8">
          <title></title>
          </head>
          <body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;">
            <div style="padding:10px; background:#333; font-size:24px; color:#CCC;">
              <a href="http://www.desertdentalstaffing.com">
                <img src="http://jobboard.ferret9.com/uploads/logos/main-logo.png" width="36" height="30" alt="Logo" style="border:none; float:left;">
              </a>
            </div>
            <div style="padding:24px; font-size:17px;">
                Hello '.$row['fullname'].',<br /><br />You have paid thru Credit Card amounting to $59.5'. '<br /><br /><br /><br />
            </div>
          </body>
          </html>';
        
        $To = strip_tags($to);
        $TextMessage = strip_tags(nl2br($message),"<br>");
        $Subject = strip_tags($subject);

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: placements@desertdentalstaffing.com' . "Desert Dental Staffing\r\n";
        $headers .= 'Cc:  p.toman@bluerivergroup.com' . "\r\n";

        $employer_mail = new Employer();
        $client_id = '';
        $employer_mail->save_email_log($client_id, $email_type, $email, $employer_id, $job_subject, $job_email_content);
        mail($To, $Subject, $TextMessage, $headers);


    return $response;
}

chargeCreditCard($amount);
